"""

__author__ = "Aman Kumar"
"""

import traceback
import os
import shutil
import json
from json import JSONDecodeError
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce


class CreateCopy:

    def __init__(self, tenant, old_uuid, new_uuid, my_logger, name):
        self.tenant = tenant
        self.old_uuid = old_uuid
        self.new_uuid = new_uuid
        self.my_logger = my_logger
        self.name = name
        self.plan__uuid = ''
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")

    def create_parent_copy(self, type, plan__uuid, parent_type, child_type):
        """
        this method create copy of meta data file for new quota object/instance from old quota object/instance
        :param type: quota object or instance
        :param plan__uuid:  current quota uuid
        :param parent_type: parent quota type
        :param child_type: copied quota type
        :return: status whether data copied or not
        """
        parent_dir = self.main_dir
        self.plan__uuid = plan__uuid
        file_loc_old = None
        file_loc_new = None
        folder_loc_new = None
        if type == 'Parent':
            folder_loc_old = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.old_uuid
            file_name_old = self.old_uuid + '_ui_value_info.txt'
            file_loc_old = folder_loc_old + '\\' + file_name_old

            folder_loc_new = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.new_uuid
            file_name_new = self.new_uuid + '_ui_value_info.txt'
            file_loc_new = folder_loc_new + '\\' + file_name_new
        elif type == 'Child':
            folder_loc_old = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.plan__uuid +"\\Simulation\\"
            folder_loc_old = folder_loc_old + self.old_uuid  
            file_name_old = self.old_uuid + '_ui_value.txt'
            file_loc_old = folder_loc_old + '\\' + file_name_old

            folder_loc_new = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.plan__uuid +"\\Simulation\\"
            folder_loc_new = folder_loc_new + self.new_uuid  
            file_name_new = self.new_uuid + '_ui_value.txt'
            file_loc_new = folder_loc_new + '\\' + file_name_new

        self.my_logger.info(f"-- old location ---- {file_loc_old} ")
        self.my_logger.info(f"-- new location ---- {file_loc_new}")
        status = "Failed"
        try:
            if not os.path.exists(folder_loc_new):
                os.makedirs(folder_loc_new)
            try:
                shutil.copyfile(file_loc_old, file_loc_new)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', file_loc_old.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', file_loc_old.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            if type == "Child":
                graph_file_old = ''
                graph_file_new = ''
                if parent_type.lower() == child_type.lower():
                    graph_file_old = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.plan__uuid + "\\Simulation\\" + self.old_uuid + "\\Graph_Info.txt"
                    graph_file_new = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.plan__uuid + "\\Simulation\\" + self.new_uuid + "\\Graph_Info.txt"
                else:
                    graph_file_old = parent_dir + "\\" + self.tenant + "\\Graph_QS_Info.txt"
                    graph_file_new = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.plan__uuid + "\\Simulation\\" + self.new_uuid + "\\Graph_Info.txt"

                self.my_logger.debug(f" --- graph file src --- {graph_file_old}")
                self.my_logger.debug(f" --- graph file destination --- {graph_file_new}")
                try:
                    shutil.copyfile(graph_file_old, graph_file_new)
                except FileNotFoundError as e:
                    raise Exception('FileNotFoundError', file_loc_old.rsplit('\\', 1)[1])
                except JSONDecodeError as e:
                    raise Exception('JSONDecodeError', file_loc_old.rsplit('\\', 1)[1])
                except Exception as e:
                    raise

            self.my_logger.debug("--- file copied ---")
            self.update_name(self.name, file_loc_new)
            status = "Success"
        except Exception as e:
            raise

        return status

    def update_name(self, name, file_loc):
        data = []
        try:
            with open(file_loc) as json_data:
                data = json.load(json_data)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        data_dict = data["Parameters"]
        for d in data_dict:
            for k, v in d.items():
                if k == "SFDC" and v == "Name":
                    d["Value"] = name
        data["Parameters"] = data_dict
        try:
            with open(file_loc, 'w+') as file_obj:
                json.dump(data, file_obj, indent=4)
        except Exception as e:
            raise Exception('FileNotSavedException', file_loc.rsplit('\\', 1)[1])
