import json
import traceback
import xlsxwriter
import copy
import pandas as pd
from src.helper.s3 import S3
from src.service.ConfigApi import Config_Api
from src.helper.config import rp_config
from src.helper.salesforce import SalesForce
from json import JSONDecodeError


class DownloadQRFile:

    def __init__(self, param_list):
        self.tenant = param_list[0]
        self.selectedBu = param_list[1]
        self.quotaId = param_list[2]
        self.quota_name = param_list[3]
        self.geo_id = param_list[4]
        self.column_params = json.loads(param_list[5])
        self.column_label_params = json.loads(param_list[6])
        self.refinement_param = json.loads(param_list[7])
        self.my_logger = param_list[8]
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.file_path = None
        self.geo_mapping_list = None

    def prepare_file(self):
        self.my_logger.debug(f"---- Inside prepare_file method ----")
        status = 'Success'
        try:
            product_list = list(self.column_params.keys())
            product_list = sorted(product_list)
            self.my_logger.debug(f"---- Product list ---- {product_list}")
            self.file_path = self.main_dir + "/goal-setting/tmp/" + self.quota_name + ".xlsx"
            writer = pd.ExcelWriter(self.file_path, engine='xlsxwriter')
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            for product in product_list:
                self.my_logger.debug(f"----- current product --- {product}")
                self.my_logger.debug(f"---- columns ---- {self.column_params[product]}")
                self.my_logger.debug(f"---- labels ---- {self.column_label_params[product]}")
                columns_str = self.column_params[product]
                columns_list = columns_str.split(",")
                quota_query = "SELECT " + columns_str + " FROM Quota_GEO_Mapping__c WHERE BUName__c='" + self.selectedBu + "' and Is_Active__c=true and Product__c='" + product + "' and Quota__c='" + self.quotaId + "' LIMIT 10000"
                self.geo_mapping_list = SalesForce.query(quota_query)
                if not self.geo_mapping_list.empty:
                    self.my_logger.debug(f"---- query columns {self.geo_mapping_list.columns}")
                    self.geo_mapping_list.columns = columns_list
                    index = self.geo_mapping_list.columns.get_loc('Initial_Goals__c')
                    if self.refinement_param[product] == 'Market Share Brand Goal':
                        index += 3
                    else:
                        index += 1
                    self.geo_mapping_list.insert(index, 'Final Refinement', '')
                    self.geo_mapping_list['Final Refinement'] = self.geo_mapping_list['Refined_Goals__c'] - self.geo_mapping_list['Initial_Goals__c']
                    final_record_df = pd.DataFrame()
                    geo_id_list = list()
                    if self.geo_id is not None and self.geo_id != '':
                        geo_id_list = [self.geo_id]
                    else:
                        geo_id_list = ['', None]

                    map_of_record_geoid = self.populate_child_records_map()
                    self.my_logger.debug(f"---- map_of_record_geoid size ---- {len(map_of_record_geoid)}")
                    self.my_logger.debug(map_of_record_geoid.keys())
                    final_record_list = list()
                    self.find_child_records(final_record_list, map_of_record_geoid, set(geo_id_list))
                    self_record_df = pd.DataFrame(
                        self.geo_mapping_list.loc[self.geo_mapping_list['Position_Code__c'] == self.geo_id])
                    child_record_df = pd.DataFrame(final_record_list)
                    final_record_df = pd.concat([self_record_df, child_record_df])

                    columns_label_list = self.column_label_params[product].split(",")
                    final_record_df.drop(['Parent_Position_Code__c'], axis=1, inplace=True)
                    self.my_logger.debug(f"---- final_record_df columns ---")
                    self.my_logger.debug(f"{final_record_df.columns}")
                    self.my_logger.debug(f"---- columns_label_list ---- ")
                    self.my_logger.debug(f"{columns_label_list}")
                    final_record_df.columns = columns_label_list
                    final_record_df.to_excel(writer, sheet_name=product, index=False, startrow=1, header=False)
                    workbook = writer.book
                    worksheet = writer.sheets[product]
                    header_format = workbook.add_format({
                        'bold': True,
                        'fg_color': '#DCDCDC',
                        'border': 1,
                        'valign': 'top'
                    })
                    border_format = workbook.add_format()
                    start_col = 0
                    end_col = start_col + len(final_record_df.columns)
                    row_size = len(final_record_df) + 1
                    for col_num, value in enumerate(final_record_df.columns.values):
                        worksheet.write(0, col_num, value, header_format)
                    border_format.set_top(1)
                    for i in range(start_col, end_col):
                        worksheet.write(row_size, i, '', border_format)
                    border_format = workbook.add_format()
                    border_format.set_left(1)
                    for i in range(start_col, row_size):
                        worksheet.write(i, end_col, '', border_format)
            writer.save()
            self.my_logger.debug(f"---- file prepared successfully ---- ")
            status = self._upload_on_s3()
            self.my_logger.debug(f"----- file uploaded successfully ---- ")
        except Exception as e:
            status = 'Failed'
            raise
        return status

    def populate_child_records_map(self):
        map_records_parent_geo = dict()
        if not self.geo_mapping_list.empty:
            for _, record in self.geo_mapping_list.iterrows():
                # self.my_logger.debug(f"---- record --- {record}")
                # if record['Parent_Position_Code__c'] is not None and record['Parent_Position_Code__c'] != '':
                if record['Parent_Position_Code__c'] not in map_records_parent_geo:
                    map_records_parent_geo[record['Parent_Position_Code__c']] = list()
                temp_list = map_records_parent_geo[record['Parent_Position_Code__c']]
                temp_list.append(record)
                map_records_parent_geo[record['Parent_Position_Code__c']] = temp_list

        return map_records_parent_geo

    def find_child_records(self, record_list, map_of_record_geoid, geo_id_set):
        self.my_logger.debug(f"---- parent geoId set ---- {geo_id_set}")
        self.my_logger.debug(f"---- parent geoId set size ---- {len(geo_id_set)}")
        self.my_logger.debug(f"---- record list size ---- {len(record_list)}")
        list_geoid = list()
        for geo_id in geo_id_set:
            if geo_id in map_of_record_geoid.keys():
                temp_record_list = map_of_record_geoid[geo_id]
                if len(temp_record_list) > 0:
                    record_list.extend(temp_record_list)
                    for record in temp_record_list:
                        list_geoid.append(record['Position_Code__c'])
        if len(list_geoid) > 0:
            self.find_child_records(record_list, map_of_record_geoid, set(list_geoid))

    def _upload_on_s3(self):
        self.my_logger.info(f"--- inside upload to s3 path --- ")
        try:
            S3.connect_to_s3(self.tenant)
        except Exception as e:
            raise
        filename = self.quota_name + '.xlsx'
        self.my_logger.debug(f"----- filename of file to be uploaded ---- {filename}")
        s3_bucket = self.config_obj.get_value_from_key("s3", "s3_bucket")
        s3path = rp_config['s3']['goalSettingPath'].format(bucket=s3_bucket, tenant=self.tenant, uuid='QR',
                                                           filename=filename)
        self.my_logger.debug(f"------ S3 path -------- ")
        self.my_logger.debug(f"{s3path}")
        S3.upload_file_from_local_path(self.file_path, s3path)

        return 'Success'
