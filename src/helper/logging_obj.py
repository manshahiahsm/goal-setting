"""

    Creates ONLY ONE custom logger for EACH running app

    Usage Convention:
        for importing in static methods of other files, use get_logger() in each static method
        for importing in class methods, use create logger once in __init__

"""

from datetime import datetime
import logging


class LoggingObj:

    currently_executing_url = None
    dict_of_loggers = {}
    app_name = None

    @staticmethod
    def get_logger():

        if LoggingObj.app_name in LoggingObj.dict_of_loggers:
            return LoggingObj.dict_of_loggers[LoggingObj.app_name]
        else:
            if LoggingObj.currently_executing_url:

                if LoggingObj.currently_executing_url.find('?') != -1:
                    LoggingObj.app_name = LoggingObj.currently_executing_url.split('/', 4)[4].split('?',1)[0]
                else:
                    logging.info("URL NAME" + LoggingObj.currently_executing_url)
                    LoggingObj.app_name = LoggingObj.currently_executing_url.split('/', 4)[4]
                    logging.info("APP NAME" + LoggingObj.app_name)

                curr_logger = logging.getLogger(LoggingObj.app_name)
                f_handler = logging.FileHandler("tmp/" + datetime.now().strftime(LoggingObj.app_name + '_%d_%m_%Y.log'))
                c_handler = logging.StreamHandler()
                f_handler.setLevel(logging.DEBUG)
                c_handler.setLevel(logging.DEBUG)
                f_format = logging.Formatter('%(asctime)s -%(module)s.%(funcName)s()-%(levelname)s- %(message)s')
                f_handler.setFormatter(f_format)
                c_handler.setFormatter(f_format)
                curr_logger.addHandler(f_handler)
                curr_logger.addHandler(c_handler)

                LoggingObj.dict_of_loggers[LoggingObj.app_name] = curr_logger
                print("CURRENT LOGGER")
                print(curr_logger)
                return curr_logger
