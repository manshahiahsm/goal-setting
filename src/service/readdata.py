import traceback
import json
import pandas as pd
import os
import src.helper.sql_connection
from pandas.io import sql
from sqlalchemy import text
from src.service.ConfigApi import Config_Api
from src.service.suggestionutility import SuggestionUtility
from src.helper.salesforce import SalesForce
from json import JSONDecodeError


class ReadSqlData:

    def __init__(self, tenant, uuid, clause, offset, row_num, order_by, my_logger, view_at='', parent_uuid=''):
        self.tenant = tenant
        self.uuid = uuid
        self.clause = clause
        self.offset = offset
        self.my_logger = my_logger
        self.order_by = order_by
        self.row_num = row_num
        self.table_name = ''
        self.view_at = view_at
        self.parent_uuid = parent_uuid
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")

        self.my_logger.debug(f" --- tenant ----- {self.tenant}")
        self.my_logger.debug(f" --- uuid ----- {self.uuid}")
        self.my_logger.debug(f" --- clause ----- {self.clause}")
        self.my_logger.debug(f" --- offset ----- {self.offset}")
        self.my_logger.debug(f" --- row_num ----- {self.row_num}")
        self.my_logger.debug(f" --- order_by ----- {self.order_by}")
        self.my_logger.debug(f" --- view at ----- {self.view_at}")
        self.my_logger.debug(f" ---- parent uuid --- {self.parent_uuid}")
        self.my_logger.debug(f" --- main_dir ----- {self.main_dir}")
        self.engine = src.helper.sql_connection.createEngineToAdminDB(self.tenant, 'processing')
        self.my_logger.debug("--- connection established ---- ")

    def run_service(self):
        """
        this method returns sql data for quota instance
        :return: json data
        """
        final_data = dict()
        try:
            data = {}
            data_count = {}
            file_location = self.main_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = ''
            try:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            uuid_data = file_data[self.uuid]
            output_header = ""
            hierarchy_column = ""
            rollup_column = ""
            for key, value in uuid_data.items():
                if key.lower() == "output":
                    output_header = value
                if key.lower() == "hierarchy":
                    hierarchy_column = value
                if key.lower() == "rollup_header":
                    rollup_column = value

            self.my_logger.debug(f"---- rollup header ------ {rollup_column}")
            self.my_logger.debug(f"---- output header ----- {output_header}")
            self.my_logger.debug(f"---- hierarchy_column ----- {hierarchy_column}")
            self.view_at = self.view_at.strip()
            self.my_logger.debug(f"--- view at updated ---- {self.view_at}")
            scenario_uuid = self.uuid
            rollup_flag = False
            if self.view_at:
                rollup_value = self.getSetGoalValue()
                rollup_value = rollup_value.strip()
                if rollup_value is not None and self.view_at.lower() != rollup_value.lower():
                    scenario_uuid = self.uuid + "_rollup"
                    rollup_flag = True
            query = None
            self.order_by = " Id ASC "
            if rollup_flag:
                table_name = "IC_Output.[t_"+scenario_uuid+"]"
                query = "Select * " + " from " + table_name + " " + self.clause + " AND Geo_Type='" + self.view_at + "' ORDER BY " + self.order_by + " offset " + self.offset + " rows FETCH NEXT " + self.row_num + " ROWS ONLY"
            else:
                query = 'exec sp_quota_view \'' + scenario_uuid + '\',\'' + self.clause + '\',' + str(self.offset) + ',' + str(self.row_num) + ',\'' + self.order_by + '\',\''+output_header+'\',\''+hierarchy_column+'\''

            self.my_logger.debug(f"---- query ------- {query}")
            final_data = dict()
            result = pd.read_sql_query(text(query), self.engine)
            data = json.loads(result.to_json(orient='records', date_format='iso', date_unit='s'))
            final_data["data"] = data
            query_count = None
            if rollup_flag:
                table_name = "IC_Output.[t_" + scenario_uuid + "]"
                query_count = "Select count(*) as tcount from "+table_name+" "+self.clause+" AND Geo_Type='"+self.view_at+"' "
            else:
                query_count = 'exec sp_quota_count \'' + scenario_uuid + '\',\'' + self.clause + '\',' + str(self.offset) + ',' + str(self.row_num) + ',\'' + self.order_by + '\',\''+hierarchy_column+'\''

            self.my_logger.debug(f"---- query count ------- {query_count}")
            result_set = pd.read_sql_query(text(query_count), self.engine)
            data_count = json.loads(result_set.to_json(orient='records'))
            self.my_logger.debug("---- count data ----")
            self.my_logger.debug(data_count)
            final_data["tCount"] = data_count
        except Exception as e:
            raise
        finally:
            self.engine.dispose()

        return json.dumps(final_data)

    def get_composite_score(self, plan_uuid, final_column_name, markers_weight_param):
        try:
            data_list = []
            markers_weight_list = markers_weight_param.split(',')
            for marker in markers_weight_list:
                data_list.append(marker)

            query = 'select '+final_column_name+' FROM IC_Output.[t_'+plan_uuid+']'
            self.my_logger.info(f" --- query ---- {query}")
            result = pd.read_sql_query(text(query), self.engine)
            final_column_list = final_column_name.split(',')
            self.my_logger.debug(f"----- result set -----")
            self.my_logger.debug(result)
            self.my_logger.debug(f"----- final column list -----")
            self.my_logger.debug(final_column_list)
            for column in final_column_list:
                column = column.strip()
                data_list.append(str(result.iloc[0][column]))

            final_column_name = 'C_1,C_2,C_3,C_4,' + final_column_name
            final_column_list = final_column_name.split(',')
            self.my_logger.debug(f" ---- final column list ----- ")
            self.my_logger.debug(f"{final_column_list}")
            self.my_logger.debug(f" --- final data list ----- ")
            self.my_logger.debug(f"{data_list}")
            try:
                df = pd.DataFrame([data_list], columns=final_column_list)
                return 'success', df
            except Exception as e:
                self.my_logger.info("--- unable to convert data list to df ----")
                self.my_logger.error(e.args)
                self.my_logger.exception(traceback.format_exc())
                SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', self.uuid)
                return 'failed', ''
        except Exception as e:
            self.my_logger.info("--- error in get_composite_score method ---")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', self.uuid)
            return 'failed', ''

    def get_final_column_name(self, parent_uuid, sc_uuid):
        try:
            rule_obj = SuggestionUtility(self.tenant, self.my_logger)
            column_list = rule_obj.get_rules_column(parent_uuid, sc_uuid)
            column_name = ','.join([str(x) for x in column_list])
            final_column_name = column_name + ',Composite_Score'
            return 'success', final_column_name
        except Exception as e:
            self.my_logger.info(f"--- unable to read final column name ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
            return 'failed', ''

    def create_suggestion_final_table(self, sc_uuid, column_name):
        status = 'success'
        try:
            table_name = 'IC_Output.[t_'+sc_uuid+'_suggestion]'
            drop_query = 'DROP TABLE IF EXISTS '+table_name
            sql.execute(text(drop_query), self.engine)
            column_name = 'C_1,C_2,C_3,C_4,' + column_name
            column_list = column_name.split(',')
            column_schema = ''
            for column in column_list:
                column_schema = column_schema+column+' NUMERIC(25,2),'
            column_schema = column_schema[:-1]
            self.my_logger.debug(f"----- Suggestion table column schema {column_schema}")
            create_query = '''CREATE TABLE '''+table_name+'''('''+column_schema+''')'''
            self.my_logger.debug(f"----- create_query {create_query}")
            sql.execute(text(create_query), self.engine)
        except Exception as e:
            self.my_logger.debug(f"--- unable to create suggestion final table --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
            status = 'Failed'
        finally:
            self.engine.dispose()
        return status

    def push_score_to_final_table(self, df, sc_uuid):
        status = 'success'
        try:
            self.my_logger.info(f" --- inserting df into final table ---- ")
            table_name = 'IC_Output.[t_'+sc_uuid+'_suggestion]'
            # creating column list for insertion
            self.my_logger.debug(f"-- df to insert ---")
            self.my_logger.debug(f"{df}")
            table_cols = ",".join([str(i) for i in df.columns.tolist()])
            self.my_logger.info(f"--- column list from df ---- {table_cols}")
            column_list = table_cols.split(',')
            self.my_logger.info(f"--- column list ---- {column_list}")
            insert_query = "INSERT INTO "+table_name+" (" + table_cols + ") VALUES ("
            try:
                for column in column_list:
                    self.my_logger.info(f"--- processing column ---- {column}")
                    self.my_logger.info(df.iloc[0][column])
                    value = df.iloc[0][column]
                    insert_query = insert_query + str(value)+","
                insert_query = insert_query[:-1]
                insert_query = insert_query+")"
                insert_query = insert_query.replace("None", "NULL")
                self.my_logger.debug(f" --- final insert query ---- ")
                self.my_logger.debug(insert_query)
                sql.execute(text(insert_query), self.engine)
            except Exception as e:
                self.my_logger.info(f"--- error while processing data frame ---")
                self.my_logger.error(e.args)
                self.my_logger.exception(traceback.format_exc())
                SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
                status = 'Failed'
        except Exception as e:
            self.my_logger.debug(f" --- unable to insert df into final table ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
            status = 'Failed'

        return status
    
    def get_suggestion_table_data(self, parent_sc_uuid):
        """
        This methods returns suggestion table data
        :param parent_sc_uuid: quota object uuid
        :return: json data
        """
        self.my_logger.info(f" --- inside get suggestion table data ----")
        final_data = dict()
        try:
            order_by_column = self.get_rules_column_order(self.uuid, parent_sc_uuid)
            table_name = 'IC_Output.[t_'+self.uuid+'_suggestion]'
            final_data = dict()
            query = "Select top 3 * from "+table_name+" ORDER BY "+order_by_column+" "
            self.my_logger.debug(f"---- query ------- {query}")
            result = pd.read_sql_query(text(query), self.engine)
            data = json.loads(result.to_json(orient='records'))
            final_data["data"] = data

            # now reading rules json
            suggestion_obj = SuggestionUtility(self.tenant, self.my_logger)
            suggestion_dict = suggestion_obj.get_rules_json(parent_sc_uuid, self.uuid)     
            final_data["rules"] = suggestion_dict
        except Exception as e:
            raise
        finally:
            self.engine.dispose()

        return json.dumps(final_data)

    def get_rules_column_order(self, sc_uuid, parent_sc_uuid):
        self.my_logger.info(f"--- inside get rules column order ---- ")
        self.my_logger.debug(f"--- parent uuid ---- {parent_sc_uuid}")
        self.my_logger.debug(f"---- uuid ---- {sc_uuid}")
        suggestion_obj = SuggestionUtility(self.tenant, self.my_logger)
        suggestion_dict = suggestion_obj.get_rules_json(parent_sc_uuid, sc_uuid)
        self.my_logger.debug(f"--- suggestion dictionary ---- ")
        self.my_logger.debug(suggestion_dict)
        self.my_logger.debug(type(suggestion_dict))
        rules_list = suggestion_dict["Rules"]
        rules_dict = dict()
        weight_list = []
        operator_map = {}
        for rule in rules_list:
            weight_list.append(rule["Rule Weightage"])
            column_name = rule["Column"]
            operator_map[column_name]= rule["Operator"]

        self.my_logger.info(f"--- before sorting list is  ----")
        self.my_logger.debug(weight_list)

        weight_list = sorted(weight_list, key=float, reverse=True)

        self.my_logger.info(f" --- after sorting list is ---- ")
        self.my_logger.debug(weight_list)

        column_order = 'Composite_Score DESC,'

        for weight in weight_list:
            for rule in rules_list:
                curr_weight = rule["Rule Weightage"]
                column_name = rule["Column"]
                if curr_weight == weight:
                    if column_order.find(column_name) == -1:
                        order_value = ' ASC,'
                        if operator_map[column_name] == '>' or operator_map[column_name] == '>=':
                            order_value = ' DESC,'
                        elif operator_map[column_name] == '<' or operator_map[column_name] == '<=':
                            order_value = ' ASC,'
                        column_order = column_order + column_name + order_value
                        break
        if column_order != '':
            column_order = column_order[:-1]
        self.my_logger.debug(f"--- final column order ---- ")
        self.my_logger.debug(column_order)
        return column_order

    def get_quota_master_data(self, scenario_uuid):
        """
        This method returns Quota Master data
        :param scenario_uuid: quota master uuid
        :return: json data
        """
        self.my_logger.debug(f"--- inside get_quota_master_data --- ")
        final_data = dict()
        try:
            exclude_columns = self.config_obj.get_value_from_key('Quota_Master', 'exclude_columns')
            exclude_columns_list = exclude_columns.split(",")
            input_labels = ''
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Instance_Info.txt"
            file_data = ''
            try:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            for key, value in file_data.items():
                if key == scenario_uuid:
                    input_labels = file_data[key]["labels"]

            period_labels = None
            App_Instance = False
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Apportioning_Info.txt"
            file_exist = False
            if os.path.exists(file_location):
                file_exist = True
            self.my_logger.info(f"---- file exist ---- {file_exist}")
            if file_exist:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
                    for key, value in file_data.items():
                        if key == scenario_uuid:
                            period_labels = file_data[key]["periods_label"]
                            App_Instance = True

            self.my_logger.debug(f" ---- scenario_uuid ----- {scenario_uuid}")
            table_name = None
            if not App_Instance:
                table_name = 'IC_Output.[t_' + scenario_uuid + ']'
            else:
                table_name = 'IC_Output.[t_' + scenario_uuid + '_GT]'

            final_data = dict()
            query = None
            if self.row_num is None or self.row_num == '':
                query = "Select * from " + table_name + " "+self.clause+" ORDER BY "+self.order_by+" "
            else:
                query = "Select * from " + table_name + " "+self.clause+" ORDER BY "+self.order_by+" offset " + self.offset + " rows FETCH NEXT " + self.row_num + " ROWS ONLY"

            count_query = "Select count(*) tcount from " + table_name + " "+self.clause+" "
            self.my_logger.debug(f"---- data query ------- {query}")
            self.my_logger.debug(f" --- count query ----- {count_query}")
            result = pd.read_sql_query(text(query), self.engine)
            result.drop(exclude_columns_list, axis=1, inplace=True)
            data = json.loads(result.to_json(orient='records'))
            result = pd.read_sql_query(text(count_query), self.engine)
            total_count = json.loads(result.to_json(orient='records'))
            final_data["data"] = data
            final_data["tCount"] = total_count
            if period_labels is not None:
                input_labels = input_labels + ',' + period_labels
            final_data["labels"] = input_labels
            filter_columns = self.config_obj.get_value_from_key('Quota_Master', 'filter_columns')
            final_data["filter"] = filter_columns
        except Exception as e:
            raise
        finally:
            self.engine.dispose()
        return json.dumps(final_data)

    def get_filter_column_data(self, scenario_uuid):
        """
        This method returns filter column data for QM
        :param scenario_uuid: quota master uuid
        :return: json data
        """
        self.my_logger.debug(f"--- inside get_filter_column_data --- ")
        final_data = dict()
        try:
            parent_dir = self.main_dir
            app_instance = False
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Apportioning_Info.txt"
            file_exist = False
            if os.path.exists(file_location):
                file_exist = True
            self.my_logger.info(f"---- file exist ---- {file_exist}")
            if file_exist:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
                    for key, value in file_data.items():
                        if key == scenario_uuid:
                            app_instance = True

            self.my_logger.debug(f" ---- scenario_uuid ----- {scenario_uuid}")
            table_name = None
            if not app_instance:
                table_name = 'IC_Output.[t_' + scenario_uuid + ']'
            else:
                table_name = 'IC_Output.[t_' + scenario_uuid + '_GT]'

            final_data = dict()
            filter_columns = None
            filter_labels = None
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Instance_Info.txt"
            with open(file_location) as json_file:
                file_data = json.load(json_file)
            for key, value in file_data.items():
                if key == scenario_uuid:
                    filter_columns = file_data[key]["filter_columns"]
                    filter_labels = file_data[key]["filter_labels"]
            # filter_columns = self.config_obj.get_value_from_key('Quota_Master', 'filter_columns')
            # filter_labels = self.config_obj.get_value_from_key('Quota_Master', 'filter_labels')

            query = None
            query = "Select distinct " + filter_columns + " from " + table_name + " GROUP BY " + filter_columns
            self.my_logger.debug(f"---- data query ------- {query}")
            result = pd.read_sql_query(text(query), self.engine)
            data = json.loads(result.to_json(orient='records'))
            final_data["data"] = data
            final_data["filter"] = filter_columns
            final_data["label"] = filter_labels
        except Exception as e:
            raise
        finally:
            self.engine.dispose()

        return json.dumps(final_data)

    def getSetGoalValue(self):
        folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid
        file_name = self.uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise
        file_data = data["Parameters"]

        rollup_value = None
        for item in file_data:
            for key, value in item.items():
                if key.lower() == "sfdc" and value.lower() == "calculate_goal_level__c":
                    rollup_value = item["Value"]

        return rollup_value