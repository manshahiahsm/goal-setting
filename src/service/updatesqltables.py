from src.helper.salesforce import SalesForce
import src.helper.sql_connection
import traceback


class UpdateSqlTables:

    def __init__(self, tenant_name, my_logger):

        self.tenant_name = tenant_name
        self.my_logger = my_logger
        self.admin_db = src.helper.sql_connection.connect_to_sql_db(self.tenant_name, 'processing')

    def run_service(self):
        self.my_logger.debug(f"--- Inside run service methods --- ")
        SalesForce.connect_to_sfdc(self.tenant_name, self.my_logger)
        plan_execution_list = SalesForce.query(f""" Select Id,plan_uuid__c from t_plan_execution__c Where Tenant__c = '{self.tenant_name}' and (Type__c='Quota Setting' or Type__c='Simulation') """)
        cursor = self.admin_db.cursor()
        status = None
        summary = None
        try:
            if not plan_execution_list.empty:
                plan_execution_list.columns = ['Id', 'plan_uuid__c']
                for _, row in plan_execution_list.iterrows():
                    scenario_uuid = str(row['plan_uuid__c'])
                    self.my_logger.debug(f"---- processing Scenario ----- ")
                    self.my_logger.debug(f"{scenario_uuid}")
                    table_name = 'IC_Output.[t_'+scenario_uuid+']'
                    query = 'Exec sp_Quota_setting_table_update \''+table_name+'\''
                    self.my_logger.debug(f"----- Query statement ------ ")
                    self.my_logger.debug(f"{query}")
                    cursor.execute(query)

                status = 'Success'
                summary = 'Tables successfully updated.'
                self.my_logger.debug(f"---- Tables updated successfully ---- ")
            else:
                status = 'Success'
                summary = 'No Scenario found.'
        except Exception as e:
            self.my_logger.debug(f"--- Unable to update table --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            status = 'failed'
            summary = 'Unable to update table. Check log.'
        finally:
            self.admin_db.commit()
            cursor.close()
            self.admin_db.close()
            return status, summary
