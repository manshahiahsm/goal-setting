import traceback
import os
import json
import pandas as pd
import uuid
import src.helper.sql_connection
from sqlalchemy import text
from src.helper.config import rp_config
from src.helper.s3 import S3
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce
from json import JSONDecodeError


class PushToQuotaMaster:

    def __init__(self, tenant, my_logger):
        self.tenant = tenant
        self.my_logger = my_logger
        self.admin_db = src.helper.sql_connection.connect_to_sql_db(self.tenant, 'processing')
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")

    def update_quota_master_definition(self, parent_uuid, input_columns, input_column_type, record_id=None):
        status = 'Success'
        try:
            input_columns = input_columns + ",Date_Period,Data_Value"
            input_column_type = input_column_type + ",Date,Number"
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\QM_listing.txt"
            file_exist = os.path.exists(file_location)
            file_data = {}
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug("--- file created ---")
            else:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            find_keys = False
            for key, value in file_data.items():
                if key == parent_uuid:
                    find_keys = True

            if not find_keys:
                file_data[parent_uuid] = dict()
                file_data[parent_uuid]["input_columns"] = input_columns
                file_data[parent_uuid]["input_column_type"] = input_column_type
                with open(file_location, 'w+') as obj:
                    self.my_logger.debug(f"--- inside writing file method ----")
                    json.dump(file_data, obj, indent=4)
                status = 'Success'
        except Exception as e:
            status = 'Failed'
            self.my_logger.debug(f"---- Unable to update Quota Master Definition file ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            value = record_id if record_id is not None else parent_uuid
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: update_quota_master_definition method', value)

        return status

    def quota_settings_to_quota_master(self, input_uuid, scenario_name, source, pushed_on, output_uuid, comment, primary_keys, pushed_by, execution_date, parent_uuid, view_at='', parent_uuid_qs=''):
        self.my_logger.info(f" --- inside quota_settings_to_quota_master --- ")
        admin_cursor = self.admin_db.cursor()
        comment = comment.replace("'", "''''")
        source = "Quota Setting"
        view_at = view_at.strip()
        if view_at:
            rollup_value = self.getSetGoalValue(input_uuid, parent_uuid_qs)
            rollup_value = rollup_value.strip()
            if rollup_value is not None and view_at.lower() != rollup_value.lower():
                input_uuid = input_uuid + "_rollup"
        self.my_logger.debug(f"---- updated input uuid ---- {input_uuid}")
        if not comment.strip():
            self.my_logger.info(f"----- NULL Comments ------- ")
            comment = ''
        # IQ-556
        input_columns, input_column_type, unique_columns, apportioning_columns, sfdc_columns, data_types, qr_columns, master_columns, master_data_types = self.update_quota_master_listing(output_uuid)
        definition_status = self.update_quota_master_definition(parent_uuid, master_columns, master_data_types)
        summary = 'Unable to push data in Quota Master. Please contact System Admin.'
        if definition_status.lower() == 'success':
            instance_status = self.update_quota_master_definition(output_uuid, master_columns, master_data_types)
            if instance_status.lower() == 'success':
                # IQ-556
                data_type_list = master_data_types.split(",")
                updated_data_type = ''
                for data_type in data_type_list:
                    if data_type.lower() == "text":
                        updated_data_type = updated_data_type + 'VARCHAR(4000)|'
                    elif data_type.lower() == "number":
                        updated_data_type = updated_data_type + 'NUMERIC(25,13)|'
                    elif data_type.lower() == "date":
                        updated_data_type = updated_data_type + 'date|'
                    else:
                        updated_data_type = updated_data_type + 'VARCHAR(4000)|'

                updated_data_type = updated_data_type[0:-1]
                self.my_logger.debug(f"--- update data types --- ")
                self.my_logger.debug(f"{updated_data_type}")
                query = "Exec sp_Quota_Master_Data_Load '"+source+"','"+input_uuid+"','"+scenario_name+"','"+pushed_on+"','"+output_uuid+"','"+input_columns+"','"+comment+"','"+primary_keys+"','"+unique_columns+"','"+pushed_by+"','"+execution_date+"','"+apportioning_columns+"','Data_Value','"+master_columns+"','"+updated_data_type+"'"
                self.my_logger.info(f"---query ----- {query}")
                status = 'Success'
                summary = 'Successfully executed.'
                try:
                    admin_cursor.execute(query)
                    result = admin_cursor.fetchone()
                    message = result[0]
                    self.my_logger.debug(f"----- message ----- ")
                    self.my_logger.debug(f"{message}")
                    result_list = message.split(";")
                    if len(result_list) >= 2:
                        status = result_list[0]
                        summary = result_list[1]
                    elif len(result_list) == 1:
                        status = result_list[0]
                        summary = ''
                    self.admin_db.commit()
                    if status.lower() == 'success':
                        summary = 'The Process has been completed successfully'
                    else:
                        summary = 'Unable to push data in Quota Master. '+summary
                except Exception as e:
                    self.my_logger.debug(f"---- unable to execute query ---- ")
                    self.my_logger.error(e.args)
                    self.my_logger.exception(traceback.format_exc())
                    SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                    SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: PushToQuotaMaster API', input_uuid)
                    status = 'Failed'
                    summary = 'Unable to push data in Quota Master. Please contact System Admin.'
                finally:
                    admin_cursor.close()
                    self.admin_db.close()
                    self.my_logger.debug(f"------ Status ----- ")
                    self.my_logger.debug(f"{status}")
                    self.my_logger.debug(f"---- summary ----- ")
                    self.my_logger.debug(f"{summary}")
                return status, summary
            else:
                return instance_status, summary
        else:
            return definition_status, summary

    def calculate_apportioning(self, periods, periods_val, input_uuid, output_uuid, frequency, execution_date, periods_label):
        self.my_logger.debug(f"---- calculate_apportioning ----- ")
        status = None
        summary = None
        try:
            admin_cursor = self.admin_db.cursor()
            input_table = 'IC_Output.[t_' + input_uuid + ']'
            input_column, input_column_type, unique_columns, column_value, sfdc_columns, data_types, qr_columns, master_columns, master_data_types = self.update_quota_master_listing(output_uuid, input_uuid)
            instance_status = self.update_quota_master_definition(output_uuid, master_columns, master_data_types)
            # Creating Goal Transpose data
            output_table1 = 'IC_Output.[t_' + output_uuid + '_GT]'
            output_table2 = 'IC_Output.[t_' + output_uuid + ']'
            query = 'exec sp_Quota_Master_Apportioning \''+input_table+'\',\''+column_value+'\',\''+periods+'\',\''+periods_val+'\',\''+output_table1+'\''
            self.my_logger.debug(f"---- query statement for GT data ----")
            self.my_logger.debug(f"{query}")
            status = 'Success'
            summary = 'Successfully executed.'
            try:
                admin_cursor.execute(query)
                self.admin_db.commit()
                result = admin_cursor.fetchone()
                message = result[0]
                self.my_logger.debug(f"----- message ----- ")
                self.my_logger.debug(f"{message}")
                result_list = message.split(";")
                if len(result_list) >= 2:
                    status = result_list[0]
                    summary = result_list[1]
                elif len(result_list) == 1:
                    status = result_list[0]
                    summary = ''
                if status.lower() == 'success':
                    summary = 'The Process has been completed successfully'
                else:
                    summary = 'Unable to push data in Quota Master. ' + summary

                # creating Goal type data
                if status.lower() == 'success':
                    self.my_logger.debug(f"--- frequency --- {frequency}")
                    self.my_logger.debug(f"---- execution date ---- {execution_date}")
                    self.my_logger.debug(f"--- Periods Label ---- {periods_label}")
                    periods_label_list = periods_label.split(",")
                    execution_date = periods_label_list[0]
                    self.my_logger.debug(f"--- updated time series date  {execution_date}")
                    query = "Exec sp_unpivot_table '" + output_table1 + "',NULL,'" + periods + "','Date_Period','Data_Value','" + execution_date + "','" + frequency + "','ASC','" + output_table2 + "','"+periods_label+"'"
                    self.my_logger.debug(f"---- Unpivot query ----- ")
                    self.my_logger.debug(f"{query}")
                    admin_cursor.execute(query)
                    self.admin_db.commit()
                    result = admin_cursor.fetchone()
                    message = result[0]
                    self.my_logger.debug(f"----- unpivot message ----- ")
                    self.my_logger.debug(f"{message}")
                    result_list = message.split(";")
                    if len(result_list) >= 2:
                        status = result_list[0]
                        summary = result_list[1]
                    elif len(result_list) == 1:
                        status = result_list[0]
                        summary = ''
                    if status.lower() == 'success':
                        summary = 'The Process has been completed successfully'
                    else:
                        summary = 'Unable to push data in Quota Master. '+summary

                self.admin_db.commit()
            except Exception as e:
                self.my_logger.debug(f"---- unable to execute query ---- ")
                self.my_logger.error(e.args)
                self.my_logger.exception(traceback.format_exc())
                SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: CallApportioning API', input_uuid)
                status = 'Failed'
                summary = 'Unable to create an apportioning instance. Please contact System Admin.'
            finally:
                admin_cursor.close()
                self.admin_db.close()
        except Exception as e:
            self.my_logger.debug(f"---- error in calculate_apportioning  ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: CallApportioning API', input_uuid)
            status = 'Failed'
            summary = 'Unable to create an apportioning instance. Please contact System Admin.'

        return status, summary

    def update_quota_master_listing(self, scenario_uuid, input_uuid=None, record_id=None):
        self.my_logger.debug(f"--- Inside update_quota_master_listing methods --- ")
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Instance_Info.txt"
            file_exist = os.path.exists(file_location)
            file_data = {}
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug("  file created ")
            else:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            find_key = False
            unique_columns = None
            input_columns = None
            input_columns_type = None
            apportioning_columns = None
            sfdc_columns = None
            data_types = None
            qr_columns = None
            column_labels = None
            filter_columns = None
            filter_labels = None
            # IQ-556
            master_columns = None
            master_data_types = None
            for key, value in file_data.items():
                if key == scenario_uuid:
                    unique_columns = file_data[scenario_uuid]["unique_columns"]
                    input_columns = file_data[scenario_uuid]["input_columns"]
                    input_columns_type = file_data[scenario_uuid]["input_columns_type"]
                    apportioning_columns = file_data[scenario_uuid]["apportioning_columns"]
                    sfdc_columns = file_data[scenario_uuid]["sfdc_columns"]
                    data_types = file_data[scenario_uuid]["data_types"]
                    qr_columns = file_data[scenario_uuid]["qr_columns"]
                    # IQ-556
                    master_columns = file_data[scenario_uuid]["master_columns"]
                    master_data_types = file_data[scenario_uuid]["master_data_types"]
                    find_key = True
                    break

            if not find_key:
                if input_uuid is None:
                    input_columns = self.config_obj.get_value_from_key('Quota_Master', 'columns')
                    input_columns_type = self.config_obj.get_value_from_key('Quota_Master', 'columns_type')
                    column_labels = self.config_obj.get_value_from_key('Quota_Master', 'labels')
                    unique_columns = self.config_obj.get_value_from_key('Quota_Master', 'qm_unique_column')
                    apportioning_columns = self.config_obj.get_value_from_key('Quota_Master', 'apportioning_column')
                    sfdc_columns = self.config_obj.get_value_from_key('Quota_Master', 'sfdc_columns')
                    data_types = self.config_obj.get_value_from_key('Quota_Master', 'data_types')
                    qr_columns = self.config_obj.get_value_from_key('Quota_Master', 'qr_columns')
                    filter_columns = self.config_obj.get_value_from_key('Quota_Master', 'filter_columns')
                    filter_labels = self.config_obj.get_value_from_key('Quota_Master', 'filter_labels')
                    # IQ-556
                    master_columns = self.config_obj.get_value_from_key('Quota_Master', 'master_columns')
                    master_data_types = self.config_obj.get_value_from_key('Quota_Master', 'master_data_types')
                else:
                    for key, value in file_data.items():
                        if key == input_uuid:
                            unique_columns = file_data[input_uuid]["unique_columns"]
                            input_columns = file_data[input_uuid]["input_columns"]
                            input_columns_type = file_data[input_uuid]["input_columns_type"]
                            apportioning_columns = file_data[input_uuid]["apportioning_columns"]
                            sfdc_columns = file_data[input_uuid]["sfdc_columns"]
                            data_types = file_data[input_uuid]["data_types"]
                            qr_columns = file_data[input_uuid]["qr_columns"]
                            column_labels = file_data[input_uuid]["labels"]
                            filter_columns = file_data[input_uuid]["filter_columns"]
                            filter_labels = file_data[input_uuid]["filter_labels"]
                            # IQ-556
                            master_columns = file_data[input_uuid]["master_columns"]
                            master_data_types = file_data[input_uuid]["master_data_types"]

                file_data[scenario_uuid] = dict()
                file_data[scenario_uuid]["input_columns"] = input_columns
                file_data[scenario_uuid]["input_columns_type"] = input_columns_type
                file_data[scenario_uuid]["unique_columns"] = unique_columns
                file_data[scenario_uuid]["apportioning_columns"] = apportioning_columns
                file_data[scenario_uuid]["labels"] = column_labels
                file_data[scenario_uuid]["sfdc_columns"] = sfdc_columns
                file_data[scenario_uuid]["data_types"] = data_types
                file_data[scenario_uuid]["qr_columns"] = qr_columns
                file_data[scenario_uuid]["filter_columns"] = filter_columns
                file_data[scenario_uuid]["filter_labels"] = filter_labels
                # IQ-556
                file_data[scenario_uuid]["master_columns"] = master_columns
                file_data[scenario_uuid]["master_data_types"] = master_data_types

                with open(file_location, 'w+') as obj:
                    self.my_logger.debug(f"--- inside writing file method ----")
                    json.dump(file_data, obj, indent=4)

            self.my_logger.debug(f"--- input_columns --- ")
            self.my_logger.debug(f"{input_columns}")
            self.my_logger.debug(f" --- unique columns --- ")
            self.my_logger.debug(f"{unique_columns}")
            self.my_logger.debug(f"--- apportioning_columns --- ")
            self.my_logger.debug(f"{apportioning_columns}")
            self.my_logger.debug(f"---- column labels ---- ")
            self.my_logger.debug(f"{column_labels}")
            self.my_logger.debug(f"---- SFDC Columns ----- ")
            self.my_logger.debug(f"{sfdc_columns}")
            self.my_logger.debug(f"---- data Types ---- ")
            self.my_logger.debug(f"{data_types}")
            self.my_logger.debug(f"--- QR columns ----- ")
            self.my_logger.debug(f"{qr_columns}")
            self.my_logger.debug(f"--- filter_columns ----- ")
            self.my_logger.debug(f"{filter_labels}")
            self.my_logger.debug(f"--- filter_labels ----- ")
            self.my_logger.debug(f"{filter_labels}")
            self.my_logger.debug(f"--- master_columns ----- ")
            self.my_logger.debug(f"{master_columns}")
            self.my_logger.debug(f"--- master_data_types ----- ")
            self.my_logger.debug(f"{master_data_types}")
            return input_columns, input_columns_type, unique_columns, apportioning_columns, sfdc_columns, data_types, qr_columns, master_columns, master_data_types
        except Exception as e:
            self.my_logger.debug(f"--- error while updating quota master file --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            value = record_id if input_uuid is None else input_uuid
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: update_quota_master_listing method', value)
            return None, None, None, None, None, None, None, None, None

    def update_quota_master_instance(self, scenario_uuid, periods, periods_val, periods_label):
        self.my_logger.debug(f"--- Inside update_quota_master_instance methods --- ")
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Apportioning_Info.txt"
            file_exist = os.path.exists(file_location)
            file_data = {}
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug("  file created ")
            else:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            find_key = False
            for key, value in file_data.items():
                if key == scenario_uuid:
                    find_key = True
                    break

            if not find_key:
                file_data[scenario_uuid] = dict()
                file_data[scenario_uuid]["periods"] = periods
                file_data[scenario_uuid]["periods_values"] = periods_val
                file_data[scenario_uuid]["periods_label"] = periods_label

            with open(file_location, 'w+') as obj:
                self.my_logger.debug(f"--- inside writing file method ----")
                json.dump(file_data, obj, indent=4)

            return 'Success', 'file updated successfully'
        except Exception as e:
            self.my_logger.debug(f"--- error while updating quota master file --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: update_quota_master_instance method', scenario_uuid)
            return 'Failed', 'Unable to update Instance file.'

    def get_periods_column(self, output_uuid):
        parent_dir = self.main_dir
        file_location = parent_dir + "\\" + self.tenant + "\\QM_Apportioning_Info.txt"
        with open(file_location) as json_file:
            file_data = json.load(json_file)
        periods = None
        periods_values = None
        periods_label = None
        find_key = False
        for key, value in file_data.items():
            if key == output_uuid:
                periods = file_data[key]["periods"]
                periods_values = file_data[key]["periods_values"]
                periods_label = file_data[key]["periods_label"]
                find_key = True

        if not find_key:
            return 'Failed', None, None, None
        else:
            return 'Success', periods, periods_values, periods_label

    def refresh_apportioning(self, input_uuid, output_uuid, frequency, execution_date):
        try:
            status, periods, periods_values, periods_label = self.get_periods_column(output_uuid)
            if status.lower() == 'failed':
                return 'Failed', 'Unable to find periods and values.'
            else:
                self.my_logger.debug(f"--- updated time series date {execution_date} ")
                status, summary = self.calculate_apportioning(periods, periods_values, input_uuid, output_uuid, frequency, execution_date, periods_label)
                return status, summary
        except Exception as e:
            self.my_logger.debug(f"--- error while reading quota master instance file --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: RefreshApportioning API', output_uuid)
            return 'Failed', 'Unable to read quota master instance file.'

    def get_sql_table_data(self, scenario_uuid, scenario_name, clause, download_flag):
        """
        This method fetch sql data for quota master instance and upload it as zip file on S3
        :param scenario_uuid: quota master uuid
        :param scenario_name: quota master name
        :param clause: where clause
        :param download_flag: flag for direct download or normal download
        :return: status whether it is uploaded successfully or not
        """
        self.my_logger.debug(f" --- Inside get_sql_table_data --- ")
        status = 'Failed'
        admin_engine = None
        try:
            input_labels = ''
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Instance_Info.txt"
            file_data = ''
            try:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
            except Exception as e:
                raise
            for key, value in file_data.items():
                if key == scenario_uuid:
                    input_labels = file_data[key]["labels"]

            period_labels = None
            file_location = parent_dir + "\\" + self.tenant + "\\QM_Apportioning_Info.txt"
            file_exist = False
            if os.path.exists(file_location):
                file_exist = True
            App_Instance = False
            if file_exist:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
                    for key, value in file_data.items():
                        if key == scenario_uuid:
                            App_Instance = True
                            period_labels = file_data[key]["periods_label"]
            self.my_logger.debug(f"---- download flag ---- {download_flag}")
            if download_flag == '1':
                input_labels = input_labels + ',Date Period,Data Value'
            else:
                if period_labels is not None:
                    input_labels = input_labels + "," + period_labels
            exclude_columns = self.config_obj.get_value_from_key('Quota_Master', 'exclude_columns')
            exclude_columns_list = exclude_columns.split(",")
            temp_uuid = str(uuid.uuid1())
            zip_file_path = parent_dir + "\\goal-setting\\tmp\\GoalSettings" + temp_uuid + ".csv.gz"
            admin_engine = src.helper.sql_connection.createEngineToAdminDB(self.tenant, 'processing')
            table_name = 'IC_Output.[t_'+scenario_uuid+']'
            if App_Instance and download_flag == '0':
                table_name = 'IC_Output.[t_'+scenario_uuid+'_GT]'

            query = None
            query = "Select * from "+table_name+" "+clause
            self.my_logger.debug(f"---- query ---- ")
            self.my_logger.debug(f"{query}")

            df = pd.read_sql_query(text(query), admin_engine)
            self.my_logger.debug(f"---- input labels---- ")
            self.my_logger.debug(f"{input_labels}")
            if download_flag == '0':
                df.drop(exclude_columns_list, axis=1, inplace=True)
            input_labels_list = input_labels.split(",")
            df.columns = input_labels_list
            try:
                df.drop(['ID'], axis=1, inplace=True)
            except Exception as e:
                df.drop(['Id'], axis=1, inplace=True)

            df.to_csv(zip_file_path, compression='gzip', index=False)
            status = self.upload_zip_file_s3(zip_file_path, scenario_name, scenario_uuid)
        except Exception as e:
            raise
        finally:
            admin_engine.dispose()

        return status

    def upload_zip_file_s3(self, file_path, scenario_name, scenario_uuid):
        status = 'Failed'
        try:
            try:
                S3.connect_to_s3(self.tenant)
            except Exception as e:
                raise
            self.my_logger.debug(f"--- S3 connection established ---")
            self.my_logger.debug(f"---- scenario name ---- {scenario_name}")
            file_name = scenario_name+'.csv.gz'
            s3_bucket = self.config_obj.get_value_from_key("s3", "s3_bucket")
            s3path = rp_config['s3']['goalSettingPath'].format(bucket=s3_bucket, tenant=self.tenant, uuid=scenario_uuid, filename=file_name)
            self.my_logger.debug(f" --- S3 Path ---  ")
            self.my_logger.debug(f"{s3path}")
            S3.upload_file_from_local_path(file_path, s3path)
            status = 'Success'
        except Exception as e:
            raise

        return status

    def getSetGoalValue(self, scenario_uuid, parent_uuid):
        folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + scenario_uuid
        file_name = scenario_uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        data = dict()
        with open(file_loc) as json_file:
            data = json.load(json_file)
        file_data = data["Parameters"]

        rollup_value = None
        for item in file_data:
            for key, value in item.items():
                if key.lower() == "sfdc" and value.lower() == "calculate_goal_level__c":
                    rollup_value = item["Value"]

        return rollup_value
