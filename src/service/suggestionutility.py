import json
import traceback
from json import JSONDecodeError

from src.service.updateplanfile import UpdatePlanFile
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce


class SuggestionUtility:
    def __init__(self, tenant, my_logger):
        self.tenant = tenant
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.my_logger = my_logger

    # Method to read suggestion json file
    def get_rules_json(self, parent_uuid, sc_uuid):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + sc_uuid
        system_file_name = 'suggestion_json_data.txt'
        system_file_loc = folder_loc + '\\' + system_file_name
        data = dict()
        try:
            with open(system_file_loc, 'r') as json_obj:
                data = json.load(json_obj)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', system_file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', system_file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return data

    def update_rule_parameters(self, parent_uuid, sc_uuid, plan_uuid):
        self.my_logger.info(f"--- Inside update_rule_parameters method --- ")
        rules_dict = self.get_rules_dict(parent_uuid, sc_uuid)
        plan_file_obj = UpdatePlanFile(self.tenant, plan_uuid, self.my_logger, sc_uuid, parent_uuid, '')
        plan_file_data = plan_file_obj.read_plan_file()
        data_dict = plan_file_data[plan_uuid]
        self.my_logger.info(f"--- After reading plan file ---")
        status = "Success"
        try:
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    component_list = data_dict[key]
                    for component in component_list:
                        for k, v in component.items():
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                if comp_key.lower() == 'measure_parameters':
                                    measure_param = component_dict[comp_key]
                                    for param_list in measure_param:
                                        param_name = param_list["Info_Name"]["Value"]
                                        if param_name.find("rule") != -1 or param_name.find("Rule") != -1:
                                            # wight param update
                                            if param_name.find("weight") != -1 or param_name.find("Weight") != -1:
                                                column_key_list = param_name.split("_", 1)
                                                column_key = column_key_list[0]
                                                column_key = column_key[1:]
                                                self.my_logger.info(f"--- column_key {column_key}")
                                                param_list["Param_ValueOfParameter"] = rules_dict[column_key]["Weightage"]
                                            # value update
                                            elif param_name.find("value") != -1 or param_name.find("Value") != -1:
                                                column_key_list = param_name.split("_", 1)
                                                column_key = column_key_list[0]
                                                column_key = column_key[1:]
                                                self.my_logger.info(f"--- column_key {column_key}")
                                                param_list["Param_ValueOfParameter"] = rules_dict[column_key]["Value"]
                                elif comp_key.lower() == 'resultants':
                                    join_dict = component_dict[comp_key]
                                    for join_data_key, join_data_value in join_dict.items():
                                        join_data_dict = join_dict[join_data_key]
                                        for jk, jv in join_data_dict.items():
                                            table_list = join_data_dict[jk]
                                            if jk.lower() == 'tables':
                                                for table_data in table_list:
                                                    if 'List_of_Dps' in table_data:
                                                        list_of_dps = table_data["List_of_Dps"]
                                                        for dp_dict in list_of_dps:
                                                            for dp_key, dp_value in dp_dict.items():
                                                                
                                                                operation_name = dp_dict[dp_key]["Info_Type"]["Value"]
                                                                if operation_name.lower() == 'computation':
                                                                    dp_name = dp_dict[dp_key]["BL_Name"]["Value"]
                                                                    dp_exp = dp_dict[dp_key]["BL_Expression"]["Value"]
                                                                    if (dp_name.find("rule") != -1 or dp_name.find("Rule") != -1) and (dp_name.find("flag") != -1 or dp_name.find("Flag") != -1):
                                                                        self.my_logger.info(f"---  1 {dp_name}")
                                                                        self.my_logger.info(f"---  2 {dp_exp}")
                                                                        
                                                                        column_key_list = dp_name.split("_", 1)
                                                                        column_key = column_key_list[0]
                                                                        self.my_logger.info(f"---  3 {column_key}")
                                                                        operator_value = rules_dict[column_key]["Operator"]
                                                                        self.my_logger.debug(f"---- operator value for key {column_key} is {operator_value}")
                                                                        operator_key = "$"+column_key+"_Operator"
                                                                        dp_exp_list = dp_exp.split(" ")
                                                                        dp_exp_list[3] = operator_value
                                                                        dp_exp_updated = " ".join(dp_exp_list)
                                                                        self.my_logger.debug(f"---- updated expression {dp_exp_updated}")
                                                                        dp_dict[dp_key]["BL_Expression"]["Value"] = dp_exp_updated

            self.my_logger.info(f"--- After updating plan file ---")
            status = plan_file_obj.update_plan(data_dict)
            self.my_logger.info(f"--- Plan json file updated ---")
        except Exception as e:
            self.my_logger.info(f" --- error inside update_rule_parameters method ----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
            status = "Failed"

        return status

    def get_rules_dict(self, parent_uuid, sc_uuid):
        rule_key = 'rules_value'
        no_of_rules = self.config_obj.get_value_from_key('Quota_Setting', rule_key)
        suggestion_dict = self.get_rules_json(parent_uuid, sc_uuid)
        rules_list = suggestion_dict["Rules"]
        rules_dict = dict()
        counter = 0
        for rule in rules_list:
            counter = counter + 1
            column_name = rule["Column"]
            column_key_list = column_name.split("_",1)
            column_key = column_key_list[0]
            rules_dict[column_key] = dict()
            rules_dict[column_key]["Operator"] = rule["Operator"]
            rules_dict[column_key]["Weightage"] = rule["Rule Weightage"]
            rules_dict[column_key]["Value"] = rule["Value"]

        if counter < int(no_of_rules):
            counter = 1
            while counter <= int(no_of_rules):
                column_key = 'Rule'+str(counter)
                if not column_key in rules_dict.keys():
                    rules_dict[column_key] = dict()
                    rules_dict[column_key]["Operator"] = '='
                    rules_dict[column_key]["Weightage"] = '0'
                    rules_dict[column_key]["Value"] = '100'
                counter = counter + 1

        self.my_logger.debug(f"--------- rules dict -------- ")
        self.my_logger.debug(rules_dict)
        return rules_dict

    def get_rules_column(self, parent_uuid, sc_uuid):
        suggestion_dict = self.get_rules_json(parent_uuid, sc_uuid)
        self.my_logger.info(f" --- suggestion dict ---- {suggestion_dict}")
        rules_list = suggestion_dict["Rules"]
        column_list = []
        for rule in rules_list:
            column_name = rule["Column"]
            column_list.append(column_name)
        return column_list
    
    # Method to save suggestion instance json file
    def save_suggestion_instances(self, parent_uuid, sc_uuid, instance_json):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + sc_uuid
        system_file_name = 'suggestion_instance_list.txt' 
        system_file_loc = folder_loc + '\\' + system_file_name
        status = None
        try:
            with open(system_file_loc, 'w+') as file_obj:
                json.dump(instance_json, file_obj, indent=4)
                status = 'Success'
        except Exception as e:
            self.my_logger.debug(f"--- unable to read system suggestion file ---")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: save_suggestion_instances method', sc_uuid)
            status = 'Failed'
        return status
    
    def get_plan_params(self, parent_uuid, sc_uuid):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + sc_uuid
        system_file_name = 'suggestion_instance_list.txt' 
        system_file_loc = folder_loc + '\\' + system_file_name
        try:
            data = dict()
            with open(system_file_loc, 'r') as json_obj:
                data = json.load(json_obj)
            
            first_plan_uuid = data["first_plan_uuid"]
            second_plan_uuid = data["second_plan_uuid"]
            first_plan_parent_uuid = data["first_plan_parent_uuid"]
            second_plan_parent_uuid = data["second_plan_parent_uuid"]

            return 'success', first_plan_uuid, second_plan_uuid, first_plan_parent_uuid, second_plan_parent_uuid
        except Exception as e:
            self.my_logger.info(f"--- unable to read plan params for suggestions --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sc_uuid)
            return 'failed', '', '', '', ''
