import configparser
# Path will be updated as per '__main__' file location

ahc_config = configparser.ConfigParser()
ahc_config._interpolation = configparser.ExtendedInterpolation()
ahc_config.optionxform = str
ahc_config.read('./src/config.ini')

rp_config = configparser.ConfigParser()
rp_config._interpolation = configparser.ExtendedInterpolation()
rp_config.optionxform = str
rp_config.read('./src/config.ini')

