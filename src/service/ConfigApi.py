import os
import json
from src.helper.ParseJSON import ParseJSON
import logging


class Config_Api(object):

    def __init__(self, tenant_name=""):
        self.rel_path = "C:\\Static_Resources\\"
        self.tenantName = tenant_name
        self.path = self.rel_path + self.tenantName.upper() + "_config.json"
        if os.path.exists(self.path):
            self.jsn = {}
            with open(self.path) as f:
                self.jsn = json.load(f)
            f.close()
        else:
            raise FileNotFoundError

    def get_value_from_key(self, outer_key, inner_key):
        control_flag = False
        if inner_key.lower() == "":
            return self.jsn[outer_key]
        else:
            outer_key = outer_key
            inner_key = inner_key
            if outer_key in self.jsn:
                outer_arr = self.jsn[outer_key]
                for el in outer_arr:
                    for (k, v) in el.items():
                        if k == inner_key:
                            control_flag = True
                            return v
                if not control_flag:
                    raise Exception('KeyError', inner_key)
            else:
                raise Exception('KeyError', outer_key)

    def update_value_from_key(self, outer_key, inner_key, value):
        outer_key = outer_key.lower()
        inner_key = inner_key.lower()
        if outer_key in self.jsn:
            outer_arr = self.jsn[outer_key]
            for dct in outer_arr:
                dct_copy = dct.copy()
                k_arr = list(dct_copy.keys())
                if k_arr[0] == inner_key:
                    dct_copy[inner_key] = value
                    dct.update(dct_copy)
            with open(self.path, 'w') as f:
                json.dump(self.jsn, f)
        else:
            raise KeyError

    def returnJSON(self, outer_key, inner_key):
        PJ=ParseJSON()
        logging.debug('keys'+outer_key+'.'+inner_key)
        logging.debug(PJ.GetValueFromKeyPath(self.jsn,outer_key+'.'+inner_key))
        return PJ.GetValueFromKeyPath(self.jsn,outer_key+'.'+inner_key)

    def helper_1(self, arr, el):
        for i in range(len(arr)):
            if el == arr[i]:
                return True
        return False

    def get_mapping_file_location(self):
        if os.path.exists(self.rel_path + "\\ErrorMapping.txt"):
            return self.rel_path + "\\ErrorMapping.txt"
        else:
            return None
