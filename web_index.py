"""
@author: Aman Kumar <aman.kumar@axtria.com>
"""
import json
import uuid
import urllib.parse
import traceback
import requests
import logging
import sys
import time
import datetime
import gc
import os
import psutil
import jwt
from datetime import datetime, timedelta
from werkzeug.security import check_password_hash
from src.service.ConfigApi import Config_Api
from src.helper.helpers import secret
from functools import wraps
from multiprocessing import Process
from logging.handlers import TimedRotatingFileHandler
from urllib import request, parse
from flask import Flask, request, jsonify
from src.helper.config import ahc_config
from src.service.readuifile import ReadUiFile
from src.service.saveuifile import SaveUiFile
from src.service.readdata import ReadSqlData
from src.service.CreateCopy import CreateCopy
from src.service.updateplanfile import UpdatePlanFile
from src.helper.salesforce import SalesForce
from src.service.readdatafromsql import ReadDataFromSql
from src.service.suggestionutility import SuggestionUtility
from src.service.pushtoquotamaster import PushToQuotaMaster
from src.service.updatesqltables import UpdateSqlTables
from src.service.pushtoqmfromrefinement import RefinementToQuotaMaster
from src.service.rolluputility import RollupUtility
from src.service.quota_refinement_job import QuotaRefinementJob
from src.service.download_log_file import  DownloadLogFile
from src.service.download_qr_file import DownloadQRFile

path = ahc_config['web_service']['plan_directory']
sys.path.insert(1, path)

from TemplatesIC.Code.DPCalculation import DPCalculation
from TemplatesIC.Code.DPCalculation import DPCalculation
from TemplatesIC.Code.EditInstance import EditInstance
from TemplatesIC.Code.ExecutePlan import ExecutePlan
from TemplatesIC.Code.InstanceCreator import InstanceCreator
from TemplatesIC.Code.ParseJSON import ParseJSON
from TemplatesIC.Code.UIFileCreatorForIndividual import UIFileCreatorForIndividualTemplate
from TemplatesIC.Code.PlanBuilderSQL import PlanBuilderSQL
from TemplatesIC.Code.PlanUpdateSFDC import PlanUpdateSFDC
from TemplatesIC.Code.AdapterFunctions import AdapterFunctions
from TemplatesIC.Code.AddDeleteJoin import AddDeleteJoin
from TemplatesIC.Code.webrequest import Webrequest

app = Flask(__name__)
PREFIX = ahc_config['web_service']['prefix_run_execution']

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("tmp\\logs.txt", when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(levelname)s - %(message)s')

os.chdir(path)
from TemplatesIC.Code.script_to_read_csv_to_proc_Call import callSQLExecution

def token_required(f):
    """
    This method validates token received by an API
    :param f: function as arguement
    :return:
    """
    @wraps(f)
    def decorated(*args, **kwargs):
        token = __resolve_token(request)
        if not token:
            return jsonify({'message': '', 'traceback': 'Token is missing'}), 403
        try:
            data = jwt.decode(token, secret)
        except Exception as e:
            return jsonify({'message': '', 'traceback': 'Token is invalid'}), 403
        return f(*args, **kwargs, token=token)

    return decorated


@app.route(PREFIX + "/")
def test_service():
    """
    This is a test method to check whether service is working or not.
    :return: Constant String.
    """
    return "Service is working fine"


@app.route(PREFIX + "/test")
@token_required
def test_token(token):
    """
    This is a test method to check whether service is working or not.
    :param token: token for authentication
    :return: token passed in url
    """
    return "inside test function - %r" % token


@app.route(PREFIX + "/getuuid", methods=['POST'])
@token_required
def get_uuid(token):
    """
    this method returns UUID value
    :param token: token for authentication
    :return: UUID string
    """
    uuid_value = str(uuid.uuid1())
    final_data = {'Status':'Success', 'Data':uuid_value}
    return jsonify(final_data)


@app.route(PREFIX + "/readuifile", methods=['POST'])
@token_required
def read_ui_file(token):
    """
    This method returns UI file for New QS object
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- Inside read_ui_file method --------")
    logging.debug(f"---- {token}-----")
    tenant = request.form.get('tenant', type=str)
    final_data = {}
    try:
        read_ui = ReadUiFile(tenant, logging)
        response = read_ui.run_service()
        final_data = {'Status': 'Success', 'Data': response}
    except Exception as e:
        logging.debug("--- Error in ReadUiFile API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadUiFile API', tenant)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/readuivalue', methods=['POST'])
@token_required
def read_ui_value(token):
    """
    This method returns saved UI file for an quota object
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- Inside read_ui_value method --------")
    tenant = request.form.get('tenant')
    sc_uuid = request.form.get('uuid')
    final_data = {}
    try:
        read_value_file = ReadUiFile(tenant, logging)
        response = read_value_file.read_ui_value_file(sc_uuid)
        final_data = {'Status': 'Success', 'Data': response}
    except Exception as e:
        logging.debug("--- Error in ReadUiValue API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadUiValue API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id
    return jsonify(final_data)


@app.route(PREFIX + '/readsimulationfile', methods=['POST'])
@token_required
def read_simulation_file(token):
    """
    This method return simulation UI file data
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- Inside read_simulation_file method --------")
    tenant = request.form.get('tenant')
    parent_uuid = request.form.get('uuid')
    final_data = {}
    try:
        read_simulation_ui = ReadUiFile(tenant, logging)
        response = read_simulation_ui.read_simulation_ui_file(parent_uuid)
        final_data = {'Status': 'Success','Data': response}
    except Exception as e:
        logging.debug("--- Error in ReadSimulationFile API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadSimulationFile API', parent_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/readsimulationvalue', methods=['POST'])
@token_required
def read_simulation_ui_value(token):
    """
    This method returns saved json file for quota instance
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- Inside read_simulation_ui_value method --------")
    tenant = request.form.get('tenant')
    parent_uuid = request.form.get('parent_uuid')
    sc_uuid = request.form.get('uuid')
    final_data = {}
    try:
        read_simulation_value = ReadUiFile(tenant, logging)
        response = read_simulation_value.read_simulation_ui_value(sc_uuid, parent_uuid)
        final_data = {'Status': 'Success', 'Data': response}
    except Exception as e:
        logging.debug("--- Error in ReadSimulationValue API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadSimulationValue API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/saveuifile", methods=['POST'])
@token_required
def save_ui_file(token):
    """
    This method save UI json file for quota object
    :param token: token for authentication
    :return: status whether it is saved successfully or not
    """
    logging.debug("--- Inside save_ui_file method -----")
    sfdc_id = request.form.get('id')
    tenant = urllib.parse.unquote(request.form.get('tenant'))
    uuid_param = urllib.parse.unquote(request.form.get('uuid'))
    data = urllib.parse.unquote_plus(request.form.get('data'), 'UTF-8', 'strict')
    final_data = dict()
    try:
        data = json.loads(data)
        save_ui = SaveUiFile(uuid_param, tenant, logging, data)
        response = save_ui.run_service()
        if response.lower() == 'success':
            SalesForce.connect_to_sfdc(tenant, logging)
            SalesForce.update('t_plan__c', sfdc_id, {'Status__c': 'Complete'}, logging)
            final_data = {'Status': 'Success', 'Data': 'Complete'}
        else:
            SalesForce.connect_to_sfdc(tenant, logging)
            SalesForce.update('t_plan__c', sfdc_id, {'Status__c': 'Incomplete'}, logging)
            final_data = {'Status': 'Success', 'Data': 'Incomplete'}
    except Exception as e:
        logging.debug("--- Error in saveuifile API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Incomplete'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadSimulationValue API', sfdc_id)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/savesimulationfile', methods=['POST'])
@token_required
def save_simulation_file(token):
    """
    This method saves UI json file for quota instance
    :param token: token for authentication
    :return: status whether it is saved successfully or not
    """
    logging.debug("--- Inside save_simulation_file method -----")
    tenant = request.form.get('tenant')
    uuid_param = request.form.get('uuid')
    parent_uuid = request.form.get('parent_uuid')
    sfdc_id = request.form.get('id')
    sc_type = request.form.get('sc_type')
    data = urllib.parse.unquote_plus(request.form.get('data'), 'UTF-8', 'strict')
    sys_data = urllib.parse.unquote_plus(request.form.get('suggest_data'), 'UTF-8', 'strict')
    final_data = dict()
    try:
        data = json.loads(data)
        sys_data = json.loads(sys_data)
        save_simulation_ui_file = SaveUiFile(uuid_param, tenant, logging, data)
        response = save_simulation_ui_file.save_simulation_file(parent_uuid, sc_type, sys_data)
        if response.lower() == 'success':
            final_data = {'Status': 'Success', 'Data': 'Complete'}
        else:
            final_data = {'Status': 'Success', 'Data': 'Incomplete'}
    except Exception as e:
        logging.debug(f"---- Error in SaveSimulationFile API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Incomplete'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: SaveSimulationFile API', uuid_param)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/createcopy', methods=['POST'])
@token_required
def create_copy(token):
    """
    This method creates copy of quota object
    :param token: token for authentication
    :return: status whether copy created successfully or not
    """
    logging.info("--- Inside create copy method ----")
    tenant = request.form.get('tenant', type=str)
    old_uuid = request.form.get('old_uuid', type=str)
    new_uuid = request.form.get('new_uuid', type=str)
    sfdc_id = request.form.get('id', type=str)
    name = request.form.get('name', type=str)
    final_data = {}
    try:
        file_copy = CreateCopy(tenant, old_uuid, new_uuid, logging, name)
        response = file_copy.create_parent_copy('Parent', '', '', '')
        if response.lower() == 'success':
            SalesForce.connect_to_sfdc(tenant, logging)
            SalesForce.update('t_plan__c', sfdc_id, {'Status__c': 'Complete'}, logging)
            final_data = {'Status': 'Success', 'Data': 'Complete'}
        else:
            SalesForce.connect_to_sfdc(tenant, logging)
            SalesForce.update('t_plan__c', sfdc_id, {'Status__c': 'Incomplete'}, logging)
            final_data = {'Status': 'Success', 'Data': 'Incomplete'}
    except Exception as e:
        logging.debug(f"---- Error in SaveSimulationFile API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Incomplete'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: CreateCopy API', new_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/createSimulationCopy', methods=['POST'])
@token_required
def create_simulation_copy(token):
    """
    This method creates copy of quota instance
    :param token: token for authentication
    :return: status whether copy created successfully or not
    """
    logging.info("--- Inside create simulation copy method ----")
    tenant = request.form.get('tenant', type=str)
    old_simulation_uuid = request.form.get('old_simulation_uuid', type=str)
    new_simulation_uuid = request.form.get('new_simulation_uuid', type=str)
    plan__uuid = request.form.get('plan__uuid', type=str)
    sfdc_id = request.form.get('id', type=str)
    name = request.form.get('name', type=str)
    parent_type = request.form.get('ptype', type=str)
    child_type = request.form.get('ctype', type=str)
    final_data = dict()
    try:
        file_copy = CreateCopy(tenant, old_simulation_uuid, new_simulation_uuid, logging, name)
        response = file_copy.create_parent_copy('Child', plan__uuid, parent_type, child_type)
        if response.lower() == 'success':
            SalesForce.connect_to_sfdc(tenant, logging)
            status = 'Complete'
            SalesForce.update('t_plan_execution__c', sfdc_id, {'Definition_Status__c': status}, logging)
            final_data = {'Status': 'Success', 'Data': 'Complete'}
        else:
            SalesForce.connect_to_sfdc(tenant, logging)
            status = 'Incomplete'
            SalesForce.update('t_plan_execution__c', sfdc_id, {'Definition_Status__c': status}, logging)
            final_data = {'Status': 'Success', 'Data': 'Incomplete'}
    except Exception as e:
        logging.debug(f"---- Error in createSimulationCopy API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Incomplete'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: CreateSimulationCopy API', old_simulation_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/dataview', methods=['POST'])
@token_required
def get_quota_data(token):
    """
    This method returns sql data for quota instance
    :param token: token for authentication
    :return: json data
    """
    tenant = request.form.get('tenant', type=str)
    uuid_param = request.form.get('uuid', type=str)
    clause = urllib.parse.unquote_plus(request.form.get('clause', default='', type=str), 'UTF-8', 'strict')
    offset = request.form.get('offset', default='', type=str)
    row_num = request.form.get('rownum', default='', type=str)
    order_by = urllib.parse.unquote_plus(request.form.get('orderby', default='', type=str), 'UTF-8', 'strict')
    view_at = request.form.get('view_at', type=str)
    parent_uuid = request.form.get('parent_uuid', type=str)
    final_data = {}
    try:
        read_sql_data = ReadSqlData(tenant, uuid_param, clause, offset, row_num, order_by, logging, view_at, parent_uuid)
        response = read_sql_data.run_service()
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug(f"---- Error in dataview API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        temp_data = {"data": [{}], "tCount": [{}]}
        final_data['Data'] = json.dumps(temp_data)
        StatusCode= ''
        log_id= ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: DataView API', uuid_param)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/getSuggestions', methods=['POST'])
@token_required
def get_suggestions(token):
    tenant = request.form.get('tenant', type=str)
    period = request.form.get('period')
    uuid_param = request.form.get('sc_uuid')  # scenario__uuid
    scenario_parent_uuid = request.form.get('scenario_parent_uuid')
    re_execute_flag = request.form.get('re_execute')
    token_param = request.form.get('token')
    sfdc_id = request.form.get('sfdc_id')
    first_plan_uuid = ''
    second_plan_uuid = ''
    first_plan_parent_uuid = ''
    second_plan_parent_uuid = ''
    status = ''
    logging.debug("--- inside suggestion method ----")
    config_obj = Config_Api(tenant)
    SalesForce.connect_to_sfdc(tenant, logging)
    try:
        step_key = "rules_step"
        step = config_obj.get_value_from_key('Quota_Setting', step_key)
        step = int(step)
        logging.debug(f"---- step value ----- {step}")
        if re_execute_flag == '0':
            first_plan_uuid = request.form.get('first_plan_uuid')
            second_plan_uuid = request.form.get('second_plan_uuid')
            first_plan_parent_uuid = request.form.get('first_plan_parent_uuid')
            second_plan_parent_uuid = request.form.get('second_plan_parent_uuid')
        else:
            suggestion_obj = SuggestionUtility(tenant, logging)
            status, first_plan_uuid, second_plan_uuid, first_plan_parent_uuid, second_plan_parent_uuid = suggestion_obj.get_plan_params(scenario_parent_uuid, uuid_param)

        logging.debug(f"------- scenario uuid ---------- {uuid_param}")
        logging.debug(f"------- scenario_parent_uuid ---------- {scenario_parent_uuid}")
        logging.debug(f"------- first_plan_uuid ---------- {first_plan_uuid}")
        logging.debug(f"------- second plan uuid ---------- {second_plan_uuid}")
        logging.debug(f" ------ Salesforce Id ------------ {sfdc_id}")
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': 'Processing'}, logging)
        if re_execute_flag == '0':
            plan_param = {'first_plan_uuid': first_plan_uuid, 'first_plan_parent_uuid': first_plan_parent_uuid,'second_plan_uuid': second_plan_uuid, 'second_plan_parent_uuid': second_plan_parent_uuid}
            suggestion_obj = SuggestionUtility(tenant, logging)
            status = suggestion_obj.save_suggestion_instances(scenario_parent_uuid, uuid_param, plan_param)
            logging.debug(f"------- plan param ---------- {plan_param}")
        logging.info(f"--- plan param status ----- {status}")
        update_listing = ReadUiFile(tenant, logging)
        if status.lower() == 'success':
            update_plan_file = UpdatePlanFile(tenant, first_plan_uuid, logging, uuid_param, scenario_parent_uuid, period)
            status = update_plan_file.update_file()
            if status.lower() == 'success':
                component_param = update_listing.get_component_name(first_plan_uuid, None, None, 2)
                Ei = EditInstance(tenant, "Plan")
                Ei.SaveTheDefinitionListNew(first_plan_uuid, '1')
                Ex = ExecutePlan(tenant, "Plan")
                returnVal = Ex.noInstanceError(first_plan_uuid, token_param, component_param)
                if returnVal == '1':
                    paygrid_status, paygrid_info = update_listing.get_paygrid_info(first_plan_uuid, first_plan_parent_uuid, uuid_param, scenario_parent_uuid)
                    logging.debug(f"---- Payout grid info status ---- {paygrid_status}")
                    logging.debug(f"--- Payout grid info ---")
                    logging.debug(paygrid_info)
                    if paygrid_status == 'failed':
                        status = 'Failed'
                        summary = 'Unable to get Payout grid information. Please contact System Admin.'
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                            return 'Failed'
                        except Exception as e:
                            logging.error(e.args)
                            logging.exception(traceback.format_exc())
                            logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                            return 'Failed'
                    PBS = PlanBuilderSQL(tenant, "Plan", "V1", token_param, first_plan_parent_uuid)
                    PBS.createSQL(first_plan_uuid, period, '{}', component_param, paygrid_info)
                    suggestion_obj = SuggestionUtility(tenant, logging)
                    update_plan_file = UpdatePlanFile(tenant, second_plan_uuid, logging, uuid_param, scenario_parent_uuid, period)
                    weight_dict = suggestion_obj.get_rules_json(scenario_parent_uuid, uuid_param)
                    try:
                        C_1_min = int(weight_dict["Components"]["C_1"]["Minimum_Weight"])
                        C_1_max = int(weight_dict["Components"]["C_1"]["Maximum_Weight"])
                        C_2_min = int(weight_dict["Components"]["C_2"]["Minimum_Weight"])
                        C_2_max = int(weight_dict["Components"]["C_2"]["Maximum_Weight"])
                        C_3_min = int(weight_dict["Components"]["C_3"]["Minimum_Weight"])
                        C_3_max = int(weight_dict["Components"]["C_3"]["Maximum_Weight"])
                        C_4_min = int(weight_dict["Components"]["C_4"]["Minimum_Weight"])
                        C_4_max = int(weight_dict["Components"]["C_4"]["Maximum_Weight"])
                    except Exception as e:
                        logging.debug(f" --- unable to get components weights ----")
                        status = 'Failed'
                        summary = 'Unable to get components weight.'
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sfdc_id)
                        except Exception as e:
                            logging.debug(f"---- 1. unable to update sfdc object {sfdc_id}")

                        return 'Failed'
                    status = suggestion_obj.update_rule_parameters(scenario_parent_uuid, uuid_param, second_plan_uuid)
                    if status.lower() == 'success':
                        read_sql_data = ReadSqlData(tenant, second_plan_uuid, '1=1', 0, 10, '', logging)
                        final_column_status, final_column_name = read_sql_data.get_final_column_name(scenario_parent_uuid, uuid_param)
                        create_table_status = read_sql_data.create_suggestion_final_table(uuid_param, final_column_name)
                        counter = 0
                        component_param = update_listing.get_component_name(second_plan_uuid, None, None, 2)
                        if final_column_status.lower() == 'success' and create_table_status.lower() == 'success':
                            for (x, y, z, k) in [(a, b, c, d) for a in range(C_1_min,C_1_max+1,step) for b in range(C_2_min,C_2_max+1,step) for c in range(C_3_min,C_3_max+1,step) for d in range(C_4_min,C_4_max+1,step) if a+b+c+d == 100 and a >= C_1_min and b >= C_2_min and c >= C_3_min and d >= C_4_min]:
                                markers_weight = {'C_1': x, 'C_2': y, 'C_3': z, 'C_4': k}
                                counter = counter + 1
                                logging.info(f" **************** Iteration {counter} ***************** ")
                                status = update_plan_file.update_plan_for_suggestions(first_plan_uuid, first_plan_parent_uuid, period, markers_weight)
                                logging.debug(f"---- after update plan for suggestion method ---- {status}")
                                Ei = EditInstance(tenant, "Plan")
                                logging.debug(f"---- after edit instance method ----")
                                Ei.SaveTheDefinitionListNew(second_plan_uuid, '1')
                                logging.debug(f"----- after SaveTheDefinitionList method -----")
                                Ex = ExecutePlan(tenant, "Plan")
                                logging.debug(f" ---- after ExecutePlan method ----- ")
                                returnVal = Ex.noInstanceError(second_plan_uuid, token_param, component_param)
                                logging.debug(f"------- return val here ------- {returnVal}")
                                if returnVal == '1':
                                    paygrid_status, paygrid_info = update_listing.get_paygrid_info(second_plan_uuid, second_plan_parent_uuid, uuid_param, scenario_parent_uuid)
                                    logging.debug(f"---- Payout grid info status ---- {paygrid_status}")
                                    logging.debug(f"--- Payout grid info ---")
                                    logging.debug(paygrid_info)
                                    if paygrid_status == 'failed':
                                        status = 'Failed'
                                        summary = 'Unable to get Payout grid information. Please contact System Admin.'
                                        try:
                                            SalesForce.connect_to_sfdc(tenant, logging)
                                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                                            return 'Failed'
                                        except Exception as e:
                                            logging.error(e.args)
                                            logging.exception(traceback.format_exc())
                                            logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                                            return 'Failed'
                                    PBS = PlanBuilderSQL(tenant, "Plan", "V1", token_param, second_plan_parent_uuid)
                                    logging.debug(f"---- after PlanBuilderSQL ----- ")
                                    response = PBS.createSQL(second_plan_uuid, period, '{}', component_param, paygrid_info)
                                    logging.debug(f"----- after createSQL method -----")
                                    read_sql_data = ReadSqlData(tenant, second_plan_uuid, '1=1', 0, 10, '', logging)
                                    markers_weight_param = str(x) + ',' + str(y) + ',' + str(z) + ',' + str(k)
                                    read_status, composite_score_df = read_sql_data.get_composite_score(second_plan_uuid, final_column_name, markers_weight_param)
                                    logging.debug(f"--- iteration result --------- {composite_score_df}")
                                    if read_status.lower() == 'success':
                                        push_data_status = read_sql_data.push_score_to_final_table(composite_score_df, uuid_param)
                                        if push_data_status.lower() == 'success':
                                            logging.info(f" ---- Data successfully inserted into final table ---- ")
                                        else:
                                            logging.info(f" ---- unable to insert Data into final table ---- ")
                                            status = 'Failed'
                                            summary = 'Rules have been configured incorrectly.'
                                            try:
                                                SalesForce.connect_to_sfdc(tenant, logging)
                                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                                            except Exception as e:
                                                logging.debug(f"---- 8. unable to update sfdc object {sfdc_id}")

                                            return 'Failed'
                                    else:
                                        logging.debug(f"---- unable to get composite score df for iteration {counter} ---- ")
                                else:
                                    logging.debug(f"--- Error in noInstanceError method for Plan 2 in iteration {counter} ---- ")
                                logging.debug(f"********** Iteration {counter} complete ************ ")

                            logging.debug("********** All Iteration complete ************ ")
                            status = 'Success'
                            summary = 'System execution complete.'
                            logging.debug(f"----- {status} -----")
                            logging.debug(f" ---- {summary} ------")
                            try:
                                SalesForce.connect_to_sfdc(tenant, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                            except Exception as e:
                                logging.debug(f"--- 2. unable to update sfdc object for {sfdc_id}")
                                status = 'Failed'

                            return status
                        else:
                            status = 'Failed'
                            summary = ''
                            if final_column_status.lower() != 'success':
                                logging.debug(f" **** unable to get final column **** ")
                                summary = 'Unable to get final columns for suggestion table.'
                            if create_table_status.lower() != 'success':
                                logging.debug(f" **** unable to create suggestion final table **** ")
                                summary = 'Unable to create final table for system suggestion.'
                            try:
                                SalesForce.connect_to_sfdc(tenant, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                            except Exception as e:
                                logging.debug(f"---- 3. unable to update sfdc object {sfdc_id}")

                            return status
                    else:
                        logging.debug(f" ---- unable to update rules parameter ----- ")
                        status = 'Failed'
                        summary = 'Unable to update rules parameter.'
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                        except Exception as e:
                            logging.debug(f"---- 4. unable to update sfdc object {sfdc_id}")

                        return status
                else:
                    logging.debug(f"--- Error in Plan 1 execution, return value -> {returnVal}")
                    status = 'Failed'
                    summary = 'Error in First Plan Execution.'
                    try:
                        SalesForce.connect_to_sfdc(tenant, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                    except Exception as e:
                        logging.debug(f"---- 5. unable to update sfdc object {sfdc_id}")

                    return status
            else:
                logging.debug(f" ---- unable to update plan 1 file ---- ")
                status = 'Failed'
                summary = 'Unable to update First Plan file.'
                try:
                    SalesForce.connect_to_sfdc(tenant, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
                except Exception as e:
                    logging.debug(f"---- 6. unable to update sfdc object {sfdc_id}")

                return status
        else:
            logging.debug(f" ---- unable to save suggestion file  ----- ")
            status = 'Failed'
            summary = 'Unable to process suggestion parameter file.'
            try:
                SalesForce.connect_to_sfdc(tenant, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
            except Exception as e:
                logging.debug(f"---- 7. unable to update sfdc object {sfdc_id}")

            return status
    except Exception as e:
        logging.debug("--- Error in getSuggestions API ---- ")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        status = 'Failed'
        summary = 'Error in getSuggestions API. Please contact System Admin.'
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Suggestion_Execution_Error_Log__c': summary}, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', sfdc_id)

    return status


@app.route(PREFIX + '/execute', methods=['POST'])
@token_required
def execute_qs(token):
    logging.debug("--- inside execute method ----")
    tenant = request.form.get('tenant', type=str)
    period = request.form.get('period')
    token_param = request.form.get('token')
    uuid_param = request.form.get('sc_uuid')
    plan_param = urllib.parse.unquote_plus(request.form.get('plan_param'), 'UTF-8', 'strict')
    clientID = request.form.get('clientID')
    status = 'Failed'
    sfdc_id = None
    try:
        logging.debug(" *** Execution Start here ")
        plan_param = json.loads(plan_param)
        update_listing = ReadUiFile(tenant, logging)
        logging.debug(f"------- plan param ---------- {plan_param}")
        status = update_listing.update_listing(plan_param)
        sfdc_id = update_listing.getsfdcid(uuid_param)
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': 'Processing'}, logging)
        if status == 'Success':
            plan_uuid = update_listing.getplanuuid(uuid_param)
            parent_uuid = update_listing.getparentuuid(uuid_param)
            parent_plan_uuid = update_listing.getparentplan(plan_uuid, uuid_param)
            logging.debug(f"--- plan id --------- {plan_uuid}")
            logging.debug(f"---- parent uuid ----- {parent_uuid}")
            logging.debug(f"---- parent_plan_uuid ----- {parent_plan_uuid}")
            update_plan_file = UpdatePlanFile(tenant, plan_uuid, logging, uuid_param, parent_uuid, period)
            status = update_plan_file.update_file()
            logging.debug(" *** Metadata Accumulation completed *** ")
            if status == "Success":
                logging.info(f"*************** Calling Get component name Method *************")
                component_param = update_listing.get_component_name(plan_uuid, uuid_param, parent_uuid, 1)
                # IQ-557
                paygrid_status, paygrid_info = update_listing.get_paygrid_info(plan_uuid, parent_plan_uuid, uuid_param, parent_uuid)
                logging.debug(f"---- Payout grid info status ---- {paygrid_status}")
                logging.debug(f"--- Payout grid info ---")
                logging.debug(paygrid_info)
                if paygrid_status == 'failed':
                    sfdc_id = update_listing.getsfdcid(uuid_param)
                    status = 'Failed'
                    summary = 'Unable to get Payout grid information. Please contact System Admin.'
                    try:
                        SalesForce.connect_to_sfdc(tenant, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                        return 'Failed'
                    except Exception as e:
                        logging.error(e.args)
                        logging.exception(traceback.format_exc())
                        logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                        return 'Failed'
                else:
                    logging.info(f"*************** End Get component name Method *************")
                    config_obj = Config_Api(tenant)
                    server_url = config_obj.get_value_from_key('server', 'url')
                    queue_prefix = config_obj.get_value_from_key('server', 'use_queue')
                    service_prefix = config_obj.get_value_from_key('service_prefix', 'SaveUIFile')
                    endpoint = server_url
                    if queue_prefix.lower() == "true":
                        endpoint = endpoint + "/queue/" + service_prefix + "/ExecutePlanParameters/"
                    else:
                        endpoint = endpoint + "/" + service_prefix + "/ExecutePlanParameters/"
                    # endpoint = endpoint + "?token=" + token_param
                    logging.debug(f"------ end point ---------- {endpoint}")
                    data = dict()
                    data["planID"] = plan_uuid
                    data["executionpd"] = period
                    data["executionParams"] = '{}'
                    data["tenant"] = tenant
                    data["type"] = "Plan"
                    data["Append"] = "No"
                    data["version"] = "V1"
                    data["componentLevelExecution"] = component_param
                    data["paygridExecutionJson"] = paygrid_info # json.dumps(dict())
                    data["definitionID"] = parent_plan_uuid
                    data["token"] = token_param
                    data["clientID"] = clientID
                    logging.debug(" *** Plan Execution Start here *** ")
                    req = requests.post(endpoint, data=data)
                    resp = req.text
                    logging.debug(f"--- response ----- {resp}")
                    resp = json.loads(resp)
                    if req.status_code != 200 or resp['Status'].lower() == 'failed':
                        sfdc_id = update_listing.getsfdcid(uuid_param)
                        status = 'Failed'
                        summary = 'Something went wrong while executing Plan. Please contact System Admin.'
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                            return 'Failed'
                        except Exception as e:
                            logging.error(e.args)
                            logging.exception(traceback.format_exc())
                            logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                            return 'Failed'
            else:
                sfdc_id = update_listing.getsfdcid(uuid_param)
                status = 'Failed'
                summary = 'Please fill all required parameters.'
                logging.debug(f"---- got sfdc id ----- {sfdc_id}")
                logging.debug(f"---- scenario uuid ---- {uuid_param}")
                try:
                    SalesForce.connect_to_sfdc(tenant, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                    return 'Failed'
                except Exception as e:
                    logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                    return 'Failed'
        else:
            sfdc_id = update_listing.getsfdcid(uuid_param)
            status = 'Failed'
            summary = 'Unable to update scenario list file.'
            logging.debug(f"---- got sfdc id ----- {sfdc_id}")
            logging.debug(f"---- scenario uuid ---- {uuid_param}")
            try:
                SalesForce.connect_to_sfdc(tenant, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                return 'Failed'
            except Exception as e:
                logging.error(e.args)
                logging.exception(traceback.format_exc())
                logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                return 'Failed'
    except Exception as e:
        logging.debug(f"---- Error in execute API ---- ")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        SalesForce.connect_to_sfdc(tenant, logging)
        logging.debug(f"---- Salesforce Id ---- {sfdc_id}")
        status = 'Failed'
        summary = 'Something went wrong while executing Plan. Please contact System Admin.'
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: execute API', uuid_param)

    return status


@app.route(PREFIX + '/reexecute', methods=['POST'])
@token_required
def re_execute_qs(token):
    logging.debug("--- inside re-execute method ----")
    tenant = request.form.get('tenant', type=str)
    period = request.form.get('period')
    token_param = request.form.get('token')
    uuid_param = request.form.get('sc_uuid')
    clientID = request.form.get('clientID')
    status = 'Failed'
    sfdc_id = None
    try:
        logging.debug(" *** Execution Start Here *** ")
        update_listing = ReadUiFile(tenant, logging)
        plan_uuid = update_listing.getplanuuid(uuid_param)
        parent_uuid = update_listing.getparentuuid(uuid_param)
        parent_plan_uuid = update_listing.getparentplan(plan_uuid, uuid_param)
        sfdc_id = update_listing.getsfdcid(uuid_param)
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': 'Processing'}, logging)
        logging.debug(f"--- plan id --------- {plan_uuid}")
        logging.debug(f"---- parent uuid ----- {parent_uuid}")
        logging.debug(f"---- parent_plan_uuid ----- {parent_plan_uuid}")
        update_plan_file = UpdatePlanFile(tenant, plan_uuid, logging, uuid_param, parent_uuid, period)
        status = update_plan_file.update_file()
        logging.debug(" *** Metadata Accumulation completed *** ")
        if status == "Success":
            logging.info(f"*************** Calling Get component name Method *************")
            component_param = update_listing.get_component_name(plan_uuid, uuid_param, parent_uuid, 1)
            # IQ-557
            paygrid_status, paygrid_info = update_listing.get_paygrid_info(plan_uuid, parent_plan_uuid, uuid_param, parent_uuid)
            logging.debug(f"---- Payout grid info status ---- {paygrid_status}")
            logging.debug(f"--- Payout grid info ---")
            logging.debug(paygrid_info)
            if paygrid_status == 'failed':
                sfdc_id = update_listing.getsfdcid(uuid_param)
                status = 'Failed'
                summary = 'Unable to get Payout grid information. Please contact System Admin.'
                try:
                    SalesForce.connect_to_sfdc(tenant, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                    return 'Failed'
                except Exception as e:
                    logging.error(e.args)
                    logging.exception(traceback.format_exc())
                    logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                    return 'Failed'
            else:
                logging.info(f"*************** End Get component name Method *************")
                config_obj = Config_Api(tenant)
                server_url = config_obj.get_value_from_key('server', 'url')
                queue_prefix = config_obj.get_value_from_key('server', 'use_queue')
                service_prefix = config_obj.get_value_from_key('service_prefix', 'SaveUIFile')
                endpoint = server_url
                if queue_prefix.lower() == "true":
                    endpoint = server_url + "/queue/" + service_prefix + "/ExecutePlanParameters/"
                else:
                    endpoint = server_url + "/" + service_prefix + "/ExecutePlanParameters/"
                # endpoint = endpoint + "?token=" + token_param
                logging.debug(f"------ end point for re-execution ------- {endpoint}")
                data = dict()
                data["planID"] = plan_uuid
                data["executionpd"] = period
                data["executionParams"] = '{}'
                data["tenant"] = tenant
                data["type"] = "Plan"
                data["Append"] = "No"
                data["version"] = "V1"
                data["componentLevelExecution"] = component_param
                data["paygridExecutionJson"] =  paygrid_info #json.dumps(dict())
                data["definitionID"] = parent_plan_uuid
                data["token"] = token_param
                data["clientID"] = clientID
                logging.debug(" *** Plan Execution Start here *** ")
                req = requests.post(endpoint, data=data)
                resp = req.text
                logging.debug(f"--- response ----- {resp}")
                resp = json.loads(resp)
                if req.status_code != 200 or resp['Status'].lower() == 'failed':
                    sfdc_id = update_listing.getsfdcid(uuid_param)
                    status = 'Failed'
                    summary = 'Something went wrong while executing Plan. Please contact System Admin.'
                    try:
                        SalesForce.connect_to_sfdc(tenant, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                        return 'Failed'
                    except Exception as e:
                        logging.error(e.args)
                        logging.exception(traceback.format_exc())
                        logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                        return 'Failed'
        else:
            sfdc_id = update_listing.getsfdcid(uuid_param)
            status = 'Failed'
            summary = 'Please fill all required parameters.'
            logging.debug(f"---- got sfdc id ----- {sfdc_id}")
            logging.debug(f"---- scenario uuid ---- {uuid_param}")
            try:
                SalesForce.connect_to_sfdc(tenant, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                return 'Failed'
            except Exception as e:
                logging.error(e.args)
                logging.exception(traceback.format_exc())
                logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                return 'Failed'
    except Exception as e:
        logging.debug(f"---- Error in reExecute API ---- ")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        SalesForce.connect_to_sfdc(tenant, logging)
        status = 'Failed'
        summary = 'Something went wrong while executing Plan. Please contact System Admin.'
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: reExecute API', uuid_param)

    return status


@app.route(PREFIX + '/executenextplan', methods=['GET', 'POST'])
@token_required
def get_next_plan(token):
    logging.debug('------ inside execute next plan ------')
    tenant = request.values.get('tenant')
    token_param = request.values.get('token')
    plan_uuid = request.values.get('planuuid')
    status = request.values.get('status')
    log_value = request.values.get('logvalue')
    logging.debug(f"----- got tenant ------ {tenant}")
    logging.debug(f"---- token value ----- {token_param}")
    logging.debug(f"----- got plan_uuid ------ {plan_uuid}")
    logging.debug(f"----- got status ------ {status}")
    logging.debug(f"----- got log_value ------ {log_value}")
    scenario_id = None
    sfdc_id = None
    try:
        read_obj = ReadUiFile(tenant, logging)
        scenario_id = read_obj.getscenarioid(plan_uuid)
        sfdc_id = read_obj.getsfdcid(scenario_id)
        scenario_parent_id = read_obj.getparentuuid(scenario_id)
        if status.lower() == "success":
            has_next_sequence = read_obj.hasnextplan(plan_uuid)
            if has_next_sequence:
                plan_data = read_obj.getnextplandata(plan_uuid)
                plan_data = json.loads(plan_data)
                logging.debug(f"--- next plan id in main class ---- {plan_data}")
                plan_uuid_new = plan_data["plan_uuid"]
                uuid_param = plan_data["uuid_param"]
                parent_uuid = plan_data["parent_uuid"]
                period = plan_data["period"]
                current_plan_parent = plan_data["current_plan_parent"]
                rollup_flag = plan_data["rollup_flag"]
                logging.debug(f"----- Rollup Flag ----- ")
                logging.debug(f"{rollup_flag}")
                status = 'success'
                rollup_enable = read_obj.checkRollupEnable(scenario_parent_id, scenario_id)
                logging.debug(f"---- roll up enable ----- {rollup_enable}")
                if rollup_flag == "true" and rollup_enable == "true":
                    # dumping lower level data in scenario final table
                    status = read_obj.prepare_final_table(scenario_id, plan_uuid)
                continue_exec = False
                if (rollup_enable == "true" and rollup_flag == "true") or (rollup_enable == "true" and rollup_flag == "false") or (rollup_enable == "false" and rollup_flag == "false"):
                    continue_exec = True
                logging.debug(f"----- Continue execution -----")
                logging.debug(f"{continue_exec}")
                if continue_exec:
                    if status.lower() == 'success':
                        parent_plan_uuid = read_obj.getparentplan(plan_uuid, uuid_param)
                        logging.debug(f"----- parent plan uuid ----- {parent_plan_uuid}")
                        update_plan_file = UpdatePlanFile(tenant, plan_uuid_new, logging, uuid_param, parent_uuid, period)
                        status = update_plan_file.update_file1(plan_uuid, parent_plan_uuid, period, current_plan_parent)
                        if status.lower() == "success":
                            component_param = read_obj.get_component_name(plan_uuid_new, None, None, 2)
                            new_parent_plan = read_obj.getparentplan(plan_uuid_new, uuid_param)
                            logging.debug(f"----- new_parent_plan ----- {new_parent_plan}")
                            # IQ-557
                            paygrid_status, paygrid_info = read_obj.get_paygrid_info(plan_uuid_new, new_parent_plan, uuid_param, parent_uuid)
                            logging.debug(f"---- Payout grid info status ---- {paygrid_status}")
                            logging.debug(f"--- Payout grid info ---")
                            logging.debug(paygrid_info)
                            if paygrid_status == 'failed':
                                sfdc_id = read_obj.getsfdcid(uuid_param)
                                status = 'Failed'
                                summary = 'Unable to get Payout grid information. Please contact System Admin.'
                                try:
                                    SalesForce.connect_to_sfdc(tenant, logging)
                                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                                    SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                                    return 'Failed'
                                except Exception as e:
                                    logging.error(e.args)
                                    logging.exception(traceback.format_exc())
                                    logging.debug(f"---- unable to update sfdc object {sfdc_id} for {uuid_param}")
                                    return 'Failed'
                            else:
                                config_obj = Config_Api(tenant)
                                server_url = config_obj.get_value_from_key('server', 'url')
                                queue_prefix = config_obj.get_value_from_key('server', 'use_queue')
                                service_prefix = config_obj.get_value_from_key('service_prefix', 'SaveUIFile')
                                endpoint = server_url
                                #if queue_prefix.lower() == "true":
                                #    endpoint = server_url + "/queue/" + service_prefix + "/ExecutePlanParameters/"
                                #else:
                                endpoint = server_url + "/" + service_prefix + "/ExecutePlanParameters/"
                                # endpoint = endpoint + "?token=" + token_param
                                logging.debug(f"------ end point ---------- {endpoint}")
                                data = dict()
                                data["planID"] = plan_uuid_new
                                data["executionpd"] = period
                                data["executionParams"] = '{}'
                                data["tenant"] = tenant
                                data["type"] = "Plan"
                                data["Append"] = "No"
                                data["version"] = "V1"
                                data["componentLevelExecution"] = component_param
                                data["paygridExecutionJson"] = paygrid_info # json.dumps(dict())
                                data["definitionID"] = new_parent_plan
                                data["token"] = token_param
                                req = requests.post(endpoint, data=data)
                                resp = req.text
                                logging.debug(f"--- response ----- {resp}")
                                resp = json.loads(resp)
                                if req.status_code != 200 or resp['Status'].lower() == 'failed':
                                    status = 'Failed'
                                    summary = 'Something went wrong while executing Plan. Please contact System Admin.'
                                    try:
                                        SalesForce.connect_to_sfdc(tenant, logging)
                                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                                        return 'Failed'
                                    except Exception as e:
                                        logging.error(e.args)
                                        logging.exception(traceback.format_exc())
                                        logging.debug(f"---- unable to update sfdc object {scenario_id}")
                                        return 'Failed'
                        else:
                            logging.debug(f"--- unable to update plan file 2 --- ")
                            summary = "Please fill all the required parameters"
                            status = 'Failed'
                            try:
                                SalesForce.connect_to_sfdc(tenant, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                                SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                                return 'Failed'
                            except Exception as e:
                                logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                                return 'Failed'
                    else:
                        status = 'Failed'
                        summary = 'Unable to prepare final table before roll up execution.Please contact admin.'
                        logging.debug(f"--- summary to be updated ---- {summary}")
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                            return 'Failed'
                        except Exception as e:
                            logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                            return 'Failed'
                else:
                    logging.debug(f"---- Discontinue Execution----")
                    status = 'Success'
                    summary = log_value
                    status = read_obj.prepare_final_table(scenario_id, plan_uuid)
                    if status.lower() == "failed":
                        status = 'Failed'
                        summary = 'Unable to prepare final table.Please contact admin.'
                        logging.debug(f"--- summary to be updated ---- {summary}")
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                            return 'Failed'
                        except Exception as e:
                            logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                            return 'Failed'
                    else:
                        status = "Success"
                        summary = "Executed Successfully"
                        try:
                            SalesForce.connect_to_sfdc(tenant, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                            SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                            return 'Success'
                        except Exception as e:
                            logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                            return 'Failed'
            else:
                logging.debug(" *** Plan Execution completed *** ")
                logging.debug(f"---- no next sequence -----")
                logging.debug(f"--- plan executed successfully-----")
                logging.debug(f"---- scenario id ----- {scenario_id}")
                logging.debug(f"---- scenario parent id ----- {scenario_parent_id}")
                status = 'Success'
                summary = log_value
                # dump data into final table
                logging.debug(f" *** dumping data into final table *** ")
                status = read_obj.prepare_final_table(scenario_id, plan_uuid)
                logging.debug(" *** Data Push in Quota Setting's table completed *** ")
                logging.debug(f" ****** final status ******* {status}")
                if status.lower() == "failed":
                    status = 'Failed'
                    summary = 'Unable to prepare final table.Please contact admin.'
                    logging.debug(f"--- summary to be updated ---- {summary}")
                    try:
                        SalesForce.connect_to_sfdc(tenant, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                        return 'Failed'
                    except Exception as e:
                        logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                        return 'Failed'
                else:
                    status = "Success"
                    summary = "Executed Successfully"
                    try:
                        SalesForce.connect_to_sfdc(tenant, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                        logging.debug(" *** Update SFDC for Success, completed *** ")
                        return 'Success'
                    except Exception as e:
                        logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                        return 'Failed'
        else:
            logging.debug(f"--- plan execution failed for {plan_uuid}")
            logging.debug(f"---- plan uuid in else ---- {plan_uuid}")
            scenario_id = read_obj.getscenarioid(plan_uuid)
            sfdc_id = read_obj.getsfdcid(scenario_id)
            logging.debug(f"--- scenario id in else -------- {scenario_id}")
            logging.debug(f"----- sfdc id in else ----- {sfdc_id}")
            status = 'Failed'
            summary = 'Error in plan execution: '+log_value
            logging.debug(f"--- summary to be updated ---- {summary}")
            try:
                SalesForce.connect_to_sfdc(tenant, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
                SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
                return 'Failed'
            except Exception as e:
                logging.debug(f"---- unable to update sfdc object for {scenario_id}")
                logging.error(e.args)
                logging.exception(traceback.format_exception())
                return 'Failed'
    except Exception as e:
        logging.debug(f"---- Error in executeNextPlan API ---- ")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        SalesForce.connect_to_sfdc(tenant, logging)
        record_value = scenario_id if scenario_id is not None else plan_uuid
        status = 'Failed'
        summary = 'Something went wrong while executing Plan. Please contact System Admin.'
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: executeNextPlan API', record_value)
        status = 'Failed'

    return status


@app.route(PREFIX + '/getMetrics', methods=['POST'])
@token_required
def get_metrics(token):
    """
    This method returns adapter columns from selected plan at quota object
    :param token: token for authentication
    :return: json data
    """
    logging.debug("--- inside get metrics------")
    tenant = request.form.get('tenant')
    component_id = request.form.get('compid')
    plan_uuid = request.form.get('planid')
    adapter = request.form.get('adapter')
    data_type = request.form.get('type')
    final_data = {}
    try:
        get_plan_metric = ReadUiFile(tenant, logging)
        response = get_plan_metric.get_metrics(component_id, plan_uuid, adapter, data_type)
        final_data = {'Status': 'Success', 'Data': response}
    except Exception as e:
        logging.debug("--------- error in GetMetrics API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: GetMetrics API', plan_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/getfile', methods=['POST'])
@token_required
def sql_to_csv(token):
    """
    This method return csv data file for quota instance
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"---- sql to csv ----")
    tenant = request.form.get('tenant', type=str)
    uuid_param = request.form.get('uuid', type=str)
    record_id = request.form.get('id')
    view_at = request.form.get('view_at')
    parent_uuid = request.form.get('parent_uuid')
    final_data = {}
    try:
        read_sql_data = ReadDataFromSql(tenant, uuid_param, record_id, logging)
        response = read_sql_data.sql_to_csv(view_at, parent_uuid)
        final_data = {'Status':'Success', 'Data': response}
    except Exception as e:
        logging.debug("--------- error in Getfile API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: Getfile API', uuid_param)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/readGraphJson", methods=['POST'])
@token_required
def get_graph_json(token):
    """
    This method returns graph json for new quota instance
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- inside read graph json file method --------")
    tenant = request.form.get('tenant', type=str)
    final_data = {}
    try:
        read_ui = ReadUiFile(tenant, logging)
        response = read_ui.get_graph_json()
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--------- error in readGraphJson API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: readGraphJson API', '')
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/getGraphData", methods=['POST'])
@token_required
def get_graph_data(token):
    """
    This method returns graph sql data
    :param token: token for authentication
    :return: json data
    """
    logging.debug("---------- inside get graph data method --------")
    tenant = request.form.get('tenant', type=str)
    scenario_uuid = request.form.get('scenario_uuid', type=str)
    view_at = request.form.get('view_at')
    parent_uuid = request.form.get('parent_uuid')
    final_data = {}
    try:
        read_ui = ReadUiFile(tenant, logging)
        response = read_ui.get_graph_data(scenario_uuid, view_at, parent_uuid)
        final_data = {'Status':'Success', 'Data': response}
    except Exception as e:
        logging.debug("--------- error in getGraphData API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({"data": [{}]})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getGraphData API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/readgraphfile", methods=['POST'])
@token_required
def read_graph_file(token):
    """
    This method returns saved graph file for quota instance
    :param token:
    :return:
    """
    logging.debug(f"--- inside read graph file method --- ")
    tenant = request.form.get('tenant', type=str)
    parent_uuid = request.form.get('parent_uuid', type=str)
    sc_uuid = request.form.get('uuid', type=str)
    final_data = {}
    try:
        read_obj = ReadUiFile(tenant, logging)
        response = read_obj.read_graph_file(parent_uuid, sc_uuid)
        final_data = {'Status':'Success', 'Data': response}
    except Exception as e:
        logging.debug("--------- error in ReadGraphFile API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: ReadGraphFile API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/savegraphjson", methods=['POST'])
@token_required
def save_graph_json(token):
    """
    Tnis method saves graph json file for quota instance
    :param token: token for authentication
    :return: status whether it is saved successfully or not
    """
    logging.debug(f" --- inside save graph json method --- ")
    tenant = request.form.get('tenant', type=str)
    uuid_param = request.form.get('uuid', type=str)
    parent_uuid = request.form.get('parent_uuid', type=str)
    data = urllib.parse.unquote_plus(request.form.get('data'), 'UTF-8', 'strict')
    final_data = {}
    try:
        data = json.loads(data)
        save_file_obj = SaveUiFile(uuid_param, tenant, logging, data)
        response = save_file_obj.save_graph_file(parent_uuid)
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--------- error in SaveGraphJson API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: SaveGraphJson API', uuid_param)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/getsuggestionsdata", methods=['POST'])
@token_required
def read_suggestion_table(token):
    """
    This method returns quota suggestion sql data
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"--- inside read suggestion table method --- ")
    tenant = request.form.get('tenant', type=str)
    sc_uuid = request.form.get('sc_uuid', type=str)
    sc_parent_uuid = request.form.get('scenario_parent_uuid', type=str)
    final_data = {}
    try:
        read_sql_data = ReadSqlData(tenant, sc_uuid, '', '', '', '', logging)
        response = read_sql_data.get_suggestion_table_data(sc_parent_uuid)
        final_data = {'Status':'Success', 'Data': response}
    except Exception as e:
        logging.debug("--------- error in GetSuggestionsData API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({"data": None, "rules": None})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: GetSuggestionsData API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/getrules", methods=['POST'])
@token_required
def get_default_rules(token):
    """
    This methods returns rules json file for quota suggestion
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"---- Inside get default rules method ---- ")
    tenant = request.form.get('tenant', type=str)
    final_data = {}
    try:
        read_obj = ReadUiFile(tenant, logging)
        response = read_obj.read_default_rules()
        final_data = {'Status': 'Success','Data': response}
    except Exception as e:
        logging.debug("--------- error in GetRules API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: GetRules API', tenant)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/getsuggestionfile", methods=['POST'])
@token_required
def get_suggestion_file(token):
    """
    This method upload csv file on S3 for quota suggestion
    :param token: token for authentication
    :return: status whether it is upload successfully or not
    """
    logging.debug(f"---- Inside get suggestion file ---- ")
    tenant = request.form.get('tenant', default='Tenant1', type=str)
    uuid_param = request.form.get('sc_uuid', default='', type=str)
    record_id = request.form.get('record_id', type=str)
    final_data = {}
    try:
        read_sql_data = ReadDataFromSql(tenant, uuid_param, record_id, logging)
        response = read_sql_data.get_suggestion_table_data()
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--------- error in GetSuggestionFile API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: GetSuggestionFile API', uuid_param)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/pushtoquotamaster', methods=['POST'])
@token_required
def push_to_quota_master(token):
    logging.debug(f"---- Inside push_to_quota_master method ---- ")
    tenant = request.form.get('tenant')
    source = request.form.get('source')
    input_uuid = request.form.get('input_uuid')
    scenario_name = request.form.get('scenario_name')
    pushed_on = request.form.get('pushed_on')
    output_uuid = request.form.get('output_uuid')
    comment = request.form.get('comment')
    sfdc_id = request.form.get('id')
    primary_keys = request.form.get('primary_keys')
    pushed_by = request.form.get('pushed_by')
    execution_date = request.form.get('cycle_date')
    parent_uuid = request.form.get('parent_uuid')
    view_at = request.form.get('view_at')
    parent_uuid_qs = request.form.get('parent_uuid_qs')

    status = 'Failed'
    try:
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': 'Processing'}, logging)
        main_obj = PushToQuotaMaster(tenant, logging)
        status, summary = main_obj.quota_settings_to_quota_master(input_uuid, scenario_name, source, pushed_on, output_uuid, comment, primary_keys, pushed_by, execution_date, parent_uuid, view_at, parent_uuid_qs)
        logging.debug(f"---- final status ----- {status}")
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)
    except Exception as e:
        logging.debug("--- Error in PushToQuotaMaster API ---")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: PushToQuotaMaster API', input_uuid)
        status = 'Failed'
        summary = 'Unable to push data in Quota Master. Please contact System Admin.'
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', sfdc_id, {'Quota_Setting_Error_Log__c': summary}, logging)

    return status


@app.route(PREFIX + '/getmasterdata', methods=['POST'])
@token_required
def get_quota_master_data(token):
    """
    This method returns Quota Master data
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"--- inside get_quota_master_data methods --- ")
    tenant = request.form.get('tenant')
    scenario_uuid = request.form.get('scenario_uuid')
    clause = urllib.parse.unquote_plus(request.form.get('clause', default='', type=str), 'UTF-8', 'strict')
    clause = clause.replace("^", "%")
    offset = request.form.get('offset', default='', type=str)
    row_num = request.form.get('rownum', default='', type=str)
    order_by = urllib.parse.unquote_plus(request.form.get('orderby', default='', type=str), 'UTF-8', 'strict')
    final_data = {}
    try:
        read_data_obj = ReadSqlData(tenant, '', clause, offset, row_num, order_by, logging)
        response = read_data_obj.get_quota_master_data(scenario_uuid)
        final_data = {'Status':'Success','Data':response}
    except Exception as e:
        logging.debug("--- Error in GetMasterData API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: GetMasterData API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/filterdata', methods=['POST'])
@token_required
def get_filter_columns_data(token):
    """
    This method returns drop down data for quota master
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"--- inside get filter data methods --- ")
    tenant = request.form.get('tenant')
    scenario_uuid = request.form.get('scenario_uuid')
    final_data = {}
    try:
        read_data_obj = ReadSqlData(tenant, '', '', '', '', '', logging)
        response = read_data_obj.get_filter_column_data(scenario_uuid)
        final_data = {'Status':'Success', 'Data': response}
    except Exception as e:
        logging.debug("--- Error in FilterData API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: FilterData API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/callapportioning', methods=['POST'])
@token_required
def calculate_apportioning(token):
    logging.debug(f"---- Inside calculate_apportioning method ---- ")
    tenant = request.form.get('tenant')
    input_uuid = request.form.get('input_uuid')
    periods = request.form.get('periods')
    periods_val = request.form.get('periods_val')
    output_uuid = request.form.get('output_uuid')
    record_id = request.form.get('sfdc_id')
    frequency = request.form.get('Frequency')
    periods_label = request.form.get('periods_label')
    execution_date = None

    update_status = None
    try:
        read_data_obj = PushToQuotaMaster(tenant, logging)
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': 'Processing'}, logging)
        update_status, update_log = read_data_obj.update_quota_master_instance(output_uuid, periods, periods_val, periods_label)
        if update_status.lower() == 'success':
            status, summary = read_data_obj.calculate_apportioning(periods, periods_val, input_uuid, output_uuid, frequency, execution_date, periods_label)
            SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': status}, logging)
            SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': summary}, logging)
        else:
            SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': update_status}, logging)
            SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': update_log}, logging)
    except Exception as e:
        logging.debug("--- Error in CallApportioning API ---")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        update_status = 'Failed'
        update_log = 'Unable to create an apportioning instance. Please contact System Admin.'
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: CallApportioning API', input_uuid)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': update_status}, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': update_log}, logging)

    return update_status


@app.route(PREFIX + '/downloadzipfile', methods=['POST'])
@token_required
def download_zip_file(token):
    """
    This method upload zip file on S3 for QM instance
    :param token: token for authentication
    :return: status whether it is uploaded successfully or not
    """
    logging.debug(f"---- Inside download zip file methods ---- ")
    tenant = request.form.get('tenant')
    scenario_uuid = request.form.get('scenario_uuid')
    clause = urllib.parse.unquote_plus(request.form.get('clause', default='', type=str), 'UTF-8', 'strict')
    clause = clause.replace("^", "%")
    scenario_name = urllib.parse.unquote_plus(request.form.get('scenario_name', default='', type=str), 'UTF-8', 'strict')
    download_flag = request.form.get('flag')
    final_data = {}
    try:
        read_obj = PushToQuotaMaster(tenant, logging)
        status = read_obj.get_sql_table_data(scenario_uuid, scenario_name, clause, download_flag)
        final_data = {'Status':'Success', 'Data':status}
    except Exception as e:
        logging.debug("--- Error in DownloadzipFile API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: DownloadzipFile API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/refreshapportioning', methods=['POST'])
@token_required
def refresh_apportioning(token):
    logging.debug(f" --- Inside refresh apportioning methods ---- ")
    tenant = request.form.get('tenant')
    input_uuid = request.form.get('input_uuid')
    output_uuid = request.form.get('output_uuid')
    record_id = request.form.get('sfdc_id')
    frequency = request.form.get('Frequency')
    execution_date = None

    status = 'Failed'
    try:
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': 'Processing'}, logging)
        read_obj = PushToQuotaMaster(tenant, logging)
        status, summary = read_obj.refresh_apportioning(input_uuid, output_uuid, frequency, execution_date)
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': summary}, logging)
    except Exception as e:
        logging.debug("--- Error in RefreshApportioning API ---")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        status = 'Failed'
        summary = 'Unable to refresh instance. Please contact System Admin.'
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Master: RefreshApportioning API', output_uuid)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': summary}, logging)

    return status


@app.route(PREFIX + '/refinementtomaster', methods=['POST'])
@token_required
def push_to_master_from_refinement(token):
    logging.debug(f"---- Push to Master from Refinement ----- ")
    tenant = request.form.get('tenant')
    source = request.form.get('source')
    refinement_id = request.form.get('refinement_id')
    scenario_name = request.form.get('scenario_name')
    pushed_on = request.form.get('pushed_on')
    scenario_uuid = request.form.get('scenario_uuid')
    primary_keys = request.form.get('primary_keys')
    record_id = request.form.get('record_id')
    pushed_by = request.form.get('pushed_by')
    execution_date = request.form.get('cycle_date')
    parent_uuid = request.form.get('parent_uuid')

    final_status = None
    try:
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': 'Processing'}, logging)
        read_obj = RefinementToQuotaMaster(tenant, source, refinement_id, scenario_name, pushed_on, scenario_uuid, primary_keys, pushed_by, execution_date, parent_uuid, logging, )
        status, log = read_obj.sfdc_to_sql()
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': log}, logging)
        final_status = status + ', '+log
    except Exception as e:
        logging.debug("--- Error in RefinementToMaster API ---")
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_status = 'Failed, Error in RefinementToMaster API'
        status = 'Failed'
        summary = 'Unable to push data in Quota Master. Please contact System Admin.'
        SalesForce.connect_to_sfdc(tenant, logging)
        SalesForce.insert_log(e, traceback.format_exc(), 'Quota Refinement: RefinementToMaster API', record_id)
        SalesForce.update('t_plan_execution__c', record_id, {'Execution_Status__c': status}, logging)
        SalesForce.update('t_plan_execution__c', record_id, {'Quota_Setting_Error_Log__c': summary}, logging)

    return final_status


@app.route(PREFIX + '/getRollupColumns', methods=['POST'])
@token_required
def get_rollup_hierarchy(token):
    """
    This method returns rollup column for quota instance
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f"---- getRollupHierarchy ----- ")
    tenant = request.form.get('tenant')
    scenario_uuid = request.form.get('scenario_uuid')
    parent_uuid = request.form.get('parent_uuid')
    parent_flag = request.form.get('parent_flag')
    final_data = {}
    try:
        roll_obj = RollupUtility(tenant, scenario_uuid, parent_uuid, parent_flag, logging)
        response = roll_obj.get_distinct_geo_types()
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--------- error in getRollupColumns API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] =  'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getRollupColumns API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/getHierarchy', methods=['POST'])
@token_required
def get_hierarchy(token):
    """
    This method returns geo level value when alignment file is changed at quota instance
    :param token: token for authentication
    :return: json data
    """
    logging.debug(f" ---- Inside getHierarchy methods ---- ")
    tenant = request.form.get('tenant')
    parent_uuid = request.form.get('parent_uuid')
    param_json = request.form.get('param_json')
    final_data = {}
    try:
        roll_obj = RollupUtility(tenant, '', parent_uuid, '', logging)
        response = roll_obj.get_hierarchy(param_json)
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--------- error in getHierarchy API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getHierarchy API', parent_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/quotaRefinement', methods=['POST'])
@token_required
def quota_refinement(token):
    """
    This method is used in Quota Refinement for refinement and approval process.
    :param token: token for authentication
    :return: status whether process is success or not
    """
    logging.debug(f" ---- Inside Quota Refinement Job ---- ")
    tenant = request.form.get('tenant')
    quota_id = request.form.get('QuotaMaster_ID')
    parent_geo_level = request.form.get('ParentGeoLevel')
    product = request.form.get('Product')
    quota_action = request.form.get('QuotaAction')
    pos_code = request.form.get('PosCode')
    quota_type = request.form.get('Type')
    distribution_type = request.form.get('distributionType')
    param_list = [tenant, quota_id, parent_geo_level, product, quota_action, pos_code, quota_type, logging, distribution_type]
    logging.debug(f"---- params List ----- {param_list}")
    final_data = {}
    try:
        qr_obj = QuotaRefinementJob(param_list)
        status, summary = qr_obj.run_job()
        logging.debug(f"----- Final Status ----- {status}")
        logging.debug(f"----- Final Summary ----- {summary}")
        final_data = {'Status':'Success', 'Data': status}
    except Exception as e:
        logging.debug("--- Error in quotaRefinement API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Refinement: quotaRefinement API', quota_id)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e  ).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/download_log', methods=['POST'])
@token_required
def download_log_file(token):
    """
    This method upload log file on S3 for quota instance
    :param token: token for authentication
    :return: status whether it is uploaded successfully or not
    """
    logging.debug(f"---- Inside download log file API ---- ")
    tenant = request.form.get('tenant')
    sc_uuid = request.form.get('sc_uuid')
    file_name = request.form.get('file_name')
    param_list = [tenant, sc_uuid, file_name, logging]
    final_data = {}
    try:
        obj = DownloadLogFile(param_list)
        status = obj.get_file()
        final_data = {'Status':'Success', 'Data':status}
    except Exception as e:
        logging.debug("--- Error in download_log API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: download_log API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/get_instance_info', methods=['POST'])
@token_required
def get_instance_info(token):
    """
    This method plan info for quota instance info
    :param token:  token for authentication
    :return: json data
    """
    logging.debug(f"---- Inside download log file API ---- ")
    tenant = request.form.get('tenant')
    sc_uuid = request.form.get('sc_uuid')
    final_data = {}
    try:
        obj = ReadUiFile(tenant, logging)
        response = obj.get_instance_list(sc_uuid)
        final_data = {'Status':'Success', 'Data':response}
    except Exception as e:
        logging.debug("--- Error in get_instance_info API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = json.dumps({"data": None})
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_instance_info API', sc_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + "/download_qr_file", methods=['POST'])
@token_required
def download_qr_file(token):
    logging.debug(f"--- Inside Download QR file method ---")
    tenant = request.form.get('tenant')
    selectedBu = request.form.get('selectedBu')
    quotaId = request.form.get('quotaId')
    quota_name = urllib.parse.unquote_plus(request.form.get("quota_name"), 'UTF-8', 'strict')
    geo_id = request.form.get('geo_id')
    column_params = urllib.parse.unquote_plus(request.form.get('column_params'), 'UTF-8', 'strict')
    column_label_params = urllib.parse.unquote_plus(request.form.get('column_label_params'), 'UTF-8', 'strict')
    refinement_param = urllib.parse.unquote_plus(request.form.get('refinement_param'), 'UTF-8', 'strict')

    param_list = [tenant, selectedBu, quotaId, quota_name, geo_id, column_params, column_label_params, refinement_param, logging]
    # logging.debug(f"---- param list ---- {param_list}")
    final_data = dict()
    try:
        obj = DownloadQRFile(param_list)
        status = obj.prepare_file()
        final_data = {'Status': 'Success', 'Data': status}
    except Exception as e:
        status = "Failed"
        logging.debug("--- Error in download_qr_file API ---")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        final_data['Data'] = 'Failed'
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Refinement: download_qr_file API', quotaId)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/getdefaultjsonNew', methods=['POST'])
@token_required
def getDeafaultJSONNew(token):
    """
        This method return simulation UI file data and Geography values
        :param token: token for authentication
        :return: json data
        """
    logging.debug("---------- Inside getDeafaultJSONNew method --------")
    tenant = request.form.get('tenant')
    scenario_uuid = request.form.get('scenario_uuid')
    parent_uuid = request.form.get('parent_uuid')
    parent_flag = request.form.get('parent_flag')
    final_data = {}
    temp_data = {}
    try:
        read_simulation_ui = ReadUiFile(tenant, logging)
        response1 = read_simulation_ui.read_simulation_ui_file(parent_uuid)
        roll_obj = RollupUtility(tenant, scenario_uuid, parent_uuid, parent_flag, logging)
        response2 = roll_obj.get_distinct_geo_types()
        uuid_value = str(uuid.uuid1())
        temp_data['filedata'] = response1
        temp_data['rollupdata'] = response2
        temp_data['uuid_value'] = uuid_value
        final_data = {'Status': 'Success', 'Data': json.dumps(temp_data)}
    except Exception as e:
        logging.debug("--- Error in getDeafaultJSONNew API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        temp_data['filedata'] = json.dumps({})
        temp_data['rollupdata'] = json.dumps({})
        uuid_value = str(uuid.uuid1())
        temp_data['uuid_value'] = uuid_value
        final_data['Data'] = json.dumps(temp_data)
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getdefaultjsonNew API',
                                           parent_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id

    return jsonify(final_data)


@app.route(PREFIX + '/getdefaultjsonEdit', methods=['POST'])
@token_required
def getDeafaultJSONEdit(token):
    """
        This method return saved simulation UI file data and Geography values
        :param token: token for authentication
        :return: json data
        """
    logging.debug("---------- Inside getDeafaultJSONEdit method --------")
    tenant = request.form.get('tenant')
    parent_uuid = request.form.get('parent_uuid')
    scenario_uuid = request.form.get('scenario_uuid')
    parent_flag = request.form.get('parent_flag')
    final_data = {}
    temp_data = {}

    try:
        read_simulation_value = ReadUiFile(tenant, logging)
        response1 = read_simulation_value.read_simulation_ui_value(scenario_uuid, parent_uuid)
        roll_obj = RollupUtility(tenant, scenario_uuid, parent_uuid, parent_flag, logging)
        response2 = roll_obj.get_distinct_geo_types()
        temp_data['filedata'] = response1
        temp_data['rollupdata'] = response2
        final_data = {'Status': 'Success', 'Data': json.dumps(temp_data)}
    except Exception as e:
        logging.debug("--- Error in getdefaultjsonEdit API ----")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        temp_data['filedata'] = json.dumps({})
        temp_data['rollupdata'] = json.dumps({})
        final_data['Data'] = json.dumps(temp_data)
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getdefaultjsonEdit API',
                                           parent_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode

        final_data['Sfid'] = log_id

    return jsonify(final_data)

@app.route(PREFIX + '/getgraphinfo', methods=['POST'])
@token_required
def get_graph_info(token):
    """
        This method returns graph sql data and graph file
        :param token: token for authentication
        :return: json data
        """
    logging.debug("---------- inside get graph info method --------")
    tenant = request.form.get('tenant', type=str)
    scenario_uuid = request.form.get('scenario_uuid', type=str)
    view_at = request.form.get('view_at')
    parent_uuid = request.form.get('parent_uuid')
    final_data = {}
    temp_data = {}
    try:
        read_obj = ReadUiFile(tenant, logging)
        response1 = read_obj.get_graph_data(scenario_uuid, view_at, parent_uuid)
        response2 = read_obj.read_graph_file(parent_uuid, scenario_uuid)
        temp_data['graphdata'] = response1
        temp_data['filedata'] = response2
        final_data = {'Status': 'Success', 'Data': json.dumps(temp_data)}
    except Exception as e:
        logging.debug("--------- error in get_graph_info API -------")
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data['Status'] = 'Failed'
        temp_data['graphdata'] = json.dumps({"data": [{}]})
        temp_data['filedata'] = json.dumps({})
        final_data['Data'] = json.dumps(temp_data)
        StatusCode = ''
        log_id = ''
        try:
            SalesForce.connect_to_sfdc(tenant, logging)
            log_id = SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getgraphinfo API', scenario_uuid)
        except Exception as e:
            StatusCode = SalesForce.get_error_code(type(e).__name__)
            final_data['StatusCode'] = StatusCode
        final_data['Sfid'] = log_id

    return jsonify(final_data)




@app.route(PREFIX + '/authorize', methods=['POST', 'GET'])
def authorize():
    client_id = request.form.get('client_id', type=str)
    password = request.form.get('client_secret', type=str)
    tenant = request.form.get('tenant', type=str)
    logging.currently_executing_url = str(request.url)
    logging.app_name = None
    logging.info(request.url)
    auth = False
    try:
        config_obj = Config_Api(tenant)
        config_client_id = config_obj.get_value_from_key("salesforce", "client_id")
        config_client_secret = config_obj.get_value_from_key("salesforce", "client_secret")
        if config_client_id == client_id:
            auth = check_password_hash(config_client_secret, password)
    except Exception as e:
        auth = False
    if auth:
        token = jwt.encode({'user': client_id, 'exp': datetime.utcnow() + timedelta(hours=12)},
                           secret, algorithm='HS256')
        response = jsonify(Contents='Access granted. Valid signature.', token=token.decode('UTF-8'))
        response.set_cookie('token', token.decode('UTF-8'))
        return response
    return jsonify(traceback='Access denied. Invalid signature.'), 403


def __resolve_token(request):
    if request.headers.get('Authorization'):
        return request.headers.get('Authorization').split(' ')[1]
    elif request.cookies.get('token'):
        return request.cookies.get('token')
    elif request.form.get('token'):
        return request.form.get('token')
    return request.args.get('token', type=str, default=None)


if __name__ == '__main__':
    app.run(debug=True, threaded=True)
