import time
import pandas as pd
import re
import requests
import logging
from src.helper.ParseJSON import ParseJSON
from datetime import datetime
from simple_salesforce import Salesforce
from src.helper.helpers import decoder
from src.service.ConfigApi import Config_Api

session = requests.Session()


class SalesForce:
    last_time_used = time.time()
    namespace = ''
    sf = None
    tenant = None
    config_obj = None
    is_sandbox = False
    my_logger = None

    @staticmethod
    def connect_to_salesforce(tenant):
        SalesForce.config_obj = Config_Api(tenant)
        SalesForce.tenant = tenant
        if SalesForce.config_obj.get_value_from_key("salesforce", "sandbox").upper() == "TRUE":
            SalesForce.is_sandbox = True

        if SalesForce.is_sandbox:
            SalesForce.sf = Salesforce(username=SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_username"),
                                       password=decoder(
                                           SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_password")),
                                       security_token=SalesForce.config_obj.get_value_from_key("salesforce",
                                                                                               "sfdc_security_token"),
                                       domain="test")
        else:
            SalesForce.sf = Salesforce(username=SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_username"),
                                       password=decoder(
                                           SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_password")),
                                       security_token=SalesForce.config_obj.get_value_from_key("salesforce",
                                                                                               "sfdc_security_token"))
        records = SalesForce.sf.query("select NamespacePrefix from ApexClass where name ='AddQuotaCtrl' limit 1")
        dic = records['records']
        df = pd.DataFrame.from_dict(dic)
        check = df.NamespacePrefix.values[0]
        if check and check != '':
            SalesForce.namespace = str(check) + '__'
        del df

    @staticmethod
    def connect_to_sfdc(tenant, my_logger):
        SalesForce.config_obj = Config_Api(tenant)
        SalesForce.my_logger = my_logger
        SalesForce.tenant = tenant
        if SalesForce.config_obj.get_value_from_key("salesforce", "sandbox").upper() == "TRUE":
            SalesForce.is_sandbox = True

        if SalesForce.is_sandbox:
            SalesForce.sf = Salesforce(username=SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_username"),
                                       password=decoder(
                                           SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_password")),
                                       security_token=SalesForce.config_obj.get_value_from_key("salesforce",
                                                                                               "sfdc_security_token"),
                                       domain="test")
        else:
            SalesForce.sf = Salesforce(username=SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_username"),
                                       password=decoder(
                                           SalesForce.config_obj.get_value_from_key("salesforce", "sfdc_password")),
                                       security_token=SalesForce.config_obj.get_value_from_key("salesforce",
                                                                                               "sfdc_security_token"))
        records = SalesForce.sf.query("select NamespacePrefix from ApexClass where name ='AddQuotaCtrl' limit 1")
        dic = records['records']
        df = pd.DataFrame.from_dict(dic)
        check = df.NamespacePrefix.values[0]
        if check and check != '':
            SalesForce.namespace = str(check) + '__'
        del df
        SalesForce.my_logger.debug(f"---- final updated name space ----- ")
        SalesForce.my_logger.debug(f"{SalesForce.namespace}")

    # replacing , with , space
    @staticmethod
    def _prepare_query(query: str) -> str:
        query = query.replace(SalesForce.namespace, "")
        for custom_object in re.findall('( [0-9aA-zZ_]+__c| [0-9aA-zZ_]+__r)', query):
            if custom_object.find('AxtriaSalesIQTM') == -1:
                query = query.replace(custom_object, ' ' + SalesForce._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__c)', query):
            if custom_object.find('AxtriaSalesIQTM') == -1:
                query = query.replace(custom_object, '.' + SalesForce._prepare_object_name(custom_object[1:]))
        for custom_object in re.findall('(\.[0-9aA-zZ_]+__r)', query):
            if custom_object.find('AxtriaSalesIQTM') == -1:
                query = query.replace(custom_object, '.' + SalesForce._prepare_object_name(custom_object[1:]))
        return query

    @staticmethod
    def _prepare_object_name(object_name):
        if object_name.find('AxtriaSalesIQTM') == -1 and object_name.find(SalesForce.namespace) == -1 and (
                object_name.endswith('__c') or object_name.endswith('__r')):
            return SalesForce.namespace + object_name
        return object_name

    @staticmethod
    def query_as_list(query_string):
        query_string = query_string.replace(",", ", ")
        query_string = SalesForce._prepare_query(query_string)
        logging.debug(f"query string --> {query_string}")
        records = SalesForce.sf.query(query_string)
        return records['records']

    @staticmethod
    def query(query_string):
        query_string = query_string.replace(",", ", ")
        query_string = SalesForce._prepare_query(query_string)
        records = SalesForce.sf.query_all(query_string)
        dic = records['records']
        df = pd.DataFrame.from_dict(dic)
        if not df.empty:
            df.drop('attributes', axis=1, inplace=True)
            logging.debug('--- inside query method -----')
        return df

    @staticmethod
    def update(object_name, object_id, key_val_dict, my_logger):
        object_name = SalesForce._prepare_object_name(object_name)
        for key, value in key_val_dict.items():
            try:
                key = SalesForce._prepare_object_name(key)
                getattr(SalesForce.sf, object_name).update(object_id, {key: value})
            except AttributeError:
                my_logger.debug("no attribute found in object %s" % object_name)

    @staticmethod
    def create(object_name, key_val_dict):
        object_name = SalesForce._prepare_object_name(object_name)
        data_dict = {}
        for key, value in key_val_dict.items():
            try:
                key_new = SalesForce._prepare_object_name(key)
                data_dict[key_new] = key_val_dict[key]
            except AttributeError:
                logging.debug("no attribute found in object %s" % object_name)

        getattr(SalesForce.sf, object_name).create(data_dict)

    @staticmethod
    def bulk_insert(object_name, dict_list):
        object_name = SalesForce._prepare_object_name(object_name)
        dataList = []
        for key_val_dict in dict_list:
            data_dict = {}
            for key, value in key_val_dict.items():
                try:
                    key_new = SalesForce._prepare_object_name(key)
                    data_dict[key_new] = key_val_dict[key]
                except AttributeError:
                    logging.debug("no attribute found in object %s" % object_name)
            dataList.append(data_dict)

        try:
            getattr(SalesForce.sf.bulk, object_name).insert(dataList)
        except AttributeError:
            logging.debug("no attribute found in object %s" % object_name)

    @staticmethod
    def bulk_update(object_name, dict_list):
        object_name = SalesForce._prepare_object_name(object_name)
        dataList = []
        for key_val_dict in dict_list:
            data_dict = {}
            for key, value in key_val_dict.items():
                try:
                    key_new = SalesForce._prepare_object_name(key)
                    data_dict[key_new] = key_val_dict[key]
                except AttributeError:
                    logging.debug("no attribute found in object %s" % object_name)
            dataList.append(data_dict)

        try:
            getattr(SalesForce.sf.bulk, object_name).update(dataList)
        except AttributeError:
            logging.debug("no attribute found in object %s" % object_name)

    @staticmethod
    def bulk_delete(object_name, data_list):
        object_name = SalesForce._prepare_object_name(object_name)
        dataList = []
        for key_val_dict in data_list:
            data_dict = {}
            for key, value in key_val_dict.items():
                try:
                    value = str(value)
                    data_dict['Id'] = value
                except AttributeError:
                    logging.debug("no attribute found in object %s" % object_name)

            dataList.append(data_dict)

        try:
            getattr(SalesForce.sf.bulk, object_name).delete(dataList)
        except AttributeError:
            logging.debug("no attribute found in object %s" % object_name)

    @staticmethod
    def delete(object_name, key_val_dict):
        object_name = SalesForce._prepare_object_name(object_name)
        for key, value in key_val_dict.items():
            try:
                value = str(value)
                getattr(SalesForce.sf, object_name).delete(value)
            except AttributeError:
                logging.debug("no attribute found in object %s" % object_name)

    @staticmethod
    def get_last_time_used():
        last = datetime.fromtimestamp(SalesForce.last_time_used).strftime("%Y-%m-%d %I:%M:%S")
        logging.debug(f"last time used salesforce is {last}")

    @staticmethod
    def insert_log(exception, stacktrace, module, record_id=None):
        object_name = SalesForce._prepare_object_name('AxtriaSalesIQTM__SalesIQ_Logger__c')
        exception_type = type(exception).__name__
        # template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        # message = template.format(exception_type, exception.args)
        data_dict = {'AxtriaSalesIQTM__Exception_Type__c': exception_type,
                     'AxtriaSalesIQTM__Message__c': SalesForce.get_formatted_error_message(exception),
                     'AxtriaSalesIQTM__Module__c': module,
                     'AxtriaSalesIQTM__Stack_Trace__c': stacktrace,
                     'Record_Id__c': record_id}
        data_dict_new = {}
        for key, value in data_dict.items():
            try:
                key_new = SalesForce._prepare_object_name(key)
                data_dict_new[key_new] = data_dict[key]
            except AttributeError:
                logging.debug("no attribute found in object %s" % object_name)

        # getattr(SalesForce.sf, object_name).create(data_dict_new)
        record = SalesForce.sf.AxtriaSalesIQTM__SalesIQ_Logger__c.create(data_dict_new)
        record_id = record.get('id')
        logging.info(f" returning salesforce id {record_id}")
        return record_id

    @staticmethod
    def get_error_code(keyword):
        pj = ParseJSON()
        mapping_file = SalesForce.config_obj.get_mapping_file_location()
        if mapping_file is None:
            SalesForce.my_logger.debug("mapping_file (C:\\Static_Resources\\ErrorMapping.txt) not found..sending "
                                       "generic code 301....")
            return '301'
        SalesForce.my_logger.debug(f'mapping_file--> {mapping_file}')
        error_mapping = pj.LoadJSONFromFile(mapping_file)
        SalesForce.my_logger.debug(f" Keyword ---> {keyword}")
        if keyword in error_mapping:
            selected_error_code = error_mapping[keyword]
        else:
            selected_error_code = '0301'
        SalesForce.my_logger.debug('selectedErrorCode--->' + selected_error_code)
        return selected_error_code

    @staticmethod
    def get_formatted_error_message(e):
        # error_code = SalesForce.get_error_code(str(e.args[0]))
        error_code = ''
        SalesForce.my_logger.debug(f"---- Received error -----> " + type(e).__name__)
        if type(e).__name__.lower() == 'exception':
            error_code = SalesForce.get_error_code(str(e.args[0]))
        else:
            error_code = SalesForce.get_error_code(type(e).__name__)
        SalesForce.my_logger.debug(f" Error code received: {error_code}")
        custom_label = SalesForce.get_custom_label(error_code)
        lst_formatted_string = []

        if len(e.args) > 1:
            for i in range(1, len(e.args)):
                lst_formatted_string.append(str(e.args[i]))

        if len(lst_formatted_string) > 0:
            return custom_label.format(*lst_formatted_string)
        else:
            return custom_label

    @staticmethod
    def get_custom_label(error_code):
        # payload = {"statusCode": error_code}
        label_name = 'Err_SIQIC_' + error_code
        headers = {'Authorization': 'Bearer ' + SalesForce.sf.session_id,
                   'Content-Type': 'application/json'}
        SalesForce.my_logger.debug(f"---- Salesforce base URL ---- {SalesForce.sf.base_url}")
        # custom_label = SalesForce.sf.apexecute('api/CustomLabel', method='POST', data=payload)
        custom_label = session.request("GET", SalesForce.sf.base_url + "tooling/query/?q=select+id,name,"
                                                                       "value+from+customlabel+where+name='{}'".format(label_name), headers=headers)

        response = custom_label.json()
        SalesForce.my_logger.debug(f"custom label ---> "+str(response['records'][0]['Value']))
        return str(response['records'][0]['Value'])

    @staticmethod
    def generate_error():
        num1 = 10
        num2 = 0
        num3 = num1 / num2

        return num3