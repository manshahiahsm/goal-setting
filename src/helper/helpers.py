import logging
from functools import wraps
from threading import Thread
from time import time
from cryptography.fernet import Fernet

"""
    Methods:
    timer(): A decorator to record execution time taken by a function being called.
    pre-running current time added to start var and post running added to end variable.
    Time information is added before returning from wrapper call.

    debugger(): A decorator for Logging information to be added to logs.
    As such this is adding only functions return values to the logger.

    decoder(): A decorator to decode the encrpyted text.

    thread(): A decorator to run a function with variable number of arguments using multi-threading 
    built in library.

"""


def timer(func):
    """
    function decorator to time the execution of a function
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        logging.info("%s - %s : %4.2f (s)" % (args[0].__class__.__name__, func.__name__, (end - start)))
        return result

    return wrapper


def debugger(func):
    """
    function decorator to log execution of a function and the result returned
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        # Todo: Add input logging INFO
        result = func(*args, **kwargs)
        logging.debug(":::: %s.%s() --> %r" % (args[0].__class__.__name__, func.__name__, result))
        return result

    return wrapper


# This is the secret key used for encryption
secret = 'mmExmaYZBo2IFlv9c2_4NpeL8RoRDlyvHqo90WVYXxI='
f = Fernet(secret)


def decoder(token: str) -> str:
    return f.decrypt(token.encode()).decode()


def thread(func):
    """
    function decorator to run a given function in parallel (=asynchronously) to main program code
    daemon = True will allow main program to exit irrespective of threaded call has ended or not.
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        thread_func = Thread(target=func, args=args, kwargs=kwargs, daemon=False)
        thread_func.start()
        return thread_func

    return wrapper

