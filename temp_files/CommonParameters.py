import json,traceback,os
from filelock import Timeout, FileLock
from TemplatesIC.Code.ParseJSON import ParseJSON
from TemplatesIC.Code.Config.ConfigApi import Config_Api

class CommonParameters:

    def __init__(self,tenant,mType,logger):
        self.tenant = tenant
        self.module_type = mType
        self.my_logger = logger
        self.PJ = ParseJSON()
        self.config_obj = Config_Api(tenant)
        self.FilePath = self.PJ.LoadJSONFromFile('./Static_Config/FilePath.txt')
        typeModule = self.PJ.GetValueFromKeyPath(self.FilePath, "FilePath." + mType)
        self.configurationFile = self.config_obj.returnJSON(typeModule, "JSON")
    
    def read_file(self):
        main_folder = self.PJ.GetValueFromKeyPath(self.configurationFile, "Templates_Folder.Others")
        file_loc = main_folder + "//common_parameters.txt"
        self.my_logger.debug(f"--- file loc ---- {file_loc}")
        data = dict()
        if not os.path.isfile(file_loc):
            f = open(file_loc, "w+")
            data = dict()
            data["Parameters"] = []
            json.dumps(data, f, indent=4)
            f.close()
        with open(file_loc) as json_file:
            data = json.load(json_file)

        return json.dumps(data)
    
    def save_json(self, json_data):
        main_folder = self.PJ.GetValueFromKeyPath(self.configurationFile, "Templates_Folder.Others")
        file_loc = main_folder + "//common_parameters.txt"
        lock_file_loc = main_folder + "//common_parameters.txt.lock"
        lock = FileLock(lock_file_loc, timeout=10)
        try:
            data = dict()
            with lock.acquire(timeout=10):
                with open(file_loc) as json_file:
                    data = json.load(json_file)
                new_param_data = json_data["Parameters"]
                old_param_data = data["Parameters"]

                old_dict = dict()
                new_dict = dict()
                for new_item in new_param_data:
                    for new_key, new_value in new_item.items():
                        new_dict[new_key] = new_value

                for old_item in old_param_data:
                    for old_key, old_value in old_item.items():
                        old_dict[old_key] = old_value

                for new_key, new_value in new_dict:
                    old_dict[new_key] = new_value
                
                final_list = list()
                for key, value in old_dict.items():
                    temp_dict = dict()
                    temp_dict[key] = value
                    final_list.append(temp_dict)
                
                data["Parameters"] = final_list
                with open(file_loc, "w+") as file_obj:
                    json.dumps(data, file_obj, indent=4)       
        except Exception as e:
            raise
        finally:
            lock.release()
        