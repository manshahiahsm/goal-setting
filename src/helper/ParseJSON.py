# -*- coding: utf-8 -*-
"""
Created on Sat Apr 27 09:20:27 2019

@author: A0295
"""
import json

class ParseJSON():
    def __init__(self):
        self.__lstPaths = []
    
    def LoadJSONFromFile(self, fileName):
        with open(fileName) as data: 
            data = json.load(data)
        return data

    def __GetKeyPathExpression(self, data, path=None):
    
        if path is None or path == '':
            return json.dumps(data)
    
        keys = path.split(".")
    
        # reset path
        path = None
    
        # if 1st key is a list index
        if all(char.isdigit() for char in keys[0]):
            path = '['+keys[0]+']'
    
            # remove 1st key from key list
            keys.pop(0)
    
        # build proper path
        for key in keys:
            # check if key is a list
            if key.endswith(']'):
                temp = key.split('[')
                key = ''.join(temp[0])
                index = int(temp[1].strip(']'))
                if path is None:
                    path = "['"+key+"']"+"["+str(index)+"]"
                else:
                    path = path+"['"+key+"']"+"["+str(index)+"]"
            else:
                if path is None:
                    path = "['"+key+"']"
    
                else:
                    # check if key is an index
                    if key.isdigit() is True:
                        path = path + "["+key+"]"
                    else:
                        path = path + "['"+key+"']"
        
        keyExpression = 'data'+path
        return keyExpression


    def GetValueFromKeyPath(self, data, path=None, default=None, checknone=False):
        '''
        The GetValueFromKeyPath function takes a dictionary or JSON data and returns value for a specific key.
        If dictator doesnt find a value for a key, or the data is missing the key, the return value is
        either None or whatever fallback value you provide as default="My Default Fallback value".
        Dictator is polite with Exception errors commonly encountered when parsing large Dictionaries/JSONs
        Usage:
        get a value from a Dictionary key
        > dictator(data, "employees[5].firstName")
        with custom value on lookup failure,
        > dictator(data, "employees[5].firstName", default="No employee found")
        pass a parameter
        > dictator(data, "company.name.{}".format(my_company))
        lookup a 3rd element of List Json, on second key, lookup index=5
        > dictator(data, "3.first.second[5]")
        lookup a nested list of lists
        > dictator(data, "0.first[1].2.second.third[0].2"
        check if return value is None, if it is, raise a Value error
        > dictator(data, "some.key.value", checknone=True)
        > error Value Error
        '''
    
        keyPathExpression = self.__GetKeyPathExpression(data, path)
        
        try:
            value = eval(keyPathExpression)
            if value is None:
                value = default
        except (KeyError, ValueError, IndexError, TypeError) as err:
            value = default
        finally:
            if checknone:
                if not value:
                    raise ValueError('missing value for %s' % path)
            return value

    def UpdateKeyValue(self, data, keyPath, value):
        keyExpression = self.__GetKeyPathExpression(data, keyPath)
        # If value is given as None, this will delete the key. Key may hold a complete dictionary or a single value
        if value == None:
            exec('del'+ ' '+keyExpression)
        else:
            keyExpression += '='+'''value'''
            exec(keyExpression)

    def UpdateListOfKeyValues(self, data, keyPathWithValueList):
        for item in keyPathWithValueList:
            self.UpdateKeyValue(data, item.split(':->')[0], None)

    def UpdateKeyValueFromFile(self, data, keyPath, fileNameContainingValue, pathPointingToValue):
        valueSourceData = self.LoadJSONFromFile(fileNameContainingValue)
        value = self.GetValueFromKeyPath(valueSourceData, pathPointingToValue)
        if value:
            self.UpdateKeyValue(data,keyPath,value)
        else:
            print('________Possible Problen With Path Containing Value________')

    def GetAllValuesForaKey(self, data, key):
        if hasattr(data,'items'):
            for k, v in data.items():
                if k == key:
                    yield v
                if isinstance(v, dict):
                    for result in self.GetAllValuesForaKey(v, key):
                        yield result
                elif isinstance(v, list):
                    for d in v:
                        for result in self.GetAllValuesForaKey(d, key):
                            yield result

    def __ResetPath(self):
        self.__lstPaths=[]
        
    def __Traverse(self, data, path='root'):
        listIndex = -1
        if isinstance(data, dict):
            for key, value in data.items():
                if isinstance(value, dict):
                    self.__Traverse(value, path + "." + key)
                elif isinstance(value, list):
                    self.__Traverse(value, path + "." + key)
                else:
                    self.__lstPaths.append(path + "." + key + ":->"+ str(value))
        if isinstance(data, list):
            for item in data:
                listIndex += 1
                if isinstance(item, dict):
                    self.__Traverse(item, "{path}[{listIndex}]".format(path=path, listIndex=listIndex))
                elif isinstance(item, list):
                    self.__Traverse(item, "{path}[{listIndex}]".format(path=path, listIndex=listIndex))
                else:
                    self.__lstPaths.append("{path}[{listIndex}] :-> {item}".format(path=path, listIndex=listIndex, item=item))
        return self.__lstPaths
    
    def __GetAllPaths(self, data):
        self.__root = 'root'
        self.__ResetPath()
        self.__Traverse(data, self.__root)
        return [i.replace(self.__root+'.','') for i in self.__lstPaths]
    
    def GetAllPaths(self, data, filterList=[]):
        subLst = self.__GetAllPaths(data)
        
        for pathFilter in filterList:
            subLst = [i for i in subLst if i.find(pathFilter)!=-1]
        
        return subLst

    def GetAllPathsWithParameters(self, data):
        self.__root = 'root'
        tempLst = self.GetAllPaths(data)
        # From list of paths, check if any path contain '{' or '}' chars indicating parameters. After parameterized path is identified, remove 'root.'
        parameterizedPathLst = [i.replace(self.__root+'.','') for i in tempLst if all(x in i for x in ['{', '}'])]
                
        return parameterizedPathLst
    
    def FilterPaths(self, data, filterStr, isLeaf=True):#if isLeaf=False, filter will be applied to complete path, else only to Leaf
        tempLst = self.GetAllPaths(data)
        
        if isLeaf == True:
            #pick Leaf and do substring match
            tempstr = [i.split(':->')[0] for i in tempLst if i.split(':->')[0].split('.')[-1].find(filterStr)!=-1]
        else:
            #substring match on entire path
            tempstr = [i.split(':->')[0] for i in tempLst if i.find(filterStr)!=-1]
        
        return tempstr
    
    def FindPathsContainingValue(self, data, value):
        self.__ResetPath()
        self.GetAllPaths(data)
        return [i.replace('root.','') for i in self.__lstPaths if i.lower().find(value.lower())!=-1]
    
'''
    def GetKeyAllValues(self, key, var):
        if hasattr(var,'items'):
            for k, v in var.items():
                if k == key:
                    yield v
                if isinstance(v, dict):
                    for result in self.GetKeyAllValues(key, v):
                        yield result
                elif isinstance(v, list):
                    for d in v:
                        for result in self.GetKeyAllValues(key, d):
                            yield result
'''
'''    def GetAllFilteredPathsWithValues(self, data, filterList):
        subLst = self.GetAllPaths(data)
        for pathFilter in filterList:
            subLst = [i for i in subLst if i.find(pathFilter)!=-1]
        return subLst

    def GetAllPathsWithParameters(self, data):
        self.__root = 'root'
        self.GetAllPaths(data)
        # From list of paths, check if any path contain '{' or '}' chars indicating parameters. After parameterized path is identified, remove 'root.'
        parameterizedPathLst = [i.replace(self.__root+'.','') for i in self.__lstPaths if all(x in i for x in ['{', '}'])]
        return parameterizedPathLst
'''