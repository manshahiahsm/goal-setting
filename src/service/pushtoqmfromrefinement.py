import traceback
import uuid
import math
import src.helper.sql_connection
from src.helper.salesforce import SalesForce
from src.service.pushtoquotamaster import PushToQuotaMaster


class RefinementToQuotaMaster:

    def __init__(self, tenant, source, refinement_id, scenario_name, pushed_on, scenario_uuid, primary_keys, pushed_by, execution_date, parent_uuid, my_logger):
        self.tenant = tenant
        self.my_logger = my_logger
        self.scenario_uuid = scenario_uuid
        self.qr_id = refinement_id
        self.scenario_name = scenario_name
        self.source = "Quota Refinement"
        self.pushed_on = pushed_on
        self.primary_keys = primary_keys
        self.pushed_by = pushed_by
        self.execution_date = execution_date
        self.parent_uuid = parent_uuid
        self.admin_db = src.helper.sql_connection.connect_to_sql_db(self.tenant, 'processing')

    def sfdc_to_sql(self):
        status = None
        summary = None
        admin_cursor = self.admin_db.cursor()
        try:
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            temp_uuid = uuid.uuid1()
            table_name = "IC_Output.[t_"+str(temp_uuid)+"]"
            new_obj = PushToQuotaMaster(self.tenant, self.my_logger)
            # IQ-556
            input_columns, input_column_type, unique_columns, apportioning_columns, sfdc_columns, data_types, qr_columns, master_columns, master_data_types = new_obj.update_quota_master_listing(scenario_uuid=self.scenario_uuid, input_uuid=None, record_id=self.qr_id)
            definition_status = new_obj.update_quota_master_definition(self.parent_uuid, master_columns, master_data_types, self.qr_id)
            instance_status = new_obj.update_quota_master_definition(self.scenario_uuid, master_columns, master_data_types, self.qr_id)
            if definition_status.lower() == 'success' and instance_status.lower() == 'success':
                summary = ''
                sfdc_columns_list = sfdc_columns.split(",")
                data_types_list = data_types.split(",")
                input_columns_list = qr_columns.split(",")
                column_type_map = dict()
                sfdc_column_type_map = dict()
                for x, y in zip(input_columns_list, data_types_list):
                    column_type_map[x] = y
                for x, y in zip(sfdc_columns_list, data_types_list):
                    sfdc_column_type_map[x] = y

                drop_query = "IF OBJECT_ID('"+table_name+"','U') IS NOT NULL DROP TABLE "+table_name+""
                self.my_logger.debug(f"--- drop query ---- ")
                self.my_logger.debug(f"{drop_query}")
                admin_cursor.execute(drop_query)
                self.admin_db.commit()
                create_query = "CREATE TABLE "+table_name+" ("
                for column_name, data_type in column_type_map.items():
                    create_query = create_query + column_name + " "
                    if data_type.lower() == 'text':
                        create_query = create_query + " VARCHAR(4000),"
                    elif data_type.lower() == 'number':
                        create_query = create_query + " NUMERIC(25,13),"
                    elif data_type.lower() == 'date':
                        create_query = create_query + " DATE,"
                    else:
                        create_query = create_query + " VARCHAR(4000),"

                create_query = create_query[:-1]
                create_query = create_query + ")"
                self.my_logger.debug(f"---- Create table Query ----")
                self.my_logger.debug(f"{create_query}")
                admin_cursor.execute(create_query)
                self.admin_db.commit()
                soql_query = "Select "+sfdc_columns+"  From Quota_GEO_Mapping__c Where Quota__c='"+self.qr_id+"'"
                refinement_list = SalesForce.query(soql_query)
                refinement_list.columns = sfdc_columns_list
                self.my_logger.debug(f"--- refinement list columns ----")
                self.my_logger.debug(f"{refinement_list.columns}")
                data_list = []
                for _, row in refinement_list.iterrows():
                    data_tuple = ()
                    data_list.clear()
                    for column in sfdc_columns_list:
                        data_type = sfdc_column_type_map[column]
                        temp_data_list = list(data_tuple)
                        data_value = row[column]

                        if data_value is not None and type(data_value) == str:
                            data_value = data_value.replace("'", "\"")

                        if data_value is None:
                            data_value = 'NULL'

                        if data_type.lower() == 'text':
                            temp_data_list.append(data_value)
                        elif data_type.lower() == 'Date':
                            temp_data_list.append(data_value)
                        else:
                            if data_value != 'NULL' and math.isnan(float(data_value)):
                                data_value = 0.0

                            temp_data_list.append(data_value)
                        data_tuple = tuple(temp_data_list)

                    data_list.append(data_tuple)
                    self.my_logger.debug(f"----- Data List Size ------ ")
                    self.my_logger.debug(len(data_list))

                    values = ', '.join(map(str, data_list))
                    insert_query = "INSERT INTO "+table_name+" VALUES {}".format(values)
                    insert_query = insert_query.replace("'NULL'", "NULL")
                    # self.my_logger.debug(f"********************** Insert Query ************************** ")
                    # self.my_logger.debug(f"{insert_query}")
                    self.my_logger.debug(f" **** Inserting data in SQL **** ")
                    admin_cursor.execute(insert_query)
                    self.admin_db.commit()
                # IQ-556
                updated_data_type = ''
                master_data_type_list = master_data_types.split(",")
                for data_type in master_data_type_list:
                    if data_type.lower() == "text":
                        updated_data_type = updated_data_type + 'VARCHAR(4000)|'
                    elif data_type.lower() == "number":
                        updated_data_type = updated_data_type + 'NUMERIC(25,13)|'
                    elif data_type.lower() == "date":
                        updated_data_type = updated_data_type + 'date|'
                    else:
                        updated_data_type = updated_data_type + 'VARCHAR(4000)|'

                updated_data_type = updated_data_type[0:-1]
                master_columns = "Comments," + master_columns
                updated_data_type = "VARCHAR(4000)|" + updated_data_type
                self.my_logger.debug(f"--- master_columns --- ")
                self.my_logger.debug(f"{master_columns}")
                self.my_logger.debug(f"--- update data types --- ")
                self.my_logger.debug(f"{updated_data_type}")
                query = "exec sp_Quota_Refinement_To_Quota_Master '" + self.source + "','" + str(temp_uuid) + "','" + self.scenario_name + "','" + self.pushed_on + "','" + self.scenario_uuid + "','" + qr_columns + "','" + self.primary_keys + "','" + unique_columns + "','"+self.pushed_by+"','"+self.execution_date+"','"+apportioning_columns+"','Data_Value','"+master_columns+"','"+updated_data_type+"'"
                self.my_logger.debug(f"---- procedure query ---- ")
                self.my_logger.debug(f"{query}")
                admin_cursor.execute(query)
                self.admin_db.commit()
                result = admin_cursor.fetchone()
                message = result[0]
                self.my_logger.debug(f"----- message -----")
                self.my_logger.debug(f"{message}")
                result_list = message.split(";")
                if len(result_list) >= 2:
                    status = result_list[0]
                    summary = result_list[1]
                elif len(result_list) == 1:
                    status = result_list[0]
                    summary = ''
                if status.lower() != 'failed':
                    admin_cursor.execute(drop_query)
                    self.admin_db.commit()
                if status.lower() == 'success':
                    summary = 'The Process has been completed successfully'
                else:
                    summary = 'Unable to push data in Quota Master. ' + summary
            else:
                status = 'Failed'
                summary = 'Unable to push data in Quota Master. Please contact System Admin.'
        except Exception as e:
            self.my_logger.debug(f"---- Error in sfdc_to_sql method ----- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Refinement: RefinementToMaster API', self.qr_id)
            status = 'Failed'
            summary = 'Unable to push data in Quota Master. Please contact System Admin.'
        finally:
            admin_cursor.close()
            self.admin_db.close()
        return status, summary
