from json import JSONDecodeError

import src.helper.sql_connection
import traceback
import json
import pandas as pd
from sqlalchemy import text
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce
from src.helper.ParseJSON import ParseJSON


class RollupUtility:

    def __init__(self, tenant, scenario_uuid, parent_def_uuid, parent_flag, my_logger):
        self.tenant = tenant
        self.my_logger = my_logger
        self.parent_flag = parent_flag
        self.parent_uuid = parent_def_uuid
        self.scenario_uuid = scenario_uuid
        self.entityType = None
        self.parentId = None
        self.componentId = None
        self.isInstance = None
        self.engine = src.helper.sql_connection.createEngineToAdminDB(self.tenant, 'processing')
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")

    def get_distinct_geo_types(self):
        """
        This method returns geo level values for quota instance
        :return: json data
        """
        self.my_logger.debug(f"---- in get_distinct_geo_types------")
        final_data = dict()
        try:
            simulation_data = self.get_simulation_data()
            alignment_data = self.get_alignment_data(simulation_data)
            object_id = alignment_data["object_id"]
            instance_id = alignment_data["instance_id"]
            table_columns = alignment_data["table_columns"]
            table_mappings = alignment_data["table_mappings"]
            final_data = self.run_query(object_id, instance_id, table_columns, table_mappings)
        except Exception as e:
            raise

        return json.dumps(final_data)

    def get_hierarchy(self, alignment_data):
        self.my_logger.debug(f"---- Inside get_hierarchy Methods ---- ")
        final_data = dict()
        try:
            self.my_logger.debug(f"---- alignment data ---- ")
            self.my_logger.debug(f"{alignment_data}")
            alignment_data = json.loads(alignment_data)
            self.my_logger.info(f"---- alignment data type ---- ")
            self.my_logger.debug(type(alignment_data))
            object_id = alignment_data["Object_Id"]
            instance_id = alignment_data["Value"]
            table_columns = alignment_data["Info_Table_Columns"]
            table_mappings = alignment_data["Info_Mapping"]
            final_data = self.run_query(object_id, instance_id, table_columns, table_mappings)
        except Exception as e:
            raise

        return json.dumps(final_data)

    def run_query(self, object_id, instance_id, table_columns, table_mappings):
        """
        This method fetch geo level data from sql table for quota instance
        :param object_id: data set object id
        :param instance_id: data set instance id
        :param table_columns: table columns name
        :param table_mappings: mapping corresponding to quota column
        :return: json data
        """
        final_data = dict()
        self.my_logger.debug(f"---- Inside run query method ---- ")
        self.my_logger.debug(f"----- table columns ----- "+table_columns)
        self.my_logger.debug(f"----- table mappings ----- " + table_mappings)
        try:
            table_name = None
            adapter_name = None
            input_type = None
            object_list = object_id.split("|")
            data_adapter_id = None
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            if len(object_list) == 1:
                self.my_logger.info("--- Case of DM ----")
                input_type = "DM"
                query = "Select Adapter__c  From Data_Set__c Where Id='" + object_list[0] + "'"
                dm_data = SalesForce.query(query)
                dm_data.columns = ["adapter"]

                for _, row in dm_data.iterrows():
                    adapter_name = row["adapter"]
                self.my_logger.info(f"--- adapter name --- {adapter_name}")

                query = "Select Data_Adapter_ID__c from Template__c WHERE Data_Adapter__c='" + adapter_name + "' AND Tenant__r.Name='" + self.tenant + "'"
                self.my_logger.debug(f"--- Query for Adapter Id --- {query}")
                template_data = SalesForce.query(query)
                template_data.columns = ["data_adapter_id"]

                for _, row in template_data.iterrows():
                    data_adapter_id = row["data_adapter_id"]

                self.my_logger.debug(f"---- Data Adapter Id ---- {data_adapter_id}")

            else:
                self.my_logger.info("---- Case of template object----- ")
                parent_id = object_list[0]
                component_selected = object_list[1]
                input_type = object_list[2]
                if component_selected.lower() == "output":
                    table_name = "IC_Output.[t_" + instance_id + "]"
                else:
                    self.entityType = input_type
                    self.isInstance = True
                    self.parentId = instance_id
                    self.componentId = component_selected
                    table_name = self.get_component_table()
                    table_name_comp = table_name.split(".")
                    table_name = table_name_comp[0] + ".[" + table_name_comp[1] + "]"
            table_column_list = table_columns.split(",")
            new_list = [item.lower() for item in table_column_list]
            table_mapping_list = table_mappings.split(",")
            index = new_list.index("geo_type")
            index_bu = new_list.index("salesforce")
            input_column = None
            bu_column = None
            if index is not None:
                input_column = table_mapping_list[index]
                bu_column = table_mapping_list[index_bu]
            index = new_list.index("geo_code")
            hierarchy_column = table_mapping_list[index]
            index = new_list.index("parent_geo_code")
            hierarchy_column = hierarchy_column + "," + table_mapping_list[index]
            self.my_logger.debug(f"--- Hierarchy Column ---- {hierarchy_column}")
            self.my_logger.debug(f"--- input type ---- {input_type}")
            self.my_logger.debug(f"--- object Id --- {object_id}")
            self.my_logger.debug(f"---- instanceId --- {instance_id}")
            self.my_logger.debug(f"--- data adapter id --- {data_adapter_id}")
            self.my_logger.debug(f"--- input column --- {input_column}")
            self.my_logger.debug(f"--- hierarchy column --- {hierarchy_column}")
            self.my_logger.debug(f"---- Salesforce Column ---- {bu_column}")
            bu_value = self.get_salesforce()
            where_clause = " " + bu_column + "=''" + bu_value + "''"
            self.my_logger.debug(f"---- Salesforce where clause --- {where_clause}")
            if input_type != 'DM':
                data_adapter_id = '0'
            self.my_logger.debug(f"--- data adapter id --- {data_adapter_id}")
            query = "EXEC usp_Quota_Find_Hierarchy '" + input_type + "','" + str(object_id) + "','" + str(instance_id) + "','" + str(data_adapter_id) + "'"
            query = query + ",'" + str(input_column) + "','" + str(hierarchy_column) + "','" + str(table_name) + "','" + where_clause + "'"
            self.my_logger.debug(f"--- procedure call ---- {query}")

            result = pd.read_sql_query(text(query), self.engine)
            data = json.loads(result.to_json(orient='records'))
            final_data["data"] = data
        except Exception as e:
            raise
        finally:
            self.engine.dispose()
        return final_data

    def get_simulation_data(self):
        parent_dir = self.main_dir
        file_loc = None
        if self.parent_flag == '1':
            folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid
            file_name = self.parent_uuid + '_ui_value_info.txt'
            self.my_logger.info(f"--- parent uuid --- {self.parent_uuid}")
            self.my_logger.info(f"---- file name ---- {file_name}")
            file_loc = folder_loc + '\\' + file_name
        else:
            folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.scenario_uuid
            file_name = self.scenario_uuid + '_ui_value.txt'
            file_loc = folder_loc + '\\' + file_name
            self.my_logger.info(f"--- sc uuid --- {self.scenario_uuid}")
            self.my_logger.debug(f"--- file name --- {file_name}")

        self.my_logger.info(f"--- file location --- {file_loc}")
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise
        return data

    def get_alignment_data(self, data):
        data_dict = dict()
        try:
            file_data = data["Parameters"]
            for item in file_data:
                for item_key, item_value in item.items():
                    if item_key == 'Action':
                        if item_value.lower() == 'alignment':
                            data_dict["object_id"] = item["Object_Id"]
                            data_dict["instance_id"] = item["Value"]
                            data_dict["table_columns"] = item["Info_Table_Columns"]
                            data_dict["table_mappings"] = item["Info_Mapping"]
        except KeyError as e:
            raise Exception('KeyError', e.args)
        except Exception as e:
            raise
        return data_dict

    def get_component_table(self):
        table_name = ''
        try:
            self.my_logger.info(f"---- Inside get_component_table ------")
            table_name = None
            pjObj = ParseJSON()
            saveuifile_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            self.my_logger.info(f"---- SaveUI file Path ---- {saveuifile_path}")
            path = saveuifile_path + "\\Static_Config\\FilePath.txt"
            self.my_logger.debug(f"----- Inside New Code ----- ")
            resource_file_data = ''
            try:
                resource_file_data = pjObj.LoadJSONFromFile(path)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', path.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', path.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            typeModule = pjObj.GetValueFromKeyPath(resource_file_data, "FilePath." + self.entityType)
            template_config_data = self.config_obj.returnJSON(typeModule, "JSON")
            listing_key = pjObj.GetAllPaths(template_config_data, ["Instances_Folder", "Listing"])[0].split(":->")[0]
            self.my_logger.info(f"****** listing_key ****** {listing_key}")
            listing_key_data = pjObj.GetValueFromKeyPath(template_config_data, listing_key)
            instance_file_path = listing_key_data + "\\"+self.parentId+".txt"
            inst_listing = ''
            try:
                inst_listing = pjObj.LoadJSONFromFile(instance_file_path)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', instance_file_path.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', instance_file_path.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            self.my_logger.info(f"****** instance_file_path ****** {instance_file_path}")
            component_key = pjObj.GetAllPaths(inst_listing, ["TableList", "Id"])
            self.my_logger.debug(f"---- component Id ----- ")
            self.my_logger.debug(f"{self.componentId}")
            self.my_logger.debug(f"----- Component key ------ ")
            self.my_logger.debug(f"{component_key}")
            for row in component_key:
                items = row.split(":->")
                self.my_logger.debug(f"----- items[1] ------ {items[1]}=={self.componentId}")
                if items[1] == self.componentId:
                    self.my_logger.debug(f"---- items value match ---- ")
                    component_key = items[0]
                    break
            component_table_key = component_key.replace("Id", "Table")
            table_name = pjObj.GetValueFromKeyPath(inst_listing, component_table_key)
        except KeyError as e:
            raise Exception('KeyError', e.args)
        except Exception as e:
            raise
        return table_name

    def get_salesforce(self):
        bu_value = None
        file_loc = ''
        try:
            folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid
            file_name = self.parent_uuid + '_ui_value_info.txt'
            self.my_logger.info(f"--- parent uuid --- {self.parent_uuid}")
            self.my_logger.info(f"---- file name ---- {file_name}")
            file_loc = folder_loc + '\\' + file_name
            bu_value = None
            with open(file_loc) as json_file:
                data = json.load(json_file)

            file_data = data["Parameters"]
            for item in file_data:
                for key, value in item.items():
                    if key == 'SFDC' and value == 'BU__c':
                       bu_value = item['Value']
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return bu_value
