from json import JSONDecodeError

import pandas as pd
import src.helper.sql_connection
from sqlalchemy import text
import traceback
from src.helper.config import rp_config
from src.helper.s3 import S3
import uuid
import json
from src.helper.salesforce import SalesForce
from src.service.ConfigApi import Config_Api


class ReadDataFromSql:

    def __init__(self, tenant, sc_uuid, record_id, my_logger):
        self.tenant = tenant
        self.uuid = sc_uuid
        self.recordid = record_id
        self.my_logger = my_logger
        self.file_path = ''
        self.s3path = ''
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.engine = src.helper.sql_connection.createEngineToAdminDB(self.tenant, 'processing')
        self.my_logger.debug("--- connection established ---- ")

    def sql_to_csv(self, view_at, parent_uuid):
        """
        This method fetch data from SQL and call function to upload it on s3
        :param view_at: geo level
        :param parent_uuid: parent quota uuid
        :return: json data
        """
        self.my_logger.debug(f"--- inside sql to csv ---")
        file_obj = ''
        response = 'Failed'
        try:
            temp_uuid = str(uuid.uuid1())
            parent_dir = self.main_dir
            self.file_path = parent_dir+"/goal-setting/tmp/GoalSettings"+temp_uuid+".csv"
            file_obj = open(self.file_path, 'w', newline='')
            self.my_logger.debug(f"---- file path  ----- {self.file_path}")
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = ''
            try:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            uuid_data = file_data[self.uuid]
            output_header = ""
            hierarchy_column = ""
            rollup_column = ""
            for key, value in uuid_data.items():
                if key.lower() == "output":
                    output_header = value
                if key.lower() == "hierarchy":
                    hierarchy_column = value
                if key.lower() == "rollup_header":
                    rollup_column = value

            self.my_logger.debug(f"---- rollup header ------ {rollup_column}")
            self.my_logger.debug(f"---- output header ----- {output_header}")
            self.my_logger.debug(f"---- hierarchy_column ----- {hierarchy_column}")
            view_at = view_at.strip()
            self.my_logger.debug(f"--- view at updated ---- {view_at}")
            scenario_uuid = self.uuid
            rollup_flag = False
            if view_at:
                rollup_value = self.getSetGoalValue(parent_uuid)
                if rollup_value is not None and view_at.lower() != rollup_value.lower():
                    scenario_uuid = self.uuid + "_rollup"
                    rollup_flag = True

            query = None
            if rollup_flag:
                table_name = "IC_Output.[t_"+scenario_uuid+"]"
                query = "Select "+rollup_column+" from "+table_name+" WHERE Geo_Type='"+view_at+"' "
            else:
                query = 'exec sp_quota_download \'' + scenario_uuid + '\',\'' + output_header + '\',\'' + hierarchy_column + '\''
            self.my_logger.debug(f"--- table query ---- {query}")
            # query_status = False
            df = pd.DataFrame()
            df = pd.read_sql_query(text(query), self.engine)
            """
            query_status = True
            except Exception as e:
                self.my_logger.debug(" unable to read sql query ")
                self.my_logger.error(e.args)
                self.my_logger.exception(traceback.format_exc())
                SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: GetFile API', self.uuid)
            if not query_status:
                column_list = []
                #if rollup_flag:
                #   column_list = rollup_column.split(",")
                #else:
                #    column_list = output_header.split(",")
                df = pd.DataFrame(columns=column_list)
                self.my_logger.debug(f"---- creating empty data frame ---- ")
            """
            try:
                df.drop(['ID'], axis=1, inplace=True)
            except Exception as e:
                df.drop(['Id'], axis=1, inplace=True)

            df.to_csv(file_obj, index=False)
            file_obj.close()
            response = self.upload_on_s3(0)
        except Exception as e:
            raise
        finally:
            self.engine.dispose()
        return response

    def upload_on_s3(self, suggestion_flag):
        status = 'Failed'
        try:
            self.my_logger.info(f"--- inside upload to s3 path --- ")
            try:
                S3.connect_to_s3(self.tenant)
            except Exception as e:
                raise
            temp = uuid.uuid1()
            filename = 'QuotaSetting'+str(temp)+'.csv'
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            t_plan_execution = SalesForce.query(f"""Select name from t_plan_execution__c where Id='{self.recordid}'""")
            if not t_plan_execution.empty:
                self.my_logger.debug(f"--- t_plan_execution is not empty ----")
                t_plan_execution.columns = ['name']
                if suggestion_flag == 0:
                    filename = str(t_plan_execution.loc[0, 'name'])+'.csv'
                else:
                    filename = str(t_plan_execution.loc[0, 'name'])+'_Suggestion.csv'
            else:
                self.my_logger.debug(f"--- t_plan_execution is empty ----")

            self.my_logger.debug(f"----- filename of file to be uploaded ---- {filename}")
            s3_bucket = self.config_obj.get_value_from_key("s3", "s3_bucket")
            self.s3path = rp_config['s3']['goalSettingPath'].format(bucket=s3_bucket, tenant=self.tenant, uuid=self.uuid, filename=filename)
            self.my_logger.debug(f"-- se path -------- ")
            self.my_logger.debug(self.s3path)
            S3.upload_file_from_local_path(self.file_path, self.s3path)
            status = 'Success'
        except Exception as e:
            raise

        return status
    
    def get_suggestion_table_data(self):
        """
        This method fetch data from SQL for quota suggestion and upload a csv file on S3
        :return: status whether it is uploaded successfully or not
        """
        self.my_logger.debug(f"--- inside get_suggestion_table_data ---- ")
        response = 'Failed'
        try:
            temp_uuid = str(uuid.uuid1())
            parent_dir = self.main_dir
            self.file_path = parent_dir+"/goal-setting/tmp/GoalSettings"+temp_uuid+".csv"
            self.my_logger.debug(f"---- file path  ----- {self.file_path}")
            file_obj = open(self.file_path, 'w', newline='')
            table_name = 'IC_Output.[t_'+self.uuid+'_suggestion]'

            query = "Select * from "+table_name+" "
            self.my_logger.info(f"---- table query ---")
            self.my_logger.debug(query)
            query_status = False
            df = pd.read_sql_query(text(query), self.engine)
            df.to_csv(file_obj, index=False)
            file_obj.close()
            response = self.upload_on_s3(1)
        except Exception as e:
            raise
        finally:
            self.engine.dispose()
        return response

    def getSetGoalValue(self, parent_uuid):
        folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + self.uuid
        file_name = self.uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise
        file_data = data["Parameters"]

        rollup_value = None
        for item in file_data:
            for key, value in item.items():
                if key.lower() == "sfdc" and value.lower() == "calculate_goal_level__c":
                    rollup_value = item["Value"]

        return rollup_value
