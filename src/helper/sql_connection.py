import pyodbc
from src.helper.helpers import decoder
from sqlalchemy import create_engine, text
from src.service.ConfigApi import Config_Api


def connect_to_sql_db(tenant, prefix):
	config_obj = Config_Api(tenant)
	parent_section = 'sql_' + prefix
	server_name = config_obj.get_value_from_key(parent_section, 'sql_server')
	db_name = config_obj.get_value_from_key(parent_section, 'sql_database')
	user = config_obj.get_value_from_key(parent_section, 'sql_username')
	password = config_obj.get_value_from_key(parent_section, 'sql_password')
	decoded_pass = decoder(password)
	connection = create_connection_with_cred(server_name, db_name, user, decoded_pass)
	return connection


def create_connection_with_cred(server_name, db_name, user, password):
	print('-------db credentials--------')
	print(server_name + ' ' + db_name + ' ' + user + ' ' + password)
	connection_string = "Driver=SQL Server Native Client 11.0;" + "Server=" + server_name + ";" + "Database=" + db_name + ";" + "uid=" + user + ";" + "pwd=" + password + ";"
	connection = pyodbc.connect(connection_string, autocommit=True)
	return connection


def createEngineToAdminDB(tenant, prefix):
	config_obj = Config_Api(tenant)
	parent_section = 'sql_' + prefix
	server_name = config_obj.get_value_from_key(parent_section, 'sql_server')
	db_name = config_obj.get_value_from_key(parent_section, 'sql_database')
	user = config_obj.get_value_from_key(parent_section, 'sql_username')
	password = config_obj.get_value_from_key(parent_section, 'sql_password')
	decoded_pass = decoder(password)
	connection_str = 'mssql+pymssql://' + user + ':' + decoded_pass + '@' + server_name + ':1433/' + db_name
	engine = create_engine(connection_str, isolation_level='AUTOCOMMIT', pool_recycle=1)
	return engine


def getLanguage(tenant):
	config_obj = Config_Api(tenant)
	return config_obj.get_value_from_key("sql_processing", "locale")
