# QASaveUIFile/QASaveUIFile.py
# Copyright (C) 2019 Axtria.
# This Webservice has webcalls related to creation, modificationa nd execution of Plan Modules
# Author: Saumya Agrawal <saumya.agrawal@axtria.com>

import json
import logging
from logging.handlers import TimedRotatingFileHandler
import traceback
from flask import Flask, request
from TemplatesIC.Code.Config.ConfigApi import Config_Api
from TemplatesIC.Code.DPCalculation import DPCalculation
from TemplatesIC.Code.EditInstance import EditInstance
from TemplatesIC.Code.ExecutePlan import ExecutePlan
from TemplatesIC.Code.InstanceCreator import InstanceCreator
from TemplatesIC.Code.ParseJSON import ParseJSON
from TemplatesIC.Code.UIFileCreatorForIndividual import UIFileCreatorForIndividualTemplate
from TemplatesIC.Code.PlanBuilderSQL import PlanBuilderSQL
from TemplatesIC.Code.PlanUpdateSFDC import PlanUpdateSFDC
from TemplatesIC.Code.AdapterFunctions import AdapterFunctions
from TemplatesIC.Code.AddDeleteJoin import AddDeleteJoin
from TemplatesIC.Code.config import token_required
from TemplatesIC.Code.Utility import Utility
from TemplatesIC.Code.migrate_data import MigrateData
from TemplatesIC.Code.CommonParameters import CommonParameters
from urllib import request, parse
import urllib.parse
from datetime import  datetime
import configparser
test_config = configparser.ConfigParser()
test_config._interpolation = configparser.ExtendedInterpolation()
test_config.optionxform = str
test_config.read('./Static_Config/config.ini')
log='./Logs/log1.txt'
ServicePrefix=test_config['web_service']['prefix_run_execution']
logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler(log, when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(levelname)s - %(message)s')

app = Flask(__name__)
logger = Utility.setup_logger('first_logger', './Logs/logforExecutionTime.txt')

@app.route(ServicePrefix+"/")
def Hello():
    return ServicePrefix +' webservice running'

@app.route(ServicePrefix+"/Save/", methods=['POST'])
@token_required
def Save(token):
    logging.debug('---------------------Save----------------------------------')
    tenant = request.form['tenant']
    data = request.form['data']
    fileName =request.form['filename']
    planIDFolder =request.form['planID']
    type=request.form['type']
    Saved=request.form['Saved']
    logging.debug('--------------------planID---'+planIDFolder)
    try:
        IC = InstanceCreator(tenant,type)
        if data!='':
            UFC = UIFileCreatorForIndividualTemplate(tenant,type)
            dataUpdateChange = UFC.fillValuesfromUIintoUIFile(data, planIDFolder, fileName)
            PJ = ParseJSON()
            FilePath = PJ.LoadJSONFromFile('./Static_Config/FilePath.txt')
            typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
            config_obj = Config_Api(tenant)
            configurationFile = config_obj.returnJSON(typeModule, "JSON")
            allPlanInstanceFolder = PJ.GetValueFromKeyPath(configurationFile, "Instances_Folder.Plan")
            jsonData = json.dumps(dataUpdateChange, indent=3)
            # print(jsonData)
            f = open(allPlanInstanceFolder + "\\" + planIDFolder + "\\" + fileName + ".txt", "w")
            f.write(jsonData)
            f.close()
            IC.FillWithValuesFile(planIDFolder, fileName)

        NDP = EditInstance(tenant,type)
        # Called only for a component
        if fileName != 'UI_File_Plan':
            if data!='':
                IC.FilterValues(planIDFolder)
            componentWorkflow = NDP.getComponentWorkflow(planIDFolder, fileName.split("UI_File_")[1])
            return json.dumps({'Status':'Success','Data':componentWorkflow})
        else:
            planWorkflow = NDP.getPlanWorkflow(planIDFolder)
            return json.dumps({'Status':'Success','Data':planWorkflow})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')

        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planIDFolder)
        return json.dumps({'Status':'Failed','Sfid':sfid})


@app.route(ServicePrefix+"/Plan/", methods=['POST'])
@token_required
def GetPlanUIFile(token):
    logging.debug('--------------------Plan---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    planIDFolder=request.form['planID']
    logging.debug('--------------------planID---' + planIDFolder)
    try:
        PJ = ParseJSON()
        FilePath = PJ.LoadJSONFromFile('./Static_Config/FilePath.txt')
        typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
        config_obj = Config_Api(tenant)
        configurationFile = config_obj.returnJSON(typeModule, "JSON")
        if (planIDFolder != None):
            if type=='Plan':
                file='UI_File_Plan'
            else :
                file = 'UI_File_SC'
            UI = UIFileCreatorForIndividualTemplate(tenant,type)
            allPlanInstanceFolder = PJ.GetValueFromKeyPath(configurationFile, "Instances_Folder.Plan")

            jsonData = PJ.LoadJSONFromFile(allPlanInstanceFolder + "\\" + planIDFolder + "\\" + file+".txt")
            if type=='Plan':
                jsonData = UI.UpdateComponentsValidation(jsonData)
            jsonDataList = PJ.GetValueFromKeyPath(jsonData, "Parameters")
            appendedFileName = json.dumps({"Parameters": jsonDataList, "filename": file})
            return json.dumps({'Status':'Success','Data':appendedFileName})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planIDFolder)
        return json.dumps({'Status':'Failed','Sfid':sfid})


@app.route(ServicePrefix+"/Component/", methods=['POST'])
@token_required
def GetComponentUIFile(token):
    logging.debug('--------------------Component---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    planIDFolder = request.form['planID']
    componentTemplateID = request.form['templateID']
    componentUIFileName = request.form['compUIFileName']
    compno = request.form['compno']
    logging.debug('--------------------planID---' + planIDFolder)
    try:
        PJ = ParseJSON()
        FilePath = PJ.LoadJSONFromFile('./Static_Config/FilePath.txt')
        typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
        config_obj = Config_Api(tenant)
        configurationFile = config_obj.returnJSON(typeModule, "JSON")
        columnOptions={}
        executionFile = 'False'
        IC = InstanceCreator(tenant,type)
        print(componentTemplateID)
        allPlanInstanceFolder = PJ.GetValueFromKeyPath(configurationFile, "Instances_Folder.Plan")
        if planIDFolder != None and executionFile == 'False':
            if planIDFolder != None and componentTemplateID != 'None':
                IC = InstanceCreator(tenant,type)
                print(planIDFolder, componentTemplateID)
                UIFileName = IC.SelectANewComponent(componentTemplateID,compno, planIDFolder, componentUIFileName)
            # Case when we have to return UI File for a existing component in the plan
            elif planIDFolder != None:
                UIFileName = componentUIFileName
            jsonData = PJ.LoadJSONFromFile(allPlanInstanceFolder + "\\" + planIDFolder + "\\" + UIFileName + ".txt")
            listExecutionInfo = PJ.GetAllPaths(jsonData, ["Name", "Execution Information"])
            if listExecutionInfo != []:
                index = int(listExecutionInfo[0].split("].Name")[0].split("Parameters[")[1])
                PJ.GetValueFromKeyPath(jsonData, "Parameters").pop(index)
            logging.debug('Caling map')
            columnOptions = IC.getColumnMappingInfo(planIDFolder,UIFileName.split("UI_File_")[1])
        appendedFileName = {"Parameters": PJ.GetValueFromKeyPath(jsonData, "Parameters"), "filename": UIFileName,
                            "TemplateName": IC.GetTemplateName(planIDFolder, UIFileName),"TemplateID":IC.GetTemplateID(planIDFolder, UIFileName),
                            "columnMap":columnOptions}
        return json.dumps({'Status':'Success','Data':json.dumps(appendedFileName)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planIDFolder)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/CreatePlan/", methods=['POST'])
@token_required
def CreatePlan(token):
    logging.debug('--------------------CreatePlan---------------------------------')
    type = request.form['type']
    tenant = request.form['tenant']
    portfolio = request.form['portfolio']
    try:
        PJ = ParseJSON()
        IC = InstanceCreator(tenant,type)
        if portfolio == 'Var':
            planIDFolder = IC.CreatePlanInstance('Plan_Variable_Template')
        else:
            planIDFolder = IC.CreatePlanInstance()
        return json.dumps({'Status':'Success','Data':planIDFolder})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, 'plan')
        return json.dumps({'Status':'Failed','Sfid':sfid})


@app.route(ServicePrefix+"/ShowTemplateList/", methods=['POST'])
@token_required
def ShowList(token):
    logging.debug('--------------------ShowTemplateList---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    try:
        PJ = ParseJSON()
        FilePath = PJ.LoadJSONFromFile('./Static_Config/FilePath.txt')
        typeModule = PJ.GetValueFromKeyPath(FilePath, "FilePath." + type)
        config_obj = Config_Api(tenant)
        configurationFile = config_obj.returnJSON(typeModule, "JSON")
        logging.debug(PJ.GetValueFromKeyPath(configurationFile,
                                                              "Templates_Folder.Others"))
        jsonData = PJ.LoadJSONFromFile(PJ.GetValueFromKeyPath(configurationFile,
                                                              "Templates_Folder.Others") + "\\Template_Information.txt")
        return json.dumps({'Status':'Success','Data':json.dumps(jsonData)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, 'plan')
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/GetAllDPsFile/", methods=['POST'])
@token_required
def UIFileForDPs(token):
    logging.debug('--------------------GetAllDPsFile---------------------------------')
    type= request.form['type']
    tenant= request.form['tenant']
    join= request.form['Join']
    tableID= request.form['tableID']
    component= request.form['componentID']
    planID= request.form['planID']
    entityType= request.form['entityType']
    logging.debug('--------------------planID---' + planID)
    try:
        DPC = DPCalculation(tenant,type)
        jsonData = DPC.getAllDPTypes(component, "J_" + str(join), tableID, planID, entityType)
        return json.dumps({'Status':'Success','Data':json.dumps(jsonData)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveThisDP/", methods=['POST'])
@token_required
def SaveDp(token):
    logging.debug('--------------------SaveThisDP---------------------------------')
    type = request.form['type']
    tenant = request.form['tenant']
    jsonDP = request.form['data']
    join = request.form['joinNo']
    tableID = request.form['entity']
    component = request.form['component']
    planID = request.form['planID']
    name = request.form['name']
    sequence =request.form['sequence']
    entityType = request.form['entityType']
    typedp = request.form['typedp']
    new = request.form['new']
    logging.debug('--------------------planID---' + planID)
    try:
        logging.debug('jsonDP = ')
        logging.debug(jsonDP)
        jsonDP = jsonDP.replace('***', '+').replace('*-*', '&')
        logging.debug('jsonDP == ')
        logging.debug(jsonDP)
        DPC = DPCalculation(tenant,type)
        DPC.SaveDPIntoPlan(component, "J_" + str(join), tableID, name, sequence, entityType, jsonDP, planID, typedp, new)
        EI = EditInstance(tenant,type)
        jsonDATA = EI.returnBLFileOfEntity(planID, component, "J_" + str(join), tableID, entityType)
        return json.dumps({'Status':'Success','Data':jsonDATA})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SelectAEntity/", methods=['POST'])
@token_required
def GetBLDataOfEntity(token):
    logging.debug('--------------------SelectAEntity---------------------------------')
    type= request.form['typeVal']
    tenant= request.form['tenant']
    planID= request.form['planID']
    component= request.form['component']
    joinNo= request.form['join']
    entityID= request.form['entityID']
    entityType= request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant,type)
        jsonData = EI.returnBLFileOfEntity(planID, component, "J_" + str(joinNo), entityID, entityType)
        return json.dumps({'Status':'Success','Data':jsonData})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/GetExecutePlan/", methods=['POST'])
@token_required
def getExecutionInforation(token):
    logging.debug('--------------------GetExecutePlan---------------------------------')
    tenant= request.form['tenant']
    planID= request.form['planID']
    type= request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        IC=InstanceCreator(tenant,type)
        IC.createMasterTable(planID)
        EP = ExecutePlan(tenant,type)
        executionJSON = EP.sendExecutionUIFile(planID)
        return json.dumps({'Status':'Success','Data':json.dumps(executionJSON)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/ExecutePlan/", methods=['POST'])
@token_required
def Execute(token):
    logging.debug('--------------------ExecutePlan---------------------------------')
    tenant = request.form['tenant']
    planID = request.form['planID']
    executionJson = request.form['executionJSON']
    instanceUuid = request.form['instanceUuid']
    new = request.form['new']
    executionpd = request.form['executionpd']
    Ids = request.form['Ids']
    instanceIDs = request.form['Instances']
    type = request.form['type']
    selectLatest= request.form['MapSelectLatest']
    selectedNodeExecute=request.form['SelectedNode']
    MapSelectedInstance= request.form['MapSelectedInstance']
    logging.debug('--------------------planID---' + planID)
    logging.debug(selectedNodeExecute)
    logger.debug('')
    logger.debug('Start of execution for ID of type- ' + type + ' ID : ' + planID + ' : ' + datetime.now().strftime(
        "%m/%d/%Y, %H:%M:%S"))
    Utility.setTimer(datetime.now())
    try:
        
        EP = ExecutePlan(tenant,type)
        TimeseriesJSON = EP.execute(planID, executionJson, instanceUuid, new,executionpd,Ids,instanceIDs,selectLatest,MapSelectedInstance,selectedNodeExecute)
        Ei = EditInstance(tenant, type)
        Ei.SaveTheDefinitionListNew(instanceUuid, '1')
        return json.dumps({'Status':'Success','Data':json.dumps(TimeseriesJSON)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})


@app.route(ServicePrefix+"/ExecutePlanParameters/", methods=['POST'])
@token_required
def ExecuteParameters(token):
    logging.debug('--------------------ExecutePlanParameters---------------------------------')
    planInstanceID = request.form['planID']
    tenant = request.form['tenant']
    executionParams = request.form['executionParams']
    executionpd = request.form['executionpd']
    type=request.form['type']
    Version=request.form['version']
    Ei = EditInstance(tenant, type)
    Append = request.form['Append']
    definitionID=request.form['definitionID']
    logging.debug('executionpd====')
    logging.debug(executionpd)
    logging.debug('executionParams====')
    logging.debug(executionParams)
    logging.debug('--------------------planID---' + planInstanceID)
    try:
        PS = PlanUpdateSFDC(tenant, 'Plan')
        PS.updateProcessingStatus(planInstanceID)        
        #Ei.SaveTheDefinitionListNew(planInstanceID, '1')
        difference = Utility.timeDiff(datetime.now())
        logger.debug('Metadata Accumulation : ' + str(difference))
        Utility.setTimer(datetime.now())
        if Append == 'Append':
            PBS = PlanBuilderSQL(tenant, type, Version,token,definitionID)
            PBS.createSQL(planInstanceID, executionpd, executionParams,{})
        if type=='Plan':
            paygridExecutionJson = request.form['paygridExecutionJson']
            logging.debug('--------------------paygridExecutionJson---' + planInstanceID)
            logging.debug(paygridExecutionJson)
            componentLevelExecution= request.form['componentLevelExecution']
            Ex = ExecutePlan(tenant, type)
            returnVal = Ex.noInstanceError(planInstanceID,token,componentLevelExecution)
            logging.debug("returnVal======"+returnVal)
            if returnVal == '1':
                PBS = PlanBuilderSQL(tenant,type,Version,token,definitionID)
                PBS.createSQL(planInstanceID,executionpd,executionParams,componentLevelExecution,paygridExecutionJson)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        PS = PlanUpdateSFDC(tenant, 'Plan')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planInstanceID)
        logging.debug('sfid == '+sfid)
        if(len(sfid)>4):
            PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin. Click <a target="_blank" href="/'+sfid+'">here</a> to go to logger.',planInstanceID, 'NA','')
            return json.dumps({'Status':'Failed','Sfid':sfid})
        else:
            PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin.',planInstanceID, 'NA', '')
            return json.dumps({'Status':'Failed','StatusCode':sfid})


@app.route(ServicePrefix+"/SaveJoin/", methods=['POST'])
@token_required
def SaveJoin(token):
    logging.debug('--------------------SaveJoin---------------------------------')
    type = request.form['type']
    planID = request.form['planID']
    tenant = request.form['tenant']
    data = request.form['data']
    joinNo = request.form['joinNo']
    componentID =request.form['componentID']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant,type)
        EI.saveTheJoinChanges(planID, componentID, "J_" + str(joinNo), data)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveEntityInfo/", methods=['POST'])
@token_required
def SaveEntityInfo(token):
    logging.debug('--------------------SaveEntityInfo---------------------------------')
    type = request.form['type']
    dataColumnJSON = request.form['dataColumnJSON']
    tenant = request.form['tenant']
    filterJSON = request.form['filterJSON']
    overrideJSON = request.form['overrideJSON']
    resultantInfoJSON = request.form['ResultantInfoJSON']
    derivedJSON = request.form['derivedJSON']
    planID = request.form['planID']
    componentID = request.form['componentID']
    joinNo = request.form['joinNo']
    entityID = request.form['entity']
    entityType = request.form['entityType']
    formatJSON = request.form['formatJSON']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant, type)
        EI.saveTheNodeChanges(planID, componentID, "J_" + str(joinNo), entityID, entityType, dataColumnJSON, filterJSON,
                              resultantInfoJSON, derivedJSON, overrideJSON,formatJSON)

        jsonDATA = EI.returnBLFileOfEntity(planID, componentID, "J_" + str(joinNo), entityID, entityType)
        return json.dumps({'Status':'Success','Data':jsonDATA})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/GetMTWorkflow2/", methods=['POST'])
@token_required
def GetMTWorkflow2(token):
    logging.debug('--------------------GetMTWorkflow2---------------------------------')
    type= request.form['type']
    tenant= request.form['tenant']
    planID= request.form['planID']
    regenerate= request.form['regenerate']
    logging.debug('--------------------planID---' + planID)
    try:
        IC = InstanceCreator(tenant, type)
        if regenerate == 'True' or regenerate == 'true':
            IC.regenerateMT(planID)
        data = IC.createMasterTable(planID)
        return json.dumps({'Status':'Success','Data':json.dumps(data)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/MTSelectJoin/", methods=['POST'])
@token_required
def MTSelectJoin(token):
    logging.debug('--------------------MTSelectJoin---------------------------------')
    tenant= request.form['tenant']
    planID= request.form['planID']
    type= request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant,type)
        data = EI.createJoinMT(planID)
        return json.dumps({'Status':'Success','Data':json.dumps(data)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/saveJoinMT/", methods=['POST'])
@token_required
def saveJoinMT(token):
    logging.debug('--------------------saveJoinMT---------------------------------')
    tenant = request.form['tenant']
    planID = request.form['planID']
    data = request.form['data']
    type = request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant,type)
        #data = EI.saveJoinMT(planID, data)
        EI.saveTheJoinChanges(planID, 'MasterTable', "J_1" , data)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/MTSelectOutput/", methods=['POST'])
@token_required
def MTSelectOutput(token):
    logging.debug('--------------------MTSelectOutput---------------------------------')
    tenant= request.form['tenant']
    planID= request.form['planID']
    ptype= request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant, ptype)
        IC = InstanceCreator(tenant, ptype)
        IC.createMasterTable(planID)
        data = EI.MTSelectOutput(planID)
        return json.dumps({'Status':'Success','Data':json.dumps(data)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/MTSaveColumns/" , methods=['POST'])
@token_required
def MTSaveColumns(token):
    logging.debug('--------------------MTSaveColumns---------------------------------')
    tenant = request.form['tenant']
    planID = request.form['planID']
    data = request.form['data']
    type = request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        EI = EditInstance(tenant,type)
        data = EI.MTSaveColumns(planID, data)
        return json.dumps({'Status':'Success','Data':data})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/DeleteThisDP/", methods=['POST'])
@token_required
def DeleteThisDP(token):
    logging.debug('--------------------DeleteThisDP---------------------------------')
    type= request.form['type']
    tenant= request.form['tenant']
    join= request.form['joinNo']
    tableID= request.form['entityID']
    component= request.form['component']
    planID= request.form['planID']
    entityType= request.form['entityType']
    name= request.form['name']
    sequence= request.form['sequence']
    logging.debug('--------------------planID---' + planID)
    try:
        DPC= DPCalculation(tenant,type)
        DPC.DeleteThisDP(component, "J_"+join, tableID,planID,name, sequence)
        EI = EditInstance(tenant,type)
        jsonData = EI.returnBLFileOfEntity(planID, component, "J_" + str(join), tableID, entityType)
        return json.dumps({'Status':'Success','Data':jsonData})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/deleteAPlan/", methods=['POST'])
@token_required
def deleteAPlan(token):
    logging.debug('--------------------deleteAPlan---------------------------------')
    tenant= request.form['tenant']
    planIds= request.form['planIDs']
    type= request.form['type']
    token= request.form['token']
    logging.debug('--------------------planID---' + planIds)
    try:
        IC= InstanceCreator(tenant,type)
        IC.deleteAPlan(planIds,token)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planIds)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/copyAPlan/", methods=['POST'])
@token_required
def copyAPlan(token):
    logging.debug('--------------------copyAPlan---------------------------------')
    tenant= request.form['tenant']
    planId= request.form['planID']
    type= request.form['type']
    logging.debug('--------------------planID---' + planId)
    try:
        IC= InstanceCreator(tenant,type)
        newPlanUUID = IC.copyAPlan(planId,'')
        return json.dumps({'Status':'Success','Data':newPlanUUID})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix + "/returnExecution/")
@token_required
def returnExecution(token):
    logging.debug('--------------------returnExecution---------------------------------')
    tenant = request.args.get('tenant', default=0, type=str)
    planStatus = request.args.get('planStatus', default=0, type=str)
    planLog = request.args.get('planLog', default=0, type=str)
    planSequence = request.args.get('planSequence', default=0, type=str)
    planid = request.args.get('planId', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    timetaken = request.args.get('timetaken', default=0, type=str)
    logging.debug('--------------------planID---' + planid)
    difference = Utility.timeDiff(datetime.now())
    logger.debug('Database execution(SQL) : ' + str(difference))
    Utility.setTimer(datetime.now())
    try:
        Ei = EditInstance(tenant, type)
        Ei.SaveTheDefinitionListNew(planid, '1')
        PS = PlanUpdateSFDC(tenant, type)
        retry=5
        
        PS.updateStatus(planStatus, planSequence, planLog, planid, timetaken, token) 
        logging.debug("planStatus===="+planStatus)
        if planStatus!='Success' or planStatus!='Failed' or 'GS_Plan_' not in planid:
            while retry:
                try:
                    PS.updateStatus(planStatus, planSequence, planLog, planid, timetaken, token)
                    retry=0
                except Exception as e:
                    #pass
                    error_code=Utility.getErrorCode(type(e).__name__,tenant)
                    if type(e).__name__=='SalesforceConnectionError' or type(e).__name__=='SalesforceAuthenticationFailed' or type(e).__name__=='SalesforceExpiredSession' or type(e).__name__=='SalesforceMalformedRequest' or type(e).__name__=='SalesforceRefusedRequest' or str(e).find("ConnectionError")!= -1 or str(e).find("Connection aborted")!= -1 or str(e.args).find("ConnectionError")!= -1 or str(e.args).find("Connection aborted")!= -1 or str(e).find("Expired session")!= -1 or str(e).find("Error Code")!= -1 or str(e.args).find("Expired session")!= -1 or str(e.args).find("Error Code")!= -1:
                        if retry>0:
                           PS.updateStatus(planStatus, planSequence, planLog, planid, timetaken, token)
                           retry=retry-1
                        else:
                           logging.debug("Error logged after retrying SFDC connection")
                           return json.dumps({'Status':'Failed','StatusCode':error_code})
                    else:
                        logging.debug("Error logged for exception "+ str(e))
                        retry=0
                        return json.dumps({'Status':'Failed','StatusCode':error_code})
        difference = Utility.timeDiff(datetime.now())
        logger.debug('SFDC Update : ' + str(difference))
        Utility.setTimer(datetime.now())
        EX = ExecutePlan(tenant, type)
        EX.placeQueryFile(planid, tenant)
        if planStatus.lower()=='success':
            EX.sql_to_s3(planid, planid, tenant)
        try:	
           EX.unloadIntermediate(planid, tenant, type)
        except:
           pass		
        difference = Utility.timeDiff(datetime.now())
        logger.debug('Data dump to S3 : ' + str(difference))
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        #PS = PlanUpdateSFDC(tenant, 'Plan')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planid)
        logging.debug('sfid == '+sfid)
        if(len(sfid)>4):
            #PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin. Click <a target="_blank" href="/'+sfid+'">here</a> to go to logger.',planid, 'NA', '')
            return json.dumps({'Status':'Failed','Sfid':sfid})
        else:
            PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin.',planid, 'NA', '')
            return json.dumps({'Status':'Failed','StatusCode':sfid})
        
        #PS.insert_log(e, traceback.format_exc(), 'Plan', planid)
        #return 'Failed'

@app.route(ServicePrefix+"/saveDefinition/", methods=['POST'])
@token_required
def saveDefinition(token):
    logging.debug('--------------------saveDefinition---------------------------------')
    tenant= request.form['tenant']
    planid= request.form['planid']
    Definition= request.form['Definition']
    type= request.form['type']
    logging.debug('--------------------planID---' + planid)
    try:
        Ei=EditInstance(tenant,type)
        Ei.SaveTheDefinitionListNew(planid,Definition)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/getInvestigate/", methods=['POST'])
@token_required
def getInvestigate(token):
    logging.debug('--------------------getInvestigate---------------------------------')
    tenant= request.form['tenant']
    planid= request.form['planid']
    type= request.form['type']
    logging.debug('--------------------planID---' + planid)
    try:
        Ei = EditInstance(tenant, type)
        jsonData = Ei.getListOfRSforInvestigate(planid)
        return json.dumps({'Status':'Success','Data':{"Investigate":jsonData}})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/getListing/",methods=['POST'])
@token_required
def getListing(token):
    logging.debug('-------------------getListing--------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    try:
        Ei = EditInstance(tenant, type)
        jsonData = Ei.getListing('')
        return json.dumps({'Status':'Success','Data':jsonData})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/getListingWithParameter/",methods=['POST'])
@token_required
def getListingWithParameter(token):
    logging.debug('--------------------getListingWithParameter---------------------------------')
    tenant= request.form['tenant']
    planID= request.form['planID']
    type= request.form['type']
    logging.debug('--------------------planID---' + planID)
    try:
        Ei = EditInstance(tenant, type)
        jsonData = Ei.getListing(planID)
        return json.dumps({'Status':'Success','Data':json.dumps(jsonData)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveMapping/"  ,methods=['POST'])
@token_required
def SaveMapping(token):
    logging.debug('--------------------SaveMapping---------------------------------')
    tenant = request.form['tenant']
    planId = request.form['planID']
    type = request.form['type']
    dataVal=request.form['dataValue']
    ComponentID=request.form['ComponentID']
    logging.debug('--------------------planID---' + planId)
    try:
        IC= InstanceCreator(tenant,type)
        IC.saveMapping(planId,dataVal,ComponentID.split("UI_File_")[1])
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveANewTemplate/"  ,methods=['POST'])
@token_required
def SaveANewTemplate(token):
    logging.debug('--------------------SaveANewTemplate---------------------------------')
    tenant = request.form['tenant']
    type = request.form['type']
    TemplateName=request.form['TemplateName']
    TemplateId=request.form['TemplateID']
    TemplateJSONstring=request.form['TemplateJSON']
    logging.debug('--------------------planID---' + TemplateId)
    try:
        TemplateJSON=eval(TemplateJSONstring)
        IC = InstanceCreator(tenant,type)
        IC.saveANewTemplate(TemplateId,TemplateName,TemplateJSON)
        UI= UIFileCreatorForIndividualTemplate(tenant,type)
        UI.create(TemplateId+".txt", "Component")
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})


@app.route(ServicePrefix+"/SaveAllAdapters/"  ,methods=['POST'])
@token_required
def SaveAllAdapters(token):
    logging.debug('--------------------SaveAllAdapters---------------------------------')
    tenant = request.form['tenant']
    adapterList = request.form['adapterList']
    columnJSON = request.form['columnJSON']
    function=request.form['function']
    try:
        AF= AdapterFunctions(tenant)
        AF.addAllAdapters(adapterList,columnJSON,function,tenant,token)
        return json.dumps({'Status':'Success','Data':'1'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveAllAdaptersget/")
@token_required
def SaveAllAdaptersget(token):
    tenant = request.args.get('tenant', default=0, type=str)
    adapterList = request.args.get('adapterList', default=0, type=str)
    columnJSON = request.args.get('columnJSON', default=0, type=str)
    function=request.args.get('function', default=0, type=str)
    try:
        AF= AdapterFunctions(tenant)
        AF.addAllAdapters(adapterList,columnJSON,function,tenant,token)
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})
    return json.dumps({'Status':'Success','Data':'1'})

@app.route(ServicePrefix+"/AddEntityInJoin/"  ,methods=['POST'])
@token_required
def AddEntityInJoin(token):
    logging.debug('--------------------AddEntityInJoin---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    planID= request.form['planID']
    Component= request.form['component']
    Join= "J_"+request.form['Join']
    EntityID= request.form['entityID']
    TypeToBeAdded= request.form['TypeToBeAdded']
    IDTobeAdded= request.form['IDTobeAdded']
    NameToBeAdded= request.form['NameToBeAdded']
    TableToBeAdded= request.form['TableToBeAdded']
    Position= request.form['Position']
    logging.debug('--------------------planID---' + planID)
    try:
        AD=AddDeleteJoin(tenant,type)
        workflow =  AD.addJoin(planID,EntityID,Join,Component,TypeToBeAdded,IDTobeAdded,NameToBeAdded,TableToBeAdded,Position,token)
        return json.dumps({'Status':'Success','Data':workflow})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/DeleteEntityFromJoin/"  ,methods=['POST'])
@token_required
def DeleteEntityInJoin(token):
    logging.debug('--------------------DeleteEntityFromJoin---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    planID= request.form['planID']
    Component= request.form['component']
    Join= "J_"+request.form['Join']
    EntityID= request.form['entityID']
    DeleteWithWarnings= request.form['DeleteWithWarnings']
    logging.debug('--------------------planID---' + planID)
    try:
        AD = AddDeleteJoin(tenant, type)
        returnJSON = AD.DeleteEntityInJoin(planID,Component,Join,EntityID,DeleteWithWarnings)
        return json.dumps({'Status':'Success','Data':returnJSON})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/DeleteAComponent/"  ,methods=['POST'])
@token_required
def DeleteAComponent(token):
    logging.debug('--------------------DeleteAComponent---------------------------------')
    tenant= request.form['tenant']
    type= request.form['type']
    planID= request.form['planID']
    componentID= request.form['componentID']
    logging.debug('--------------------planID---' + planID)
    try:
        IC= InstanceCreator(tenant,type)
        IC.deleteAComponent(planID,componentID)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/AddAComponentInPlan/"  ,methods=['POST'])
@token_required
def AddAComponentinPlan(token):
    logging.debug('--------------------AddAComponentInPlan---------------------------------')
    tenant = request.form['tenant']
    type = request.form['type']
    planID = request.form['planID']
    logging.debug('--------------------planID---' + planID)
    try:
        EI= EditInstance(tenant,type)
        EI.addComponent(planID)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SavePivot/"  ,methods=['POST'])
@token_required
def savePivot(token):
    logging.debug('--------------------SavePivot---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    Component =request.form['component']
    Join ='J_'+request.form['joinNo']
    EntityID =request.form['entity']
    EntityType =request.form['entityType']
    PivotJson=request.form['data']
    logging.debug('--------------------planID---' + planID)
    try:
        DC = DPCalculation(tenant,type)
        DC.savePivot(planID,Component,Join,EntityID,PivotJson,EntityType)
        EI = EditInstance(tenant, type)
        jsonDATA = EI.returnBLFileOfEntity(planID, Component, Join, EntityID, EntityType)
        return json.dumps({'Status':'Success','Data':jsonDATA})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})
    
@app.route(ServicePrefix+"/calculateDeleteDependency/"  ,methods=['POST'])
@token_required
def calculateDeleteDependency(token):
    logging.debug('--------------------calculateDeleteDependency---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    Component =request.form['Component']
    Join ='J_'+request.form['Join']
    logging.debug('--------------------planID---' + planID)
    try:
        ADJ = AddDeleteJoin(tenant,type)
        Warnings = ADJ.calculateDeleteDependencies(Component,Join,planID)
        return json.dumps({'Status':'Success','Data':json.dumps(Warnings)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/deleteJoin/"  ,methods=['GET', 'POST'])
@token_required
def deleteJoin(token):
    logging.debug('--------------------deleteJoin---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    Component =request.form['Component']
    Join ='J_'+request.form['Join']
    logging.debug('--------------------planID---' + planID)
    try:
        ADJ = AddDeleteJoin(tenant,type)
        Workflow = ADJ.deleteJoin(Component,Join,planID)
        return json.dumps({'Status':'Success','Data':Workflow})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/checkColumnValidation/"  ,methods=[ 'POST'])
@token_required
def checkColumnValidation(token):
    logging.debug('--------------------checkColumnValidation---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    logging.debug('--------------------planID---' + planID)
    try:
        EX = ExecutePlan(tenant,type)
        validation = EX.checkColumnValidation(planID)
        logging.debug('validation value --- '+validation)
        return json.dumps({'Status':'Success','Data':json.dumps({"validation":validation})})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/saveOpenStructure/"  ,methods=[ 'POST'])
@token_required
def saveOpenStructure(token):
    logging.debug('--------------------checkColumnValidation---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    nodeID = request.form['nodeID']
    typeToBeAdded = request.form['typeToBeAdded']
    IDToBeAdded = request.form['IDToBeAdded']
    nameToBeAdded = request.form['nameToBeAdded']
    tableToBeAdded = request.form['tableToBeAdded']
    component = request.form['component']
    logging.debug('--------------------planID---' + planID)
    try:
        AD=AddDeleteJoin(tenant,type)
        AD.addOpenStructureToNode(planID,nodeID,component, typeToBeAdded, IDToBeAdded, nameToBeAdded, tableToBeAdded,token)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/refreshOpenStructure/"  ,methods=[ 'POST'])
@token_required
def refreshOpenStructure(token):
    logging.debug('--------------------refreshOpenStructure---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    nodeID = request.form['nodeID']
    component = request.form['component']
    logging.debug('--------------------planID---' + planID)
    try:
        AD=AddDeleteJoin(tenant,type)
        AD.refreshOpenStructure(planID,nodeID,component,token)
        return json.dumps({'Status':'Success','Data':'Success'})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/SaveMappingForNode/"  ,methods=['POST'])
@token_required
def SaveMappingForNode(token):
    logging.debug('--------------------SaveMappingForNode---------------------------------')
    tenant =request.form['tenant']
    type =request.form['type']
    planID =request.form['planID']
    inputColumns = request.form['inputColumns']
    renameColumns = request.form['renameColumns']
    deleteColumns = request.form['deleteColumns']
    mappingColumns = request.form['mappingColumns']
    typeColumns = request.form['typeColumns']
    component = request.form['component']
    inputSet = request.form['inputSet']
    logging.debug('--------------------planID---' + planID)
    try:
        IC=InstanceCreator(tenant,type)
        jsonValue=IC.saveMappingForNode(planID,component,inputColumns,renameColumns,deleteColumns,mappingColumns,inputSet,typeColumns)
        return json.dumps({'Status':'Success','Data':json.dumps(jsonValue)})
    except Exception as e:
        logging.debug('--------------Exception-------------------------')
        sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planID)
        return json.dumps({'Status':'Failed','Sfid':sfid})

@app.route(ServicePrefix+"/updateIntermediateStatus/"  ,methods=['GET','POST'])
@token_required
def updateIntermediateStatus(token):
    logging.debug('--------------------updateIntermediateStatus---------------------------------')
    tenant = request.args.get('tenant', default=0, type=str)
    planid = request.args.get('planId', default=0, type=str)
    type = request.args.get('type', default=0, type=str)
    logging.debug('--------------------planID---' + planid)
    #PU=PlanUpdateSFDC(tenant,type)
    #PU.updateIntermediateStatus(planid)
    
    retry=5
    while retry:
        try:
            if type=='Plan':
                PU=PlanUpdateSFDC(tenant,type)
                PU.updateIntermediateStatus(planid)
                retry=0
            else:
                retry=retry-1
        except Exception as e:
            error_code=Utility.getErrorCode(type(e).__name__,tenant)
            if type(e).__name__=='SalesforceConnectionError' or type(e).__name__=='SalesforceAuthenticationFailed' or type(e).__name__=='SalesforceExpiredSession' or type(e).__name__=='SalesforceMalformedRequest' or type(e).__name__=='SalesforceRefusedRequest' or str(e).find("ConnectionError")!= -1 or str(e).find("Connection aborted")!= -1 or str(e.args).find("ConnectionError")!= -1 or str(e.args).find("Connection aborted")!= -1 or str(e).find("Expired session")!= -1 or str(e).find("Error Code")!= -1 or str(e.args).find("Expired session")!= -1 or str(e.args).find("Error Code")!= -1:
                if retry>0:
                    if type=='Plan': 
                        PU.updateIntermediateStatus(planStatus, planSequence, planLog, planid, timetaken, token)
                        retry=retry-1
                else:
                    logging.debug("Error logged after retrying SFDC connection")
                    return json.dumps({'Status':'Failed','StatusCode':error_code})
            else:
                 logging.debug("Error logged for exception "+ str(e))
                 retry=0
                 PS = PlanUpdateSFDC(tenant, 'Plan')
                 sfid=Utility.insert_log(e, traceback.format_exc(), 'Plan',tenant, planid)
                 logging.debug('sfid == '+sfid)
                 if(len(sfid)>4):
                    PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin. Click <a target="_blank" href="/'+sfid+'">here</a> to go to logger.',planid, 'NA', '')
                 else:
                    PS.updateStatus('Failed', '0', 'There are some issues with instance execution. Contact your system admin.',planid, 'NA', '')
                 return json.dumps({'Status':'Failed','StatusCode':error_code})
    return json.dumps({'Status':'Success','Data':''})

@app.route(ServicePrefix+"/migratedata/"  ,methods=['POST'])
@token_required
def migratedata(token):
    logging.debug("----------------migratedata start---------------")
    inputdata=request.form['inputdata']
    #inputdata_json=eval(inputdata)
    migration_name=request.form['name']
    migrationdata=request.form['migrationdata']
    logging.debug(inputdata)
    logging.debug(migration_name)
    logging.debug(migrationdata)
    MD=MigrateData(inputdata,migration_name,migrationdata)
    return "Success"

@app.route(ServicePrefix+"/readparameters"  ,methods=['POST'])
@token_required
def read_parameters(token):
    logging.debug("---- inside read parameters----")
    tenant = request.form.get('tenant')
    module_type = request.form.get('type')
    obj = CommonParameters(tenant, module_type, logging)
    final_data = dict()
    try:
        resp = obj.read_file()
        final_data = {"Status":"Success", "Data":resp}
    except Exception as e:
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data["Status"] = "Failed"
        sfid=Utility.insert_log(e, traceback.format_exc(), module_type, tenant, tenant)
        final_data["Sfid"] = sfid
    return json.dumps(final_data)

@app.route(ServicePrefix+"/saveparameters"  ,methods=['POST'])
@token_required
def read_parameters(token):
    logging.debug("---- inside save parameters----")
    tenant = request.form.get('tenant')
    module_type = request.form.get('type')
    param_data = urllib.parse.unquote_plus(request.form.get('param_json'), 'UTF-8', 'strict')
    obj = CommonParameters(tenant, module_type, logging)
    final_data = dict()
    try:
        resp = obj.save_json(param_data)
        final_data = {"Status":"Success", "Data":"Success"}
    except Exception as e:
        logging.debug(str(e))
        logging.error(e.args)
        logging.exception(traceback.format_exc())
        final_data["Status"] = "Failed"
        final_data["data"] = "Failed"
        sfid=Utility.insert_log(e, traceback.format_exc(), module_type, tenant, tenant)
        final_data["Sfid"] = sfid
    return json.dumps(final_data)

if __name__ == '__main__':
    app.run(debug=True)
