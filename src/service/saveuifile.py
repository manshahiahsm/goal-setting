import json
import traceback
import os
from json import JSONDecodeError
from shutil import copyfile
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce


class SaveUiFile:

    def __init__(self, uuid, tenant, my_logger, data):
        self.tenant = tenant
        self.my_logger = my_logger
        self.uuid = uuid
        self.data = data
        self.parent_uuid = ""
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")

    def run_service(self):
        """
        This method saves UI json file for quota object
        :return: status whether it is saved successfully or not
        """
        folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.uuid
        self.my_logger.debug(f"--- folder location ----- " + folder_loc)
        status = 'Failed'
        file_loc = ''
        try:
            if not os.path.exists(folder_loc):
                os.makedirs(folder_loc)
            self.my_logger.debug("--- folder exists -----")
            file_name = self.uuid + '_ui_value_info.txt'
            file_loc = folder_loc + '\\' + file_name
            self.my_logger.debug(f"---- file location ----- {file_loc}")
            with open(file_loc, 'w+') as file_obj:
                json.dump(self.data, file_obj, indent=4)
            status = 'Success'
        except Exception as e:
            raise Exception('FileNotSavedError', file_loc.rsplit('\\', 1)[1])

        return status

    def save_simulation_file(self, parent_uuid, qs_type, sys_data):
        """
        this method saves UI json file file for quota instance
        :param parent_uuid: uuid of quota object
        :param qs_type: quota instance type
        :param sys_data: system suggestion data
        :return: status whether it is saved successfully or not
        """
        status = 'Failed'
        try:
            self.parent_uuid = parent_uuid
            self.my_logger.info(" --- Inside Save Simulation file --- ")
            self.my_logger.debug(f"--- parent uuid ----- {self.parent_uuid}")
            folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid
            graph_file_loc_src = ""
            if qs_type.lower() == 'simulation':
                graph_file_loc_src = self.main_dir + "\\" + self.tenant + "\\Graph_Info.txt"
            else:
                graph_file_loc_src = self.main_dir + "\\" + self.tenant + "\\Graph_QS_Info.txt"

            graph_file_loc_dest = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid + "\\Graph_Info.txt"
            self.my_logger.debug(f"--- folder location ----- {folder_loc}")
            self.my_logger.debug(f" --- graph file location ---- {graph_file_loc_src}")

            if not os.path.exists(folder_loc):
                os.makedirs(folder_loc)

            # simulation file location
            file_name = self.uuid + '_ui_value.txt'
            file_loc = folder_loc + '\\' + file_name

            # system suggestion file location
            system_file_name = 'suggestion_json_data.txt'
            system_file_loc = folder_loc + '\\' + system_file_name

            # copy simulation json
            try:
                with open(file_loc, 'w+') as file_obj:
                    json.dump(self.data, file_obj, indent=4)
            except Exception as e:
                raise Exception('FileNotSavedError', file_loc.rsplit('\\', 1)[1])

            # copy system suggestion json
            res = not sys_data
            if not res:
                try:
                    with open(system_file_loc, 'w+') as file_obj:
                        json.dump(sys_data, file_obj, indent=4)
                except Exception as e:
                    raise Exception('FileNotSavedError', system_file_loc.rsplit('\\', 1)[1])

            # copy graph file from src to destination
            if not os.path.exists(graph_file_loc_dest):
                copyfile(graph_file_loc_src, graph_file_loc_dest)

            status = 'Success'
        except Exception as e:
            raise

        return status

    def save_graph_file(self, parent_uuid):
        """
        This method saves graph json file for quota instance
        :param parent_uuid: quota object uuid
        :return: status whether it is saved successfully or not
        """
        self.parent_uuid = parent_uuid
        self.my_logger.debug(f" --- inside save graph file --- ")
        graph_file_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid + "\\Graph_Info.txt"
        status = 'Failed'
        self.my_logger.debug(f" --- graph file location ----- {graph_file_loc}")
        try:
            with open(graph_file_loc, 'w+') as file_obj:
                json.dump(self.data, file_obj, indent=4)
            status = 'Success'
            self.my_logger.debug(f" --- graph file updated --- ")
        except Exception as e:
            raise Exception('FileNotSavedError', graph_file_loc.rsplit('\\', 1)[1])

        return status
