import json
import traceback
import uuid
import os
from os import path
from src.helper.ParseJSON import ParseJSON
from src.service.ConfigApi import Config_Api
from src.helper.salesforce import SalesForce


class UpdatePlanFile:

    def __init__(self, tenant, plan_uuid, my_logger, uuid, parent_uuid, period):
        self.tenant = tenant
        self.plan_uuid = plan_uuid
        self.my_logger = my_logger
        self.uuid = uuid
        self.parent_uuid = parent_uuid
        self.instance_file_path = ''
        self.start_date = ''
        self.end_date = ''
        self.time_period = period
        self.low_segment = ''
        self.mid_segment = ''
        self.high_segment = ''
        self.product = ''
        self.rollup_level = ''
        self.business_unit = ''
        self.Team1 = ''
        self.Team2 = ''
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.my_logger.debug("---- inside constructor ------ ")
        self.my_logger.debug(f"----- tenant  {self.tenant}")
        self.my_logger.debug(f"----- plan_uuid  {self.plan_uuid}")
        self.my_logger.debug(f"----- uuid  {self.uuid}")
        self.my_logger.debug(f"----- parent_uuid  {self.parent_uuid}")
        self.my_logger.debug(f"----- time_period  {self.time_period}")

    def update_file(self):
        try:
            plan_file_data = self.read_plan_file()
            self.my_logger.debug(f"-- type of plan file data --- ")
            self.my_logger.debug(type(plan_file_data))
            simulation_data = self.read_simulation_file()
            bu_status = self.get_bu_team_info()
            self.my_logger.debug(f"--- business unit read status --- {bu_status}")
            file_dict = self.prepare_file_dict(simulation_data)
            param_dict = self.prepare_simulation_dict(simulation_data)
            ui_file_status = self.update_ui_file()
            self.my_logger.info(f"---- UI File Status ---- {ui_file_status}")
            self.my_logger.debug(f"---------------------------")
            self.my_logger.debug(f"---- param dictionary -----")
            self.my_logger.debug(f"{param_dict}")
            self.my_logger.debug(f"---------- test start date ------------")
            self.my_logger.debug(f"{self.start_date}")
            self.my_logger.debug(f"---------- test end date ------------")
            self.my_logger.debug(f"{self.end_date}")
            self.my_logger.debug(f"------ file dictionary ----- ")
            self.my_logger.debug(f"{file_dict}")
            data_dict = plan_file_data[self.plan_uuid]

            # update plan component parameters
            date_dict = {}
            component_uuid = {}
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    self.my_logger.debug(" --- inside plan component --- ")
                    component_list = data_dict[key]
                    for component in component_list:
                        self.my_logger.debug("--- inside component List ---")
                        for k, v in component.items():
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                self.my_logger.debug(f" component_key --> {comp_key} ")
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'bl_execution_time_period':
                                    self.my_logger.debug(f"--- execution period ---- {self.time_period}")
                                    component_dict[comp_key] = self.time_period
                                elif comp_key.lower() == 'measure_parameters':
                                    self.my_logger.debug(" --- inside Measure parameter --- ")
                                    measure_param = component_dict[comp_key]
                                    self.my_logger.debug(f"--- component name --> {component_name}")
                                    for param_list in measure_param:
                                        param_name = param_list["Info_Name"]["Value"]
                                        self.my_logger.debug(f"-------- parameter -------")
                                        self.my_logger.debug(f"--- parameter name --- {param_name}")
                                        if param_name.lower() == 'rollup_level' or param_name.lower() == '$rollup_level':
                                            param_list["Param_ValueOfParameter"] = self.rollup_level
                                        elif param_name.lower() == 'business_unit' or param_name.lower() == '$business_unit':
                                            param_list["Param_ValueOfParameter"] = self.business_unit
                                        elif param_name.lower() == 'team1' or param_name.lower() == '$team1':
                                            param_list["Param_ValueOfParameter"] = self.Team1
                                        elif param_name.lower() == 'team2' or param_name.lower() == '$team2':
                                            param_list["Param_ValueOfParameter"] = self.Team2
                                        else:
                                            if param_name in param_dict[component_name]:
                                                param_list["Param_ValueOfParameter"] = param_dict[component_name][param_name]
                                            else:
                                                self.my_logger.debug(f"---- NO value found for Parameter {param_name}")
                                elif comp_key.lower() == 'resultants':
                                    self.my_logger.debug(f"--- inside resultant key plan 1 ----")
                                    date_dict[component_key] = {}
                                    join_dict = component_dict[comp_key]
                                    for join_data_key, join_data_value in join_dict.items():
                                        join_data_dict = join_dict[join_data_key]
                                        for jk, jv in join_data_dict.items():
                                            table_list = join_data_dict[jk]
                                            if jk.lower() == 'tables':
                                                self.my_logger.debug("--- inside table ----")
                                                self.my_logger.debug(type(table_list))
                                                for table_data in table_list:
                                                    file_type = table_data["Info_Entity"]["Value"]
                                                    entity_id = table_data["Info_Table_id"]["Value"]
                                                    date_dict[component_key][entity_id] = file_type
                                                    self.my_logger.debug(f"--- file type ---- {file_type}")
                                                    if file_type != 'ResultSet':
                                                        if file_type.lower() == 'alignment':
                                                            self.my_logger.debug("--- inside file type alignment--- ")
                                                            self.my_logger.debug("---- file dict alignment ----- ")
                                                            self.my_logger.debug({file_dict["All"]["Alignment"]["Info_Mapping"]})
                                                            table_data["Info_Mapping"] = file_dict["All"]["Alignment"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["All"]["Alignment"]["Value"]
                                                        elif file_type.lower() == 'sales_transpose':
                                                            self.my_logger.debug("--- inside file type sales transpose---")
                                                            self.my_logger.debug("---- file dict alignment ----- ")
                                                            self.my_logger.debug({file_dict["ST"]["forcast"]["Info_Mapping"]})
                                                            table_data["Info_Mapping"] = file_dict["ST"]["forcast"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["ST"]["forcast"]["Value"]
                                                        else:
                                                            table_data["Info_Mapping"] = file_dict[component_name]["Sales"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict[component_name]["Sales"]["Value"]
                                                    else:
                                                        uuid_value = str(uuid.uuid1())
                                                        table_data["Info_Object_Id"] = {}
                                                        table_data["Info_Object_Id"]["Value"] = uuid_value
                                                        key = str(component_key) + 'uuid'
                                                        self.my_logger.debug(f"----uuid generated key ---- {key}")
                                                        component_uuid[key] = uuid_value
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    component_list = data_dict[key]
                    for component in component_list:
                        for k, v in component.items():
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'instance_parameters':
                                    instance_dict = component_dict[comp_key]
                                    for inst_key, inst_val in instance_dict.items():
                                        filetype = date_dict[component_key][inst_key]
                                        if filetype.lower() == 'alignment':
                                            self.my_logger.debug(f"---- file dict alignment ----")
                                            self.my_logger.debug(file_dict["All"]["Alignment"])
                                            instance_dict[inst_key]["Param_Period"] = file_dict["All"]["Alignment"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["All"]["Alignment"]["Instance"]
                                        elif filetype.lower() == 'sales_transpose':
                                            self.my_logger.debug(f"---- file dict ST ----")
                                            self.my_logger.debug(file_dict["ST"]["forcast"])
                                            instance_dict[inst_key]["Param_Period"] = file_dict["ST"]["forcast"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["ST"]["forcast"]["Instance"]
                                        else:
                                            self.my_logger.debug(f"---- file dict sales ----")
                                            self.my_logger.debug(file_dict[component_name]["Sales"])
                                            instance_dict[inst_key]["Param_Period"] = file_dict[component_name]["Sales"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict[component_name]["Sales"]["Instance"]

            for key, value in data_dict.items():
                if key.lower() == 'mastertable':
                    master_dict = data_dict[key]
                    self.my_logger.debug(f"---- master dict type----")
                    self.my_logger.debug(type(master_dict))
                    for mst_key, mst_value in master_dict.items():
                        if mst_key == 'Resultants':
                            self.my_logger.debug(" *** Inside mastertable resultant set -----")
                            result_dict = master_dict[mst_key]
                            for result_key, result_value in result_dict.items():
                                join_dict = result_dict[result_key]
                                for join_key, join_val in join_dict.items():
                                    tablelist = join_dict[join_key]
                                    if join_key == 'Tables':
                                        for tableitem in tablelist:
                                            self.my_logger.debug("---- table item type ----")
                                            self.my_logger.debug(type(tableitem))
                                            self.my_logger.debug(tableitem)
                                            uuid_param = tableitem["Info_Table_id"]["Value"]
                                            uuid_param = uuid_param + "uuid"
                                            self.my_logger.debug(f"----- uuid param ----- {uuid_param}")
                                            if uuid_param in component_uuid:
                                                self.my_logger.debug(f"------ key is present ----------")
                                                uuid_value = component_uuid[uuid_param]
                                                self.my_logger.debug(f"----- uuid_value ----- {uuid_value}")
                                                tableitem["Info_Object_Id"] = uuid_value

            # self.my_logger.debug(f" final data ---------{data_dict}")
            final_data_dict = dict()
            final_data_dict[self.plan_uuid] = data_dict
            with open(self.instance_file_path, 'w+') as file_obj:
                json.dump(final_data_dict, file_obj, indent=4)
        except Exception as e:
            self.my_logger.debug(" **** unable to update file **** ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: update_file method', self.uuid)
            return "failed"

        return "Success"

    def read_plan_file(self):
        plan_file = dict()
        try:
            pjobj = ParseJSON()
            saveuifile_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            self.my_logger.info(f"---- SaveUI file Path ---- {saveuifile_path}")
            path = saveuifile_path + "\\Static_Config\\FilePath.txt"
            resource_file_data = pjobj.LoadJSONFromFile(path)
            typeModule = pjobj.GetValueFromKeyPath(resource_file_data, "FilePath.Plan")
            template_config_data = self.config_obj.returnJSON(typeModule, "JSON")
            instance_key = pjobj.GetAllPaths(template_config_data, ["Instances_Folder", "Plan"])[0].split(":->")[0]
            instance_key_data = pjobj.GetValueFromKeyPath(template_config_data, instance_key)
            self.my_logger.debug(f" instance_key_data -- {instance_key_data}")
            self.instance_file_path = instance_key_data + "\\" + self.plan_uuid + "\\" + self.plan_uuid + "_with_values.txt"
            self.my_logger.debug(f"--- instance_file_path ---- {self.instance_file_path}")
            plan_file = pjobj.LoadJSONFromFile(self.instance_file_path)
            self.my_logger.debug(f"--- plan file type ----")
            self.my_logger.debug(type(plan_file))
            self.my_logger.debug(f"----- instance file path ------- {self.instance_file_path}")
        except Exception as e:
            self.my_logger.debug(" -- error while reading plan file ----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: read_plan_file method', self.uuid)

        return plan_file

    def read_simulation_file(self):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid
        file_name = self.uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        self.my_logger.info(f"--- file location --- {file_loc}")
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
            self.my_logger.debug(f"---- data read ---- ")
        except Exception as e:
            self.my_logger.info("*** Unable to read file ****")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: read_simulation_file method', self.uuid)
            data = dict()

        return data

    def get_bu_team_info(self):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid
        file_name = self.parent_uuid + '_ui_value_info.txt'
        file_loc = folder_loc + '\\' + file_name
        data = dict()
        with open(file_loc) as json_file:
            data = json.load(json_file)
        status = False
        file_data = data["Parameters"]
        for items in file_data:
            for key, value in items.items():
                if key.lower() == "sfdc" and value.lower() == "bu__c":
                    self.business_unit = items["Value"]
                    status = True
                if key.lower() == "sfdc" and value.lower() == "team1__c":
                    if items["Value"] != "--None--" and items["Value"] != "None":
                        self.Team1 = items["Value"]
                    else:
                        self.Team1 = ""
                if key.lower() == "sfdc" and value.lower() == "team2__c":
                    if items["Value"] != "--None--" and items["Value"] != "None":
                        self.Team2 = items["Value"]
                    else:
                        self.Team2 = ""
        self.my_logger.debug(f"---- Team 1--- {self.Team1}")
        self.my_logger.debug(f"---- Team 2--- {self.Team2}")
        return status

    def prepare_simulation_dict(self, data):
        self.my_logger.debug(" --------------------------------------------------------")
        self.my_logger.debug(" inside prepare_simulation_dict method ")
        self.my_logger.debug(type(data))
        data_dict = dict()
        self.low_segment = data["segmentAnalysisLow"]
        self.mid_segment = data["segmentAnalysisMedium"]
        self.high_segment = data["segmentAnalysisHigh"]
        markers_data = data["Markers"]
        for item in markers_data:
            component_name = item["SFDC"]
            component_floor = item["Threshold_Floor"]
            component_cap = item["Threshold_Cap"]
            if component_cap is None or component_cap == '':
                component_cap = '9999999999'
            if component_floor is None or component_floor == '':
                component_floor = '-9999999999'

            component_key = item["Component_Id"]
            component_weight = item["Marker_Weight"]
            floor_var = '$' + component_key + '_Floor'
            floor_var = floor_var.lower()
            cap_var = '$' + component_key + '_Cap'
            cap_var = cap_var.lower()
            weight_var = '$' + component_key + '_Marker_weight'
            weight_var = weight_var.lower()
            data_dict[floor_var] = component_floor
            data_dict[cap_var] = component_cap
            data_dict[weight_var] = component_weight
            # prepare dict for flooring and capping parameters
            key_var_name = ''
            if component_name.lower() != "overall":
                key_var_name = "Final_Goals_" + component_key
            else:
                key_var_name = "Final_Goals_All"
            self.my_logger.debug(f" ---- key variable name ---- {key_var_name}")
            data_dict[key_var_name] = {}
            data_dict[key_var_name]["floor"] = component_floor
            data_dict[key_var_name]["cap"] = component_cap

            parameter_list = item["Params"]
            for parameter in parameter_list:
                self.my_logger.debug(f"---- component Name ----- {component_name}")
                data_dict[component_name] = {}
                data_dict[component_name]["floor"] = component_floor
                data_dict[component_name]["cap"] = component_cap

                if component_name != 'OverAll':
                    if len(parameter) == 0:
                        data_dict[component_name]["$Baseline1_startdate"] = ''
                        data_dict[component_name]["$Baseline1_enddate"] = ''
                        data_dict[component_name]["$Baseline2_enddate"] = ''
                        data_dict[component_name]["$Baseline2_startdate"] = ''
                        data_dict[component_name]["$Baseline1_weight"] = "0"
                        data_dict[component_name]["$Baseline2_weight"] = "0"
                        data_dict[component_name]["Baseline3_enddate"] = ''
                        data_dict[component_name]["Baseline3_startdate"] = ''
                        data_dict[component_name]["Baseline3_weight"] = "0"
                        data_dict[component_name]["Baseline4_enddate"] = ''
                        data_dict[component_name]["Baseline4_startdate"] = ''
                        data_dict[component_name]["Baseline4_weight"] = "0"
                        data_dict[component_name]["Baseline5_enddate"] = ''
                        data_dict[component_name]["Baseline5_startdate"] = ''
                        data_dict[component_name]["Baseline5_weight"] = "0"
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                    elif len(parameter) == 1:
                        data_dict[component_name]["$Baseline1_startdate"] = parameter[0]["Baseline_From"]
                        data_dict[component_name]["$Baseline1_enddate"] = parameter[0]["Baseline_To"]
                        data_dict[component_name]["$Baseline1_weight"] = "0" if len(parameter[0]["Baseline_Weight"])==0 else parameter[0]["Baseline_Weight"]
                        data_dict[component_name]["$Baseline2_enddate"] = ''
                        data_dict[component_name]["$Baseline2_startdate"] = ''
                        data_dict[component_name]["$Baseline2_weight"] = "0"
                        data_dict[component_name]["Baseline3_enddate"] = ''
                        data_dict[component_name]["Baseline3_startdate"] = ''
                        data_dict[component_name]["Baseline3_weight"] = "0"
                        data_dict[component_name]["Baseline4_enddate"] = ''
                        data_dict[component_name]["Baseline4_startdate"] = ''
                        data_dict[component_name]["Baseline4_weight"] = "0"
                        data_dict[component_name]["Baseline5_enddate"] = ''
                        data_dict[component_name]["Baseline5_startdate"] = ''
                        data_dict[component_name]["Baseline5_weight"] = "0"
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                    elif len(parameter) == 2:
                        data_dict[component_name]["$Baseline1_startdate"] = parameter[0]["Baseline_From"]
                        data_dict[component_name]["$Baseline1_enddate"] = parameter[0]["Baseline_To"]
                        data_dict[component_name]["$Baseline1_weight"] = "0" if len(parameter[0]["Baseline_Weight"])==0 else parameter[0]["Baseline_Weight"]
                        data_dict[component_name]["$Baseline2_enddate"] = parameter[1]["Baseline_To"]
                        data_dict[component_name]["$Baseline2_startdate"] = parameter[1]["Baseline_From"]
                        data_dict[component_name]["$Baseline2_weight"] = "0" if len(parameter[1]["Baseline_Weight"])==0 else parameter[1]["Baseline_Weight"]
                        data_dict[component_name]["Baseline3_enddate"] = ''
                        data_dict[component_name]["Baseline3_startdate"] = ''
                        data_dict[component_name]["Baseline3_weight"] = "0"
                        data_dict[component_name]["Baseline4_enddate"] = ''
                        data_dict[component_name]["Baseline4_startdate"] = ''
                        data_dict[component_name]["Baseline4_weight"] = "0"
                        data_dict[component_name]["Baseline5_enddate"] = ''
                        data_dict[component_name]["Baseline5_startdate"] = ''
                        data_dict[component_name]["Baseline5_weight"] = "0"
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                    elif len(parameter) == 3:
                        data_dict[component_name]["$Baseline1_startdate"] = parameter[0]["Baseline_From"]
                        data_dict[component_name]["$Baseline1_enddate"] = parameter[0]["Baseline_To"]
                        data_dict[component_name]["$Baseline1_weight"] = "0" if len(parameter[0]["Baseline_Weight"])==0 else parameter[0]["Baseline_Weight"]
                        data_dict[component_name]["$Baseline2_enddate"] = parameter[1]["Baseline_To"]
                        data_dict[component_name]["$Baseline2_startdate"] = parameter[1]["Baseline_From"]
                        data_dict[component_name]["$Baseline2_weight"] = "0" if len(parameter[1]["Baseline_Weight"])==0 else parameter[1]["Baseline_Weight"]
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                        data_dict[component_name]["Baseline3_enddate"] = parameter[2]["Baseline_To"]
                        data_dict[component_name]["Baseline3_startdate"] = parameter[2]["Baseline_From"]
                        data_dict[component_name]["Baseline3_weight"] = "0" if len(parameter[2]["Baseline_Weight"])==0 else parameter[2]["Baseline_Weight"]
                        data_dict[component_name]["Baseline4_enddate"] = ''
                        data_dict[component_name]["Baseline4_startdate"] = ''
                        data_dict[component_name]["Baseline4_weight"] = "0"
                        data_dict[component_name]["Baseline5_enddate"] = ''
                        data_dict[component_name]["Baseline5_startdate"] = ''
                        data_dict[component_name]["Baseline5_weight"] = "0"
                    elif len(parameter) == 4:
                        data_dict[component_name]["$Baseline1_startdate"] = parameter[0]["Baseline_From"]
                        data_dict[component_name]["$Baseline1_enddate"] = parameter[0]["Baseline_To"]
                        data_dict[component_name]["$Baseline1_weight"] = "0" if len(parameter[0]["Baseline_Weight"])==0 else parameter[0]["Baseline_Weight"]
                        data_dict[component_name]["$Baseline2_enddate"] = parameter[1]["Baseline_To"]
                        data_dict[component_name]["$Baseline2_startdate"] = parameter[1]["Baseline_From"]
                        data_dict[component_name]["$Baseline2_weight"] = "0" if len(parameter[1]["Baseline_Weight"])==0 else parameter[1]["Baseline_Weight"]
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                        data_dict[component_name]["Baseline3_enddate"] = parameter[2]["Baseline_To"]
                        data_dict[component_name]["Baseline3_startdate"] = parameter[2]["Baseline_From"]
                        data_dict[component_name]["Baseline3_weight"] = "0" if len(parameter[2]["Baseline_Weight"])==0 else parameter[2]["Baseline_Weight"]
                        data_dict[component_name]["Baseline4_enddate"] = parameter[3]["Baseline_To"]
                        data_dict[component_name]["Baseline4_startdate"] = parameter[3]["Baseline_From"]
                        data_dict[component_name]["Baseline4_weight"] = "0" if len(parameter[3]["Baseline_Weight"])==0 else parameter[3]["Baseline_Weight"]
                        data_dict[component_name]["Baseline5_enddate"] = ''
                        data_dict[component_name]["Baseline5_startdate"] = ''
                        data_dict[component_name]["Baseline5_weight"] = "0"
                    else:
                        data_dict[component_name]["$Baseline1_startdate"] = parameter[0]["Baseline_From"]
                        data_dict[component_name]["$Baseline1_enddate"] = parameter[0]["Baseline_To"]
                        data_dict[component_name]["$Baseline1_weight"] = "0" if len(parameter[0]["Baseline_Weight"])==0 else parameter[0]["Baseline_Weight"]
                        data_dict[component_name]["$Baseline2_enddate"] = parameter[1]["Baseline_To"]
                        data_dict[component_name]["$Baseline2_startdate"] = parameter[1]["Baseline_From"]
                        data_dict[component_name]["$Baseline2_weight"] = "0" if len(parameter[1]["Baseline_Weight"])==0 else parameter[1]["Baseline_Weight"]
                        data_dict[component_name]["$test_startdate"] = self.start_date
                        data_dict[component_name]["$test_enddate"] = self.end_date
                        data_dict[component_name]["Baseline3_enddate"] = parameter[2]["Baseline_To"]
                        data_dict[component_name]["Baseline3_startdate"] = parameter[2]["Baseline_From"]
                        data_dict[component_name]["Baseline3_weight"] = "0" if len(parameter[2]["Baseline_Weight"])==0 else parameter[2]["Baseline_Weight"]
                        data_dict[component_name]["Baseline4_enddate"] = parameter[3]["Baseline_To"]
                        data_dict[component_name]["Baseline4_startdate"] = parameter[3]["Baseline_From"]
                        data_dict[component_name]["Baseline4_weight"] = "0" if len(parameter[3]["Baseline_Weight"])==0 else parameter[3]["Baseline_Weight"]
                        data_dict[component_name]["Baseline5_enddate"] = parameter[4]["Baseline_To"]
                        data_dict[component_name]["Baseline5_startdate"] = parameter[4]["Baseline_From"]
                        data_dict[component_name]["Baseline5_weight"] = "0" if len(parameter[4]["Baseline_Weight"])==0 else parameter[4]["Baseline_Weight"]
                else:
                    self.my_logger.debug(f"--- inside overall ----")

        return data_dict

    def prepare_file_dict(self, data):
        self.my_logger.debug(f"---------------------------------------------")
        self.my_logger.debug("---- inside file dictionary prepration ----------")
        data_dict = dict()
        file_data = data["Parameters"]
        insert_flag = False
        for item in file_data:
            self.my_logger.debug(f"--- item type -----")
            self.my_logger.debug(type(item))
            for ikey, ivalue in item.items():
                if ikey == 'Action':
                    self.my_logger.debug(f"---- inside action -----")
                    self.my_logger.debug(f"--- value --- {ivalue}")
                    if ivalue.lower() == 'alignment':
                        self.my_logger.debug("---- items value ----")
                        self.my_logger.debug(item)
                        data_dict["All"] = {}
                        data_dict["All"]["Alignment"] = {}
                        data_dict["All"]["Alignment"]["Value"] = item["Object_Id"]
                        data_dict["All"]["Alignment"]["Info_Mapping"] = item["Info_Mapping"]
                        self.my_logger.debug(f"---- info mapping ----- ")
                        self.my_logger.debug(item["Info_Mapping"])
                        data_dict["All"]["Alignment"]["Info_Table_Columns"] = item["Info_Table_Columns"]
                        data_dict["All"]["Alignment"]["Info_Data_Type"] = item["Info_Data_Type"]
                        data_dict["All"]["Alignment"]["Date"] = item["Date"]
                        data_dict["All"]["Alignment"]["Instance"] = item["Value"]
                    elif ivalue.lower() == 'forecast':
                        data_dict["ST"] = {}
                        data_dict["ST"]["forcast"] = {}
                        data_dict["ST"]["forcast"]["Value"] = item["Object_Id"]
                        data_dict["ST"]["forcast"]["Info_Mapping"] = item["Info_Mapping"]
                        self.my_logger.debug(f"---- info mapping ----- ")
                        self.my_logger.debug(item["Info_Mapping"])
                        data_dict["ST"]["forcast"]["Info_Table_Columns"] = item["Info_Table_Columns"]
                        data_dict["ST"]["forcast"]["Info_Data_Type"] = item["Info_Data_Type"]
                        data_dict["ST"]["forcast"]["Date"] = item["Date"]
                        data_dict["ST"]["forcast"]["Instance"] = item["Value"]
                    elif "GS_" not in ivalue:
                        if not insert_flag:
                            data_dict["AllSales"] = {}
                            data_dict["AllSales"]["Sales"] = {}
                            data_dict["AllSales"]["Sales"]["Value"] = item["Object_Id"]
                            data_dict["AllSales"]["Sales"]["Info_Mapping"] = item["Info_Mapping"]
                            data_dict["AllSales"]["Sales"]["Info_Table_Columns"] = item["Info_Table_Columns"]
                            data_dict["AllSales"]["Sales"]["Info_Data_Type"] = item["Info_Data_Type"]
                            data_dict["AllSales"]["Sales"]["Date"] = item["Date"]
                            data_dict["AllSales"]["Sales"]["Instance"] = item["Value"]
                            insert_flag = True

                        data_dict[ivalue] = {}
                        data_dict[ivalue]["Sales"] = {}
                        data_dict[ivalue]["Sales"]["Value"] = item["Object_Id"]
                        data_dict[ivalue]["Sales"]["Info_Mapping"] = item["Info_Mapping"]
                        data_dict[ivalue]["Sales"]["Info_Table_Columns"] = item["Info_Table_Columns"]
                        data_dict[ivalue]["Sales"]["Info_Data_Type"] = item["Info_Data_Type"]
                        data_dict[ivalue]["Sales"]["Date"] = item["Date"]
                        data_dict[ivalue]["Sales"]["Instance"] = item["Value"]
                elif ikey == "SFDC":
                    if ivalue == "StartDate__c":
                        self.start_date = item["Value"]
                    elif ivalue == "EndDate__c":
                        self.end_date = item["Value"]
                    elif ivalue.lower() == "calculate_goal_level__c":
                        self.rollup_level = item["Value"]

        return data_dict

    def update_file1(self, input_plan_uuid, plan_parent_uuid, plan_date, current_plan_parent=''):
        input_plan = input_plan_uuid
        parent_uuid = plan_parent_uuid
        input_plan_date = plan_date
        self.my_logger.debug(f"---- updating plan 2 file ---- ")
        self.my_logger.debug(f"---- input plan uuid ----- {input_plan}")
        self.my_logger.debug(f"---- current plan parent ---- {current_plan_parent}")
        try:
            self.my_logger.debug("-------- inside update_file1 --------")
            plan_file_data = self.read_plan_file()
            self.my_logger.debug(f"----- instance file path ------- {self.instance_file_path}")
            # self.my_logger.debug(f" -- plan_file_data -- {plan_file_data}")
            simulation_data = self.read_simulation_file()
            bu_status = self.get_bu_team_info()
            self.my_logger.debug(f"--- business unit read status --- {bu_status}")
            file_dict = self.prepare_file_dict(simulation_data)
            param_dict = self.prepare_simulation_dict(simulation_data)
            rules_dict = self.get_rules_dict()
            product = self.get_product()
            ui_file_status = self.update_ui_file()
            self.my_logger.info(f"---- UI File Status ---- {ui_file_status}")
            self.my_logger.debug(f"---- product name ----")
            self.my_logger.debug(f"{self.product}")
            self.my_logger.debug(f"---------------------------")
            self.my_logger.debug(f"---- param dictionary -----")
            self.my_logger.debug(f"{param_dict}")
            self.my_logger.debug(f"---------- test start date ------------")
            self.my_logger.debug(f"{self.start_date}")
            self.my_logger.debug(f"---------- test end date ------------")
            self.my_logger.debug(f"{self.end_date}")

            self.my_logger.debug(f"------ file dictionary ----- {file_dict}")
            self.my_logger.debug(f"---- self.plan_uuid ---- {self.plan_uuid}")
            data_dict = plan_file_data[self.plan_uuid]

            # update plan component parameters
            date_dict = {}
            component_uuid = {}
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    self.my_logger.debug(" --- inside plan component --- ")
                    component_list = data_dict[key]
                    for component in component_list:
                        self.my_logger.debug("--- inside component List ---")
                        for k, v in component.items():
                            self.my_logger.debug(f"-- key1 --- {k}")
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                self.my_logger.debug(f" component_key --> {comp_key} ")
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'bl_execution_time_period':
                                    self.my_logger.debug(f"--- execution period ---- {self.time_period}")
                                    component_dict[comp_key] = self.time_period
                                elif comp_key.lower() == 'measure_parameters':
                                    self.my_logger.debug(" --- inside Measure parameter --- ")
                                    measure_param = component_dict[comp_key]
                                    self.my_logger.debug(f"component name {component_name}")
                                    for param_list in measure_param:
                                        param_name = param_list["Info_Name"]["Value"]
                                        self.my_logger.debug(f"-------- parameter -------")
                                        self.my_logger.debug(f"--- parameter name --- {param_name}")
                                        self.my_logger.debug(f"----- component name ----- {component_name}")
                                        if param_name.find("rule") != -1 or param_name.find("Rule") != -1:
                                            # wight param update
                                            if param_name.find("weight") != -1 or param_name.find("Weight") != -1:
                                                column_key_list = param_name.split("_", 1)
                                                column_key = column_key_list[0]
                                                column_key = column_key[1:]
                                                self.my_logger.info(f"--- column_key {column_key}")
                                                param_list["Param_ValueOfParameter"] = rules_dict[column_key]["Weightage"]
                                            # value update
                                            elif param_name.find("value") != -1 or param_name.find("Value") != -1:
                                                column_key_list = param_name.split("_", 1)
                                                column_key = column_key_list[0]
                                                column_key = column_key[1:]
                                                self.my_logger.info(f"--- column_key {column_key}")
                                                param_list["Param_ValueOfParameter"] = rules_dict[column_key]["Value"]
                                        elif param_name.lower() == '$low_segment' or param_name.lower() == 'low_segment':
                                            param_list["Param_ValueOfParameter"] = self.low_segment
                                        elif param_name.lower() == '$medium_segment' or param_name.lower() == 'medium_segment':
                                            param_list["Param_ValueOfParameter"] = self.mid_segment
                                        elif param_name.lower() == '$high_segment' or param_name.lower() == 'high_segment':
                                            param_list["Param_ValueOfParameter"] = self.high_segment
                                        elif param_name.lower() == '$product' or param_name.lower() == 'product_name' or param_name.lower() == '$product_name':
                                            param_list["Param_ValueOfParameter"] = self.product
                                        elif param_name.lower() == 'rollup_level' or param_name.lower() == '$rollup_level':
                                            param_list["Param_ValueOfParameter"] = self.rollup_level
                                        elif param_name.lower() == 'business_unit' or param_name.lower() == '$business_unit':
                                            param_list["Param_ValueOfParameter"] = self.business_unit
                                        elif param_name.lower() == 'team1' or param_name.lower() == '$team1':
                                            param_list["Param_ValueOfParameter"] = self.Team1
                                        elif param_name.lower() == 'team2' or param_name.lower() == '$team2':
                                            param_list["Param_ValueOfParameter"] = self.Team2
                                        else:
                                            self.my_logger.info(f"---- inside else condition for parameter -----")
                                            self.my_logger.debug(param_name)
                                            for new_key, new_value in param_dict.items():
                                                if new_key.lower() == param_name.lower():
                                                    param_name = param_name.lower()
                                                    param_list["Param_ValueOfParameter"] = param_dict[param_name]
                                                    self.my_logger.debug(f"---- markers weight  updated for {param_name}")
                                elif comp_key.lower() == 'resultants':
                                    self.my_logger.debug(f" --- inside resultants --- ")
                                    date_dict[component_key] = {}
                                    join_dict = component_dict[comp_key]
                                    join_rs_uuid = dict()
                                    rs_flag = False
                                    rs_uuid = ''
                                    for join_data_key, join_data_value in join_dict.items():
                                        join_data_dict = join_dict[join_data_key]
                                        for jk, jv in join_data_dict.items():
                                            table_list = join_data_dict[jk]
                                            if jk.lower() == 'tables':
                                                self.my_logger.debug("--- table ----")
                                                for table_data in table_list:
                                                    file_type = table_data["Info_Entity"]["Value"]
                                                    entity_id = table_data["Info_Table_id"]["Value"]
                                                    info_output = table_data["Info_Output"]["Value"]
                                                    date_dict[component_key][entity_id] = file_type
                                                    self.my_logger.debug(f"--- component key --- {component_key}")
                                                    self.my_logger.debug(f"--- file type ---- {file_type}")
                                                    self.my_logger.debug(f"--- info output --- {info_output}")
                                                    if file_type.lower() != 'resultset':
                                                        if file_type.lower() == 'alignment':
                                                            table_data["Info_Mapping"] = file_dict["All"]["Alignment"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["All"]["Alignment"]["Value"]
                                                        elif file_type.lower() == 'sales':
                                                            table_data["Info_Mapping"] = file_dict["AllSales"]["Sales"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["AllSales"]["Sales"]["Value"]
                                                        else:
                                                            if component_name.lower() == "union":
                                                                curr_object_id = table_data["Param_Object_Id"]
                                                                curr_object_id_list = curr_object_id.split("|")
                                                                curr_object_id_list[0] = current_plan_parent
                                                                new_object_id = "|".join(curr_object_id_list)
                                                                table_data["Param_Object_Id"] = new_object_id
                                                            else:
                                                                table_data["Param_Object_Id"] = parent_uuid+'|Output|Plan'

                                                    elif file_type.lower() == 'resultset' and info_output.lower() == "true":
                                                        uuid_value = None
                                                        if not rs_flag:
                                                            uuid_value = str(uuid.uuid1())
                                                            rs_uuid = uuid_value
                                                            rs_flag = True
                                                        else:
                                                            uuid_value = rs_uuid
                                                        self.my_logger.debug(f"--- rs flag --- {rs_flag}")
                                                        self.my_logger.debug(f"--- rs_uuid--- {rs_uuid}")
                                                        self.my_logger.debug(f"--- uuid value --- {uuid_value}")
                                                        table_data["Info_Object_Id"] = {}
                                                        table_data["Info_Object_Id"]["Value"] = uuid_value
                                                        key = str(component_key) + 'uuid'
                                                        rs_key = str(entity_id) + 'uuid'
                                                        join_rs_uuid[rs_key] = uuid_value
                                                        self.my_logger.debug(f"---- generated key ---- {key}")
                                                        component_uuid[key] = uuid_value
                                                    else:
                                                        rs_key = str(entity_id) + 'uuid'
                                                        uuid_value = join_rs_uuid[rs_key]
                                                        table_data["Info_Object_Id"] = {}
                                                        table_data["Info_Object_Id"]["Value"] = uuid_value

                                                    # updating floor and capping derived fields
                                                    if 'List_of_Dps' in table_data:
                                                        list_of_dps = table_data["List_of_Dps"]
                                                        for dp_dict in list_of_dps:
                                                            for dp_key, dp_value in dp_dict.items():
                                                                function_name = dp_dict[dp_key]["Info_Type"]["Value"]
                                                                var_name = dp_dict[dp_key]["BL_Name"]["Value"]
                                                                self.my_logger.debug(f"--- function name ---- {function_name}")
                                                                self.my_logger.debug(f" --- variable name ---- {var_name}")

                                                                if function_name.lower() == "auto_spread":
                                                                    dp_dict[dp_key]["BL_Floor_Val"]["Value"] = param_dict[var_name]["floor"]
                                                                    dp_dict[dp_key]["BL_Cap_Val"]["Value"] = param_dict[var_name]["cap"]
                                                                
                                                                operation_name = dp_dict[dp_key]["Info_Type"]["Value"]
                                                                if operation_name.lower() == 'computation':
                                                                    dp_name = dp_dict[dp_key]["BL_Name"]["Value"]
                                                                    dp_exp = dp_dict[dp_key]["BL_Expression"]["Value"]
                                                                    if (dp_name.find("rule") != -1 or dp_name.find("Rule") != -1) and (dp_name.find("flag") != -1 or dp_name.find("Flag") != -1):
                                                                        self.my_logger.info(f"---  1 {dp_name}")
                                                                        self.my_logger.info(f"---  2 {dp_exp}")
                                                                        
                                                                        column_key_list = dp_name.split("_", 1)
                                                                        column_key = column_key_list[0]
                                                                        self.my_logger.info(f"---  3 {column_key}")
                                                                        operator_value = rules_dict[column_key]["Operator"]
                                                                        self.my_logger.debug(f"---- operator value for key {column_key} is {operator_value}")
                                                                        operator_key = "$"+column_key+"_Operator"
                                                                        dp_exp_list = dp_exp.split(" ")
                                                                        dp_exp_list[3] = operator_value
                                                                        dp_exp_updated = " ".join(dp_exp_list)
                                                                        self.my_logger.debug(f"---- updated expression {dp_exp_updated}")
                                                                        dp_dict[dp_key]["BL_Expression"]["Value"] = dp_exp_updated

            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    component_list = data_dict[key]
                    for component in component_list:
                        for k, v in component.items():
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'instance_parameters':
                                    instance_dict = component_dict[comp_key]
                                    for inst_key, inst_val in instance_dict.items():
                                        filetype = date_dict[component_key][inst_key]
                                        self.my_logger.debug(f"------- filetype --------- {filetype}")
                                        self.my_logger.debug(f"--- component_name ---- {component_name}")
                                        if filetype == 'Alignment':
                                            instance_dict[inst_key]["Param_Period"] = file_dict["All"]["Alignment"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["All"]["Alignment"]["Instance"]
                                        elif filetype == 'Sales':
                                            instance_dict[inst_key]["Param_Period"] = file_dict["AllSales"]["Sales"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["AllSales"]["Sales"]["Instance"]
                                        else:
                                            if component_name.lower() == "union":
                                                instance_dict[inst_key]["Info_InstanceIds"] = self.plan_uuid
                                                self.my_logger.debug(f"------ input plan date ------ ")
                                                self.my_logger.debug(input_plan_date)
                                                self.my_logger.debug(type(instance_dict[inst_key]["Param_Period"]))
                                                self.my_logger.debug(isinstance(instance_dict[inst_key]["Param_Period"],dict))
                                                # instance_dict[inst_key]["Param_Period"]["Value"] = input_plan_date
                                                if isinstance(instance_dict[inst_key]["Param_Period"],dict) and "Value" in instance_dict[inst_key]["Param_Period"].keys():
                                                    instance_dict[inst_key]["Param_Period"]["Value"] = input_plan_date
                                                else:
                                                    instance_dict[inst_key]["Param_Period"] = input_plan_date
                                            else:
                                                instance_dict[inst_key]["Param_Period"] = input_plan_date
                                                instance_dict[inst_key]["Info_InstanceIds"] = input_plan

            for key, value in data_dict.items():
                if key.lower() == 'mastertable':
                    master_dict = data_dict[key]
                    self.my_logger.debug(f"---- master table dict type----")
                    self.my_logger.debug(type(master_dict))
                    for mst_key, mst_value in master_dict.items():
                        if mst_key == 'Resultants':
                            result_dict = master_dict[mst_key]
                            for result_key, result_value in result_dict.items():
                                join_dict = result_dict[result_key]
                                for join_key, join_val in join_dict.items():
                                    tablelist = join_dict[join_key]
                                    if join_key == 'Tables':
                                        self.my_logger.debug("---- inside table key ---")
                                        for tableitem in tablelist:
                                            self.my_logger.debug("---- inside table item ---- ")
                                            uuid_param = tableitem["Info_Table_id"]["Value"]
                                            uuid_param = uuid_param + "uuid"
                                            self.my_logger.debug(f"----- uuid param ----- {uuid_param}")
                                            self.my_logger.debug(f"--- component uuid ---- {component_uuid}")
                                            if uuid_param in component_uuid:
                                                self.my_logger.debug(f"------ key is present ----------")
                                                uuid_value = component_uuid[uuid_param]
                                                self.my_logger.debug(f"----- uuid_value ----- {uuid_value}")
                                                tableitem["Info_Object_Id"] = uuid_value

            # self.my_logger.debug(f" ------ final updated data ---------{data_dict}")
            final_data_dict = dict()
            final_data_dict[self.plan_uuid] = data_dict
            self.my_logger.debug(f"----- instance file path -------------")
            self.my_logger.debug(f"{self.instance_file_path}")
            with open(self.instance_file_path, 'w+') as file_obj:
                json.dump(final_data_dict, file_obj, indent=4)
        except Exception as e:
            self.my_logger.debug(" unable to update file ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: update_file1 method', self.uuid)
            return "failed"

        return "Success"

    def update_plan(self, data_dict):
        final_data_dict = dict()
        status = "Success"
        try:
            final_data_dict[self.plan_uuid] = data_dict
            self.my_logger.debug(f"----- instance file path for rules parameter update -------------")
            self.my_logger.debug(f"{self.instance_file_path}")
            with open(self.instance_file_path, 'w+') as file_obj:
                json.dump(final_data_dict, file_obj, indent=4)
        except Exception as e:
            self.my_logger.info(f"--- Unable to update plan file for rules parameter ----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: update_plan method', self.uuid)
            status = "Failed"

        return status

    def get_product(self):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid
        file_name = self.parent_uuid + '_ui_value_info.txt'
        file_loc = folder_loc + '\\' + file_name
        self.my_logger.debug(f"--- file location ---- {file_loc}")
        file_data = []
        with open(file_loc) as json_file:
            file_data = json.load(json_file)
        data = file_data["Parameters"]
        check_flag = False
        for item in data:
            check_flag = False
            for key, value in item.items():
                if key.lower() == 'sfdc' and value.lower() == 'product__c':
                    check_flag = True
                if check_flag is True:
                    for key1, value1 in item.items():
                        if key1.lower() == 'value':
                            self.product = value1
                            return value1

    def update_plan_for_suggestions(self, input_plan_uuid, plan_parent_uuid, plan_date, markers_weight):
        input_plan = input_plan_uuid
        parent_uuid = plan_parent_uuid
        input_plan_date = plan_date
        self.my_logger.debug(f"---- updating plan 2 file for suggestion ---- ")
        self.my_logger.debug(f"---- input plan uuid ----- {input_plan}")
        try:
            self.my_logger.debug("-------- inside update_plan_for_suggestions --------")
            plan_file_data = self.read_plan_file()
            self.my_logger.debug(f"----- instance file path ------- {self.instance_file_path}")
            simulation_data = self.read_simulation_file()
            bu_status = self.get_bu_team_info()
            self.my_logger.debug(f"--- business unit read status --- {bu_status}")
            file_dict = self.prepare_file_dict(simulation_data)
            param_dict = self.prepare_simulation_dict(simulation_data)
            ui_file_status = self.update_ui_file()
            self.my_logger.info(f"---- UI File Status ---- {ui_file_status}")
            for component_key, value in markers_weight.items():
                weight_var = '$' + component_key + '_Marker_weight'
                weight_var = weight_var.lower()
                param_dict[weight_var] = value
            product = self.get_product()
            self.my_logger.debug(f"---- product name ----")
            self.my_logger.debug(f"{self.product}")
            self.my_logger.debug(f"---------------------------")
            self.my_logger.debug(f"---- param dictionary -----")
            self.my_logger.debug(f"{param_dict}")
            self.my_logger.debug(f"---------- test start date ------------")
            self.my_logger.debug(f"{self.start_date}")
            self.my_logger.debug(f"---------- test end date ------------")
            self.my_logger.debug(f"{self.end_date}")

            self.my_logger.debug(f"------ file dictionary ----- {file_dict}")
            self.my_logger.debug(f"---- self.plan_uuid ---- {self.plan_uuid}")
            data_dict = plan_file_data[self.plan_uuid]

            # update plan component parameters
            date_dict = {}
            component_uuid = {}
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    self.my_logger.debug(" --- inside plan component --- ")
                    component_list = data_dict[key]
                    for component in component_list:
                        self.my_logger.debug("--- inside component List ---")
                        for k, v in component.items():
                            self.my_logger.debug(f"-- key1 --- {k}")
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                self.my_logger.debug(f" component_key --> {comp_key} ")
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'bl_execution_time_period':
                                    self.my_logger.debug(f"--- execution period ---- {self.time_period}")
                                    component_dict[comp_key] = self.time_period
                                elif comp_key.lower() == 'measure_parameters':
                                    self.my_logger.debug(" --- inside Measure parameter --- ")
                                    measure_param = component_dict[comp_key]
                                    self.my_logger.debug(f"component name {component_name}")
                                    for param_list in measure_param:
                                        param_name = param_list["Info_Name"]["Value"]
                                        self.my_logger.debug(f"-------- parameter -------")
                                        self.my_logger.debug(f"--- parameter name --- {param_name}")
                                        self.my_logger.debug(f"----- component name ----- {component_name}")
                                        if param_name.lower() == '$low_segment':
                                            param_list["Param_ValueOfParameter"] = self.low_segment
                                        elif param_name.lower() == '$medium_segment':
                                            param_list["Param_ValueOfParameter"] = self.mid_segment
                                        elif param_name.lower() == '$high_segment':
                                            param_list["Param_ValueOfParameter"] = self.high_segment
                                        elif param_name.lower() == '$product':
                                            param_list["Param_ValueOfParameter"] = self.product
                                        elif param_name.lower() == 'rollup_level' or param_name.lower() == '$rollup_level':
                                            param_list["Param_ValueOfParameter"] = self.rollup_level
                                        elif param_name.lower() == 'business_unit' or param_name.lower() == '$business_unit':
                                            param_list["Param_ValueOfParameter"] = self.business_unit
                                        elif param_name.lower() == 'team1' or param_name.lower() == '$team1':
                                            param_list["Param_ValueOfParameter"] = self.Team1
                                        elif param_name.lower() == 'team2' or param_name.lower() == '$team2':
                                            param_list["Param_ValueOfParameter"] = self.Team2
                                        else:
                                            self.my_logger.info(f" --- inside else for param update ------ ")
                                            self.my_logger.debug(param_name)
                                            for new_key, new_value in param_dict.items():
                                                if new_key.lower() == param_name.lower():
                                                    param_name = param_name.lower()
                                                    param_list["Param_ValueOfParameter"] = param_dict[param_name]
                                                    self.my_logger.info(f"---- param updated for {param_name}")
                                elif comp_key.lower() == 'resultants':
                                    self.my_logger.debug(f" --- inside resultants --- ")
                                    date_dict[component_key] = {}
                                    join_dict = component_dict[comp_key]
                                    for join_data_key, join_data_value in join_dict.items():
                                        join_data_dict = join_dict[join_data_key]
                                        for jk, jv in join_data_dict.items():
                                            table_list = join_data_dict[jk]
                                            if jk.lower() == 'tables':
                                                self.my_logger.debug("--- table ----")
                                                for table_data in table_list:
                                                    file_type = table_data["Info_Entity"]["Value"]
                                                    entity_id = table_data["Info_Table_id"]["Value"]
                                                    date_dict[component_key][entity_id] = file_type
                                                    self.my_logger.debug(f"--- file type ---- {file_type}")
                                                    if file_type != 'ResultSet':
                                                        if file_type == 'Alignment':
                                                            table_data["Info_Mapping"] = file_dict["All"]["Alignment"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["All"]["Alignment"]["Value"]
                                                        elif file_type == 'Sales':
                                                            table_data["Info_Mapping"] = file_dict["AllSales"]["Sales"]["Info_Mapping"]
                                                            table_data["Param_Object_Id"] = file_dict["AllSales"]["Sales"]["Value"]
                                                        else:
                                                            table_data["Param_Object_Id"] = parent_uuid+'|Output|Plan'

                                                    else:
                                                        uuid_value = str(uuid.uuid1())
                                                        table_data["Info_Object_Id"] = {}
                                                        table_data["Info_Object_Id"]["Value"] = uuid_value
                                                        key = str(component_key) + 'uuid'
                                                        self.my_logger.debug(f"---- generated key ---- {key}")
                                                        component_uuid[key] = uuid_value

                                                    # updating floor and capping derived fields
                                                    if 'List_of_Dps' in table_data:
                                                        list_of_dps = table_data["List_of_Dps"]
                                                        for dp_dict in list_of_dps:
                                                            for dp_key, dp_value in dp_dict.items():
                                                                function_name = dp_dict[dp_key]["Info_Type"]["Value"]
                                                                var_name = dp_dict[dp_key]["BL_Name"]["Value"]
                                                                self.my_logger.debug(f"--- function name ---- {function_name}")
                                                                self.my_logger.debug(f" --- variable name ---- {var_name}")

                                                                if function_name.lower() == "auto_spread":
                                                                    dp_dict[dp_key]["BL_Floor_Val"]["Value"] = ''
                                                                    dp_dict[dp_key]["BL_Cap_Val"]["Value"] = ''
            for key, value in data_dict.items():
                if key.lower() == 'plan_components':
                    component_list = data_dict[key]
                    for component in component_list:
                        for k, v in component.items():
                            component_key = k
                            component_dict = v
                            for comp_key, comp_item in component_dict.items():
                                component_name = component_dict["Param_Component_Name"]
                                if comp_key.lower() == 'instance_parameters':
                                    instance_dict = component_dict[comp_key]
                                    self.my_logger.debug(f"---  instance key type ---")
                                    self.my_logger.debug(type(instance_dict))
                                    for inst_key, inst_val in instance_dict.items():
                                        filetype = date_dict[component_key][inst_key]
                                        self.my_logger.debug(f"------- filetype --------- {filetype}")
                                        self.my_logger.debug(f"--- component_name ---- {component_name}")
                                        if filetype == 'Alignment':
                                            instance_dict[inst_key]["Param_Period"] = file_dict["All"]["Alignment"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["All"]["Alignment"]["Instance"]
                                        elif filetype == 'Sales':
                                            instance_dict[inst_key]["Param_Period"] = file_dict["AllSales"]["Sales"]["Date"]
                                            instance_dict[inst_key]["Info_InstanceIds"] = file_dict["AllSales"]["Sales"]["Instance"]
                                        else:
                                            instance_dict[inst_key]["Param_Period"] = input_plan_date
                                            instance_dict[inst_key]["Info_InstanceIds"] = input_plan

            for key, value in data_dict.items():
                if key.lower() == 'mastertable':
                    master_dict = data_dict[key]
                    self.my_logger.debug(f"---- master table dict type----")
                    self.my_logger.debug(type(master_dict))
                    for mst_key, mst_value in master_dict.items():
                        if mst_key == 'Resultants':
                            result_dict = master_dict[mst_key]
                            for result_key, result_value in result_dict.items():
                                join_dict = result_dict[result_key]
                                for join_key, join_val in join_dict.items():
                                    tablelist = join_dict[join_key]
                                    if join_key == 'Tables':
                                        self.my_logger.debug("---- inside table key ---")
                                        for tableitem in tablelist:
                                            self.my_logger.debug("---- inside table item ---- ")
                                            uuid_param = tableitem["Info_Table_id"]["Value"]
                                            uuid_param = uuid_param + "uuid"
                                            self.my_logger.debug(f"----- uuid param ----- {uuid_param}")
                                            self.my_logger.debug(f"--- component uuid ---- {component_uuid}")
                                            if uuid_param in component_uuid:
                                                self.my_logger.debug(f"------ key is present ----------")
                                                uuid_value = component_uuid[uuid_param]
                                                self.my_logger.debug(f"----- uuid_value ----- {uuid_value}")
                                                tableitem["Info_Object_Id"] = uuid_value

            # self.my_logger.debug(f" ------ final updated data ---------{data_dict}")
            final_data_dict = dict()
            final_data_dict[self.plan_uuid] = data_dict
            self.my_logger.debug(f"----- instance file path -------------")
            self.my_logger.debug(f"{self.instance_file_path}")
            with open(self.instance_file_path, 'w+') as file_obj:
                json.dump(final_data_dict, file_obj, indent=4)
        except Exception as e:
            self.my_logger.debug(" unable to update file ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSuggestions API', self.uuid)
            return "failed"

        return "Success"

    def get_rules_dict(self):
        self.my_logger.info("---- inside get rules dict method in update plan file ------ ")
        rules_dict = dict()
        try:
            file_status, file_data = self.get_rules_json()
            self.my_logger.info(f"--- file status ---- {file_status}")
            self.my_logger.debug(file_data)
            rule_key = 'rules_value'
            no_of_rules = self.config_obj.get_value_from_key('Quota_Setting', rule_key)
            if file_status.lower() == 'success':
                rules_list = file_data["Rules"]
                counter = 0
                for rule in rules_list:
                    counter = counter + 1
                    column_name = rule["Column"]
                    column_key_list = column_name.split("_", 1)
                    column_key = column_key_list[0]
                    rules_dict[column_key] = dict()
                    rules_dict[column_key]["Operator"] = rule["Operator"]
                    rules_dict[column_key]["Weightage"] = rule["Rule Weightage"]
                    rules_dict[column_key]["Value"] = rule["Value"]

                if counter < int(no_of_rules):
                    counter = 1
                    while counter <= int(no_of_rules):
                        column_key = 'Rule' + str(counter)
                        if not column_key in rules_dict.keys():
                            rules_dict[column_key] = dict()
                            rules_dict[column_key]["Operator"] = '='
                            rules_dict[column_key]["Weightage"] = '0'
                            rules_dict[column_key]["Value"] = '100'
                        counter = counter + 1

                self.my_logger.debug(f"--------- rules dict in if -------- ")
                self.my_logger.debug(rules_dict)
            else:
                counter = 1
                while counter <= int(no_of_rules):
                    column_key = 'Rule' + str(counter)
                    if not column_key in rules_dict.keys():
                        rules_dict[column_key] = dict()
                        rules_dict[column_key]["Operator"] = '='
                        rules_dict[column_key]["Weightage"] = '0'
                        rules_dict[column_key]["Value"] = '100'
                    counter = counter + 1
                self.my_logger.debug(f"--------- rules dict in else -------- ")
                self.my_logger.debug(rules_dict)
                return rules_dict
        except Exception as e:
            self.my_logger.info(f"---- unable to prepare rule dictionary to update plan -----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_rules_dict method', self.uuid)
        finally:
            self.my_logger.debug(f"----- final rules dictionary ----")
            self.my_logger.debug(rules_dict)
        return rules_dict

    def get_rules_json(self):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid
        system_file_name = 'suggestion_json_data.txt'
        system_file_loc = folder_loc + '\\' + system_file_name
        self.my_logger.info(f"---- rules file location ---")
        self.my_logger.debug(system_file_loc)
        data = []
        status = ''
        try:
            if path.exists(system_file_loc):
                self.my_logger.info(f"---- file exist ---- ")
                with open(system_file_loc, 'r') as json_obj:
                    data = json.load(json_obj)
                status = 'success'
            else:
                status = 'failed'
        except Exception as e:
            self.my_logger.debug(f"--- unable to read system suggestion file ---")
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_rules_json method', self.uuid)
            data = []
        return status, data

    def update_ui_file(self):
        status = "Success"
        try:
            self.my_logger.info("---- Inside update UI file method ----")
            pjobj = ParseJSON()
            saveuifile_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            self.my_logger.info(f"---- SaveUI file Path ---- {saveuifile_path}")
            path = saveuifile_path + "\\Static_Config\\FilePath.txt"
            resource_file_data = pjobj.LoadJSONFromFile(path)
            type_module = pjobj.GetValueFromKeyPath(resource_file_data, "FilePath.Plan")
            template_config_data = self.config_obj.returnJSON(type_module, "JSON")
            instance_key = pjobj.GetAllPaths(template_config_data, ["Instances_Folder", "Plan"])[0].split(":->")[0]
            instance_key_data = pjobj.GetValueFromKeyPath(template_config_data, instance_key)
            self.my_logger.debug(f" instance_key_data -- {instance_key_data}")
            file_loc = instance_key_data + "\\" + self.plan_uuid
            self.my_logger.debug(f"---- File Location ----- ")
            self.my_logger.debug(file_loc)
            files = os.listdir(file_loc)
            for file_name in files:
                self.my_logger.debug(f"--- Current files ---- ")
                self.my_logger.debug(f"{file_name}")
                if "UI_File" in file_name and file_name != "UI_File_Plan":
                    final_loc = file_loc + "\\" + file_name
                    self.my_logger.info(f"--- final location ---- ")
                    self.my_logger.info(f"{final_loc}")
                    with open(final_loc) as json_file:
                        data = json.load(json_file)

                    param_list = data["Parameters"]
                    for param in param_list:
                        for key, val in param.items():
                            if key.lower() == 'name' and val.lower() == 'execution information':
                                param_data_list = param["Params"]
                                for item in param_data_list:
                                    for ikey, ival in item.items():
                                        if ikey == "Value":
                                            item[ikey] = self.start_date
                    data["Parameters"] = param_list
                    with open(final_loc, 'w+') as file_obj:
                        json.dump(data, file_obj, indent=4)
        except Exception as e:
            self.my_logger.debug("---- Error inside Update UI file method ----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: update_ui_file method', self.uuid)
            status = "Failed"
        return status
