import traceback
import math
import src.helper.sql_connection as sql_conn
from src.helper.salesforce import SalesForce


class QuotaRefinementJob:

    def __init__(self, param):
        self.tenant = param[0]
        self.quota_id = param[1]
        self.parent_geo_level = param[2]
        self.product = param[3]
        self.quota_action = param[4]
        self.pos_code = param[5]
        self.quota_type = param[6]
        self.my_logger = param[7]
        self.distribution_type = param[8]
        self.admin_db = sql_conn.connect_to_sql_db(self.tenant, 'processing')
        self.engine = sql_conn.createEngineToAdminDB(self.tenant, 'processing')

    def run_job(self):
        """
        This method call sql procedure for refinement and approval process.
        :return: status and summary whether it is completed successfully or not
        """
        self.my_logger.info(f"--- Inside run job method ---")
        status = None
        summary = None
        admin_cursor = self.admin_db.cursor()
        try:
            product_list = self.product.split(";")
            formatted_product = "','".join(map(str, product_list))
            formatted_product = "('" + formatted_product + "')"
            self.my_logger.info(f"---- Formatted Product ----- ")
            self.my_logger.debug(f"{formatted_product}")
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            quota_query = "SELECT Id, Quota__c, BUName__c, Baseline_Sales__c, Initial_Goals__c, Is_Active__c, " \
                          "Level__c, Original_Initial_Goals__c, Parent_Position_Code__c, Parent_Position_Name__c, " \
                          "Position_Code__c, Position_Name__c, Product__c, Refined_Goals__c, Role__c, Level_Seq__c, " \
                          "Original_Market_Goals__c,Orginal_Market_Share_Goals__c,Refined_Market_Share_Goals__c," \
                          "Absolute_Growth_value__c FROM Quota_GEO_Mapping__c WHERE Quota__c = '" + self.quota_id + "' and Product__c in " + formatted_product + " "
            self.my_logger.info(f"---- quota query -----")
            self.my_logger.debug(f"{quota_query}")

            delete_query = "DELETE FROM T_Quota_Refinement WHERE Quota_MasterID = '" + self.quota_id + "' and Product in " + formatted_product + " "
            self.my_logger.info(f"---- Delete query ---- ")
            self.my_logger.debug(f"{delete_query}")

            record_id_list = []
            admin_cursor.execute(delete_query)
            self.admin_db.commit()
            self.my_logger.info(f" --- Delete query successfully executed --- ")

            refinement_list = SalesForce.query(quota_query)
            refinement_list.columns = ['Id', 'quota', 'bu_name', 'baseline_sales', 'initial_goals', 'is_active',
                                       'level', 'original_initial_goals', 'parent_pos_code', 'parent_pos_name',
                                       'position_code', 'position_name', 'product', 'refined_goals', 'roles',
                                       'level_sequence', 'original_market_goals', 'original_market_share_goals',
                                       'Refined_Market_Share_Goals', 'Absolute_Growth_value']
            self.my_logger.info(f" ---- refinement list columns ---- ")
            self.my_logger.debug(f"{refinement_list.columns}")
            data_list = []
            NULL_STR = 'NULL'
            for _, row in refinement_list.iterrows():
                temp_data_list = []
                record_id_list.append(row['Id'])
                quota_mid = NULL_STR if row['quota'] is None else row['quota']
                record_id = NULL_STR if row['Id'] is None else row['Id']
                parent_pos_code = NULL_STR if row['parent_pos_code'] is None else row['parent_pos_code']
                parent_pos_name = NULL_STR if row['parent_pos_name'] is None else row['parent_pos_name']
                position_code = NULL_STR if row['position_code'] is None else row['position_code']
                position_name = NULL_STR if row['position_name'] is None else row['position_name']
                level = NULL_STR if row['level'] is None else row['level']
                bu_name = NULL_STR if row['bu_name'] is None else row['bu_name']
                product = NULL_STR if row['product'] is None else row['product']
                baseline_sales = NULL_STR if row['baseline_sales'] is None or math.isnan(float(row['baseline_sales'])) else float(row['baseline_sales'])
                initial_goals = NULL_STR if row['initial_goals'] is None or math.isnan(float(row['initial_goals'])) else float(row['initial_goals'])
                refined_goals = NULL_STR if row['refined_goals'] is None or math.isnan(float(row['refined_goals'])) else float(row['refined_goals'])
                level_sequence = NULL_STR if row['level_sequence'] is None else int(row['level_sequence'])
                original_initial_goals = NULL_STR if row['original_initial_goals'] is None or math.isnan(float(row['original_initial_goals'])) else float(row['original_initial_goals'])
                roles = NULL_STR if row['roles'] is None else row['roles']
                market_initial_goals = NULL_STR if row['original_market_goals'] is None or math.isnan(float(row['original_market_goals'])) else float(row['original_market_goals'])
                original_market_share_goals = NULL_STR if row['original_market_share_goals'] is None or math.isnan(float(row['original_market_share_goals'])) else float(row['original_market_share_goals'])
                refined_market_share_goals = NULL_STR if row['Refined_Market_Share_Goals'] is None or math.isnan(float(row['Refined_Market_Share_Goals'])) else float(row['Refined_Market_Share_Goals'])
                absolute_growth_value = NULL_STR if row['Absolute_Growth_value'] is None or math.isnan(float(row['Absolute_Growth_value'])) else float(row['Absolute_Growth_value'])

                bu_name = bu_name.replace("'", "''")
                roles = roles.replace("'", "''")
                temp_data_list.append(quota_mid)
                temp_data_list.append(record_id)
                temp_data_list.append(parent_pos_code)
                temp_data_list.append(parent_pos_name)
                temp_data_list.append(position_code)
                temp_data_list.append(position_name)
                temp_data_list.append(level)
                temp_data_list.append(bu_name)
                temp_data_list.append(product)
                temp_data_list.append(baseline_sales)
                temp_data_list.append(initial_goals)
                temp_data_list.append(refined_goals)
                temp_data_list.append(level_sequence)
                temp_data_list.append(original_initial_goals)
                temp_data_list.append(roles)
                temp_data_list.append(market_initial_goals)
                temp_data_list.append(original_market_share_goals)
                temp_data_list.append(refined_market_share_goals)
                temp_data_list.append(absolute_growth_value)

                data_tuple = tuple(temp_data_list)
                data_list.clear()
                data_list.append(data_tuple)

                values = ', '.join(map(str, data_list))
                insert_query = "Insert into T_Quota_Refinement (Quota_MasterID,QuotaID,Parent_Position_Code," \
                               "Parent_Position_Name,Position_Code,Position_Name,Level,BU_Name,Product," \
                               "Baseline_Sales,Initial_Goals,Refined_Goals,Level_Seq,Original_Initial_Goals," \
                               "Role,Market_Initial_Goals,Original_Market_Share_Goals," \
                               "Refined_Market_Share_Goals," \
                               "Absolute_Growth_Value) VALUES {}".format(values)
                insert_query = insert_query.replace("'NULL'", "NULL")
                insert_query = insert_query.replace('"', "'")
                # self.my_logger.info(f"--- insert query ---")
                # self.my_logger.debug(f"{insert_query}")
                admin_cursor.execute(insert_query)
                self.admin_db.commit()

            parent_geo_level = self.parent_geo_level.replace("'", "''")
            for prod in product_list:
                proc_query = "EXEC sp_Quota_Refinement '" + self.quota_id + "','" + prod + "','" + parent_geo_level + "','" + self.quota_action + "','" + self.pos_code + "','" + self.quota_type + "','" + self.distribution_type + "'"
                self.my_logger.info(f" --- procedure call query for product {prod}---- ")
                self.my_logger.debug(f"{proc_query}")
                admin_cursor.execute(proc_query)
                self.admin_db.commit()
                self.my_logger.info(f"--- SQL Procedure executed successfully for {prod}----")

            values = '\',\''.join(map(str, record_id_list))
            values = "('" + values + "')"
            quota_sql_query = "SELECT QuotaID, Position_Code, Position_Name, Level, Initial_Goals, Refined_Goals, " \
                              "Baseline_Sales, Product, Quota_MasterID, Parent_Position_Name, " \
                              "Parent_Position_Code,Refined_Market_Share_Goals,Absolute_Growth_Value FROM " \
                              "T_Quota_Refinement WHERE QuotaID in {}".format(values)

            # self.my_logger.info(f"--- quota sql query ---- ")
            # self.my_logger.debug(f"{quota_sql_query}")
            self.my_logger.info(f"--- fetching data from sql to update in sfdc ---- ")
            admin_cursor.execute(quota_sql_query)
            data_list = []
            for row in admin_cursor:
                data = dict()
                data['Id'] = row.QuotaID
                data['Initial_Goals__c'] = row.Initial_Goals if row.Initial_Goals is None else float(row.Initial_Goals)
                data['Refined_Goals__c'] = row.Refined_Goals if row.Refined_Goals is None else float(row.Refined_Goals)
                data['Baseline_Sales__c'] = row.Baseline_Sales if row.Baseline_Sales is None else float(row.Baseline_Sales)
                data['Refined_Market_Share_Goals__c'] = 0 if row.Refined_Market_Share_Goals is None else float(row.Refined_Market_Share_Goals)
                data['Absolute_Growth_value__c'] = 0 if row.Absolute_Growth_Value is None else float(row.Absolute_Growth_Value)
                data_list.append(data)

            self.my_logger.debug(f"---- Data List -----")
            self.my_logger.debug(data_list)
            self.my_logger.debug(f"---- Data List Size -----")
            self.my_logger.debug(len(data_list))
            SalesForce.bulk_update('Quota_GEO_Mapping__c', data_list)

            status = 'Success'
            summary = 'Job executed successfully'
        except Exception as e:
            raise
        finally:
            self.admin_db.commit()
            admin_cursor.close()
            self.admin_db.close()

        return status, summary
