import json
import traceback
import pandas as pd
import src.helper.sql_connection as sql_conn
from sqlalchemy import text
from src.helper.s3 import S3
from src.service.ConfigApi import Config_Api
from src.helper.ParseJSON import ParseJSON
from src.helper.config import rp_config
from src.helper.salesforce import SalesForce
from json import JSONDecodeError


class DownloadLogFile:

    def __init__(self, param_list):
        self.tenant = param_list[0]
        self.sc_uuid = param_list[1]
        self.file_name = param_list[2]
        self.my_logger = param_list[3]
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.admin_db = sql_conn.connect_to_sql_db(self.tenant, 'processing')
        self.engine = sql_conn.createEngineToAdminDB(self.tenant, 'processing')
        self.file_path = None

    def get_file(self):
        """
        This method upload log file on S3
        :return: status whether it is uploaded successfully or not
        """
        self.my_logger.info(f"***** Inside get File method *****")
        status = 'Failed'
        try:
            status, plan_list = self.get_plan_list()
            self.my_logger.info(f"---- plan list status---- {status}")
            if status == 'Success':
                status, plan_name_map = self.get_plan_name_map(plan_list)
                self.my_logger.info(f"---- plan name map status ---- {status}")
                if status == 'Success':
                    self.file_path = self.main_dir + "/goal-setting/tmp/"+self.file_name+"_query_logs.xlsx"
                    writer = pd.ExcelWriter(self.file_path, engine='xlsxwriter')
                    for plan_uuid in plan_list:
                        table_exist = self.check_table_exists(plan_uuid)
                        self.my_logger.debug(f"---- Table exists for plan {plan_uuid}----- {table_exist}")
                        if table_exist:
                            table_name = "[IC_Staging.t_" + plan_uuid + "_log]"
                            query = "SELECT * FROM " + table_name + "ORDER BY Id"
                            df = pd.read_sql_query(text(query), self.engine)
                            sheet_name = plan_name_map[plan_uuid]
                            if len(sheet_name) > 31:
                                self.my_logger.info(f"--- sheet name's length is greater than 31 --- ")
                                sheet_name = sheet_name[:31]
                            df.to_excel(writer, sheet_name=sheet_name, index=False)
                    writer.save()
                    status = self.upload_on_s3()
        except Exception as e:
            raise

        return status

    def upload_on_s3(self):
        """
        upload file on s3
        :return:
        """
        self.my_logger.info(f"--- inside upload to s3 path --- ")
        try:
            S3.connect_to_s3(self.tenant)
        except Exception as e:
            raise
        filename = self.file_name + '_query_logs.xlsx'
        self.my_logger.debug(f"----- filename of file to be uploaded ---- {filename}")
        s3_bucket = self.config_obj.get_value_from_key("s3", "s3_bucket")
        s3path = rp_config['s3']['goalSettingPath'].format(bucket=s3_bucket, tenant=self.tenant, uuid=self.sc_uuid, filename=filename)
        self.my_logger.debug(f"------ S3 path -------- ")
        self.my_logger.debug(f"{s3path}")
        S3.upload_file_from_local_path(self.file_path, s3path)

        return 'Success'

    def get_plan_list(self):
        plan_list = []
        status = 'Failed'
        plan_list_loc = ''
        try:
            plan_list_loc = self.main_dir + "\\" + self.tenant + "\\plan_listing.txt"
            self.my_logger.info(f"---- plan listing file location -----")
            self.my_logger.debug(f"{plan_list_loc}")
            data = []
            with open(plan_list_loc) as json_file:
                data = json.load(json_file)

            scenario_plan_data = data[self.sc_uuid]["plan_execution"]
            for plan_item in scenario_plan_data:
                plan_uuid = plan_item["plan_uuid"]
                plan_list.append(plan_uuid)
            status = 'Success'
            self.my_logger.info(f"**** Plan uuid List ***** ")
            self.my_logger.debug(f"{plan_list}")
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', plan_list_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', plan_list_loc.rsplit('\\', 1)[1])
        except KeyError as e:
            raise Exception('KeyError', e.args)
        except Exception as e:
            raise

        return status, plan_list

    def get_plan_name_map(self, plan_list):
        self.my_logger.info(f"**** Inside get_plan_name_map method ****")
        file_name_map = {}
        status = 'Failed'
        resource_file_data = ''
        try:
            pj_obj = ParseJSON()
            template_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            path = template_path + "\\Static_Config\\FilePath.txt"
            self.my_logger.debug(f"---- Path ---- {template_path}")
            self.my_logger.debug(f"----- Inside New Code ----- ")
            resource_file_data = pj_obj.LoadJSONFromFile(path)
            type_module = pj_obj.GetValueFromKeyPath(resource_file_data, "FilePath." + "Plan")
            template_config_data = self.config_obj.returnJSON(type_module, "JSON")
            self.my_logger.debug(f"---- template config data ----- ")
            self.my_logger.debug(f"{template_config_data}")

            listing_key = pj_obj.GetAllPaths(template_config_data, ["Instances_Folder", "Listing"])[0].split(":->")[0]
            self.my_logger.info(f"****** listing_key ****** {listing_key}")

            listing_key_data = pj_obj.GetValueFromKeyPath(template_config_data, listing_key)
            self.my_logger.info(f"***** Listing key data ****** {listing_key_data}")

            counter = 1
            for plan_uuid in plan_list:
                try:
                    def_file_path = listing_key_data + "\\" + plan_uuid + ".txt"
                    def_listing = pj_obj.LoadJSONFromFile(def_file_path)
                    plan_name_path = pj_obj.GetAllPaths(def_listing, ["Name"])[0].split(":->")[0]
                    plan_name = pj_obj.GetValueFromKeyPath(def_listing, plan_name_path)
                    self.my_logger.info(f"---- Plan Name for {plan_uuid}")
                    self.my_logger.debug(f"{plan_name}")
                    file_name_map[plan_uuid] = plan_name
                    counter = counter + 1
                except Exception as e:
                    self.my_logger.info(f" **** error occurred for plan {plan_uuid}")
                    self.my_logger.error(e.args)
                    self.my_logger.exception(traceback.format_exc())
                    SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                    SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_plan_name_map method', self.sc_uuid)
                    file_name_map[plan_uuid] = 'Quota_Setting_Plan ' + str(counter)
                    counter = counter + 1
            status = 'Success'
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', resource_file_data.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', resource_file_data.rsplit('\\', 1)[1])
        except Exception as e:
            raise
        return status, file_name_map

    def check_table_exists(self, plan_uuid):
        cursor = self.admin_db.cursor()
        table_name = "[IC_staging.t_" + plan_uuid + "_log]"
        query = "IF OBJECT_ID('{0}', 'U') IS NOT NULL SELECT 1 as count ELSE SELECT 0 as count".format(table_name)
        self.my_logger.info(f"---- check table exist query ----")
        self.my_logger.debug(f"{query}")
        cursor.execute(query)
        if cursor.fetchone()[0] == 1:
            cursor.close()
            return True
        cursor.close()
        return False
