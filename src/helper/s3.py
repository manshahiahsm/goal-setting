import os
from uuid import uuid4
import boto3
import pandas as pd
import s3fs
import gzip
from datetime import datetime
from pandas import read_csv
from src.helper.config import rp_config
from src.helper.config import ahc_config
from src.helper.helpers import decoder
from src.service.ConfigApi import Config_Api
import logging
from logging.handlers import TimedRotatingFileHandler
logging.basicConfig(level=logging.DEBUG,
                    handlers=[TimedRotatingFileHandler("tmp\\logs.txt", when="midnight"),
                              logging.StreamHandler()],
                    format='%(asctime)s - %(levelname)s - %(message)s')


class S3:
    """
    This class implements boto3 API to interact with S3 service. Currently it supports the following methods:
    upload_file(): copy an input dataframe into a csv at unique S3 temp location,
    use default s3 client method to copy to desired path

    download_file(): copy csv from remote path to unique S3 path with temp filename,
    return csv as dataframe to user and delete temp file.

    copy_file(): use boto3 resource methods to loop and filter out src_path objects and copy one by one

    exists(): use try catch block to validate whether given key exists in the bucket

    list_files(): use it to get list of files at a given prefix of location

    Attributes:
        :param: src_path: source path
        :param: dest_path: destination_path
        :param:
    """
    # s3_client = boto3.client('s3', region_name=ahc_config['s3']['region_name'],
    #                         aws_access_key_id=ahc_config['s3']['access_key'],
    #                         aws_secret_access_key=decoder(ahc_config['s3']['secret_key']))
    # s3_resource = boto3.resource('s3',
    #                             aws_access_key_id=ahc_config['s3']['access_key'],
    #                             aws_secret_access_key=decoder(ahc_config['s3']['secret_key']))

    s3_client = None
    s3_resource = None

    bucket = None
    folder = None
    config_obj = None
    local_directory = ahc_config['path']['temp_path']

    @staticmethod
    def connect_to_s3(tenant):
        S3.config_obj = Config_Api(tenant)
        S3.s3_client = boto3.client('s3', region_name=S3.config_obj.get_value_from_key("s3", "s3_region_name"),
                                    aws_access_key_id=S3.config_obj.get_value_from_key("s3", "s3_access_key"),
                                    aws_secret_access_key=decoder(
                                        S3.config_obj.get_value_from_key("s3", "s3_secret_key")))
        S3.s3_resource = boto3.resource('s3',
                                        aws_access_key_id=S3.config_obj.get_value_from_key("s3", "s3_access_key"),
                                        aws_secret_access_key=decoder(
                                            S3.config_obj.get_value_from_key("s3", "s3_secret_key")))

        S3.bucket = S3.config_obj.get_value_from_key("s3", "s3_bucket")
        S3.folder = S3.config_obj.get_value_from_key("s3", "s3_folder")

    @staticmethod
    def upload_file(data_obj, remote_path):
        """For a given filename and path, upload from ./tmp local_directory to AWS incoming bucket."""
        local_path = S3.local_directory + str(uuid4())
        try:
            data_obj.to_csv(local_path, index=False)
            S3.s3_client.upload_file(local_path, S3.bucket, remote_path)
            os.remove(local_path)
        except Exception as e:
            logging.error(e.args)

    @staticmethod
    def upload_file_from_data_object(data_obj, remote_path, filename):
        """Upload data from an object to S3 with given filename"""
        try:
            keyname = filename + ".csv"
            # local path is for temp folder filename saved
            local_path = os.path.abspath(os.path.join(S3.local_directory, str(uuid4()) + keyname))
            # print("local_path",local_path)
            data_obj.to_csv(local_path, sep=',', index=False)
            # my_logger.info(f"->file_name ::{filename} remote path :: {remote_path}")
            # complete path is S3 path
            complete_path = remote_path + keyname
            S3.upload_file_from_local_path(local_path, complete_path)
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def upload_file_from_local_path(local_path, complete_path):
        """Upload data from local path to S3 and deletes file on local path"""
        # my_logger = LoggingObj.get_logger()
        try:
            # print("inside_s3_upload")
            # print(local_path,S3.bucket,complete_path)
            # my_logger = LoggingObj.get_logger()
            S3.s3_client.upload_file(local_path, S3.bucket, complete_path)
            logging.debug(f"local file is uploaded at S3 prefix:: {complete_path}")
            os.remove(local_path)
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def download_file(complete_path):
        """downloads file from s3 and returns the data frame"""
        try:
            local_path = S3.local_directory + str(uuid4())
            S3.s3_client.download_file(S3.bucket, complete_path, local_path)
            data_obj = read_csv(local_path)
            os.remove(local_path)
            return data_obj
        except Exception as e:
            logging.warning(e.args)
            logging.warning(complete_path)
            raise e

    @staticmethod
    def copy_file(src_path, dest_path):
        """Copies all the files from src_prefix to dest_prefix
            VERY IMP AND COOL::
            Replace function is to change old substring to new prefix of dest_path in case of multiple keys found
        """
        try:
            bucket_resource = S3.s3_resource.Bucket(S3.bucket)
            for obj in bucket_resource.objects.filter(Prefix=src_path):
                source = {'Bucket': S3.bucket, 'Key': obj.key}
                # replace the prefix for each file
                dest_key = obj.key.replace(src_path, dest_path)
                bucket_resource.copy(source, dest_key)
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def exists(key):
        """Validates presence of key for a given S3 bucket."""
        logging.info('Validating file presence... %s' % key)
        try:
            contents = S3.s3_client.list_objects(Bucket=S3.bucket, Prefix=key)['Contents']
            return True
        except KeyError as e:
            logging.error("File not found %s" % key)
            raise e
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def list_files(prefix: str) -> list:
        """returns all 'folders' and file names in s3 under the prefix folder"""
        try:
            keys = []
            result = S3.s3_client.list_objects(Bucket=S3.bucket, Prefix=prefix, Delimiter='/')
            print(result)
            if result.get('CommonPrefixes'):
                for o in result.get('CommonPrefixes'):
                    print("o :: ", o)
                    keys.append(o.get('Prefix'))
            if result.get('Contents'):
                for o in result.get('Contents'):
                    print("o2 :: ", o)
                    print(o.get('Key'))
                    keys.append(o.get('Key'))
            return keys
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def archive_folder(src_path, dest_path):
        """Copies all the files from src_path to dest_path: paths till folder name"""
        try:
            bucket_resource = S3.s3_resource.Bucket(S3.bucket)
            for obj in bucket_resource.objects.filter(Prefix=src_path):
                source = {'Bucket': S3.bucket, 'Key': obj.key}
                # replace the prefix for each file
                dest_key = obj.key.replace(src_path, dest_path) + datetime.now().strftime('%d_%m_%Y_%H_%S_%M')
                bucket_resource.copy(source, dest_key)
        except Exception as e:
            logging.error(e.args)

    @staticmethod
    def archive_files(local_path, remote_path):
        """For a given local path, upload to AWS incoming bucket."""
        try:
            S3.s3_client.upload_file(local_path, S3.bucket, remote_path)
            logging.debug('uploaded file at ' + remote_path)
        except Exception as e:
            logging.error(e.args)

    @staticmethod
    def get_key_names_from_folder(prefix: str) -> list:
        keys = []
        result = S3.s3_client.list_objects(Bucket=S3.bucket, Prefix=prefix, Delimiter='/')
        if result.get('Contents'):
            for o in result.get('Contents'):
                # print("o2 :: ", o)
                # print(o.get('Key'))
                keys.append(o.get('Key'))
        get_file_names_only = []
        for s in keys:
            s = s.rsplit('/', 1)
            get_file_names_only.append(s[-1])
        return get_file_names_only

    @staticmethod
    def s3_delete_all_at_prefix(prefix: str):
        try:
            bucket_resource = S3.s3_resource.Bucket(S3.bucket)
            list_of_deleted = list(bucket_resource.objects.filter(Prefix=prefix).delete())
            if list_of_deleted:
                logging.info(f"deleted all objects for S3 prefix :: {prefix}")
            else:
                logging.info(f"no objects found for S3 prefix :: {prefix}")
        except Exception as e:
            logging.error(e.args)
            raise e

    @staticmethod
    def download_file_to(remote_path, local_path):
        """ downloads file from s3 to given local path """
        try:
            S3.s3_client.download_file(S3.bucket, remote_path, local_path)
        except Exception as e:
            logging.warning(e.args)
            logging.warning(remote_path)

    @staticmethod
    def read_as_df(s3_path, tmp_file_name, header=None):
        S3.s3_resource.Bucket(S3.bucket).download_file(s3_path, rp_config['filesystem']['tmp'].format(
            filename=tmp_file_name))
        df = pd.read_csv(rp_config['filesystem']['tmp'].format(filename=tmp_file_name), header=header)
        os.remove(os.path.join('./tmp', tmp_file_name))
        return df.fillna('Null')

    @staticmethod
    def read_as_string(s3_path):
        return S3.s3_resource.Object(S3.bucket, s3_path).get()['Body'].read().decode('utf-8')

    @staticmethod
    def s3_copy_file(filename: str, source: str, destination: str):
        pass

    @staticmethod
    def read(filename, rows) -> list:
        fs_connection = s3fs.S3FileSystem(key=ahc_config['s3']['access_key'],
                                          secret=decoder(ahc_config['s3']['secret_key']))
        """Read the specific no. of rows from the S3 file.

        This utility makes sure that the file is not downloaded on the application server. This method
        calls the actual implementation based on the file extension. It is based on the
        principle of least knowledge. This allows to avoid tight coupling between clients and subsystems.

        Args:
            filename (str): filename with extension
            rows (int): no. of rows to read.

        Returns:
            list: comma separated list items where each item is the content of the row.
        """
        if filename.endswith('.gz'):
            return S3._read_gz(filename, 1 , fs_connection)
        else:
            return S3._read_default(filename, 1 ,fs_connection)

    @staticmethod
    def _read_default(filename, rows, fs_connection) -> list:
        records = []
        logging.info("Reading %s for %d rows" % (filename, rows))
        try:
            with fs_connection.open(filename, 'rb') as f:
                for _ in range(rows):
                    records.append(f.readline().decode('utf-8'))
        except Exception as e:
            raise e
        return records

    @staticmethod
    def _read_gz(filename, rows,fs_connection) -> list:
        records = []
        logging.info("Reading %s gzip file for %d rows " % (filename, rows))
        try:
            with gzip.open(fs_connection.open(filename, 'rb'), 'rt', encoding='utf-8-sig') as f:
                for _ in range(rows):
                    records.append(f.readline())
        except Exception as e:
            print ("Read error")
            raise e
        return records
    # @staticmethod
    # def s3_delete_file(filename:str,source:str):
    #     """delete a file from S3 bucket"""
    #     try:
    #         s3 = boto3.resource('s3')
    #         s3.Object('your-bucket', 'your-key').delete()
    #     except Exception as e:
    #         my_logger.error(e.args)
    #         raise e
