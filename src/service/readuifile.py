"""
@author: Aman Kumar
"""
import json
import traceback
import os
from json import JSONDecodeError
from src.helper.config import ahc_config
import src.helper.sql_connection
import pandas as pd
import datetime
import urllib.parse
from sqlalchemy import create_engine, text
from src.service.ConfigApi import Config_Api
from src.helper.ParseJSON import ParseJSON
from src.helper.salesforce import SalesForce
import os.path
from os import path
from flask import jsonify


class ReadUiFile:

    def __init__(self, tenant, my_logger):
        self.my_logger = my_logger
        self.tenant = tenant
        self.uuid = ""
        self.parent_uuid = ""
        self.engine = None
        self.config_obj = Config_Api(self.tenant)
        self.main_dir = self.config_obj.get_value_from_key("folder_location", "QuotaSetting")
        self.my_logger.info('*** Inside ReadUiFile Class ***')

    def run_service(self):
        """
        this method returns UI file for New QS object
        :return: json data
        """
        filename = "{}\\{}\\UIFile_info.txt"
        filename = filename.format(self.main_dir, self.tenant)
        self.my_logger.info(f"----- filename ------ " + filename)
        data = dict()
        try:
            with open(filename) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', filename.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', filename.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def read_ui_value_file(self, uuid):
        """
        This method returns saved UI json file for quota object
        :param uuid: quota setting object uuid
        :return: saved json file
        """
        self.uuid = uuid
        file_loc = "{}\\{}\\QuotaSettings\\{}\\{}_ui_value_info.txt"
        file_loc = file_loc.format(self.main_dir, self.tenant, self.uuid, self.uuid)
        self.my_logger.debug(f"--- filename ---- {file_loc}")
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def read_simulation_ui_file(self, parent_uuid):
        """
        This method returns UI json file for quota instance
        :param parent_uuid: quota object uuid
        :return: json data
        """
        self.parent_uuid = parent_uuid
        filename = "{}\\{}\\SimulationUI_Info.txt"
        filename = filename.format(self.main_dir, self.tenant)
        self.my_logger.info(f"----- filename ------ {filename}")
        data = dict()
        try:
            with open(filename) as json_file:
                data = json.load(json_file)

            parent_data = self.read_ui_value_file(parent_uuid)
            file_list_data = self.input_file_info(parent_data)
            PJ = ParseJSON()
            parent_data_json = json.loads(parent_data)
            self.my_logger.info(parent_data_json)
            val = PJ.GetAllPaths(parent_data_json,["Action","Others"])
            index_business_term = val[0].split('.')[0]
            index_business_term_val = PJ.GetAllPaths(parent_data_json,[index_business_term,"Business_Term"])[0].split(':->')[1]
            for i in data['Parameters']:
                if i['SFDC'] == 'Others':
                    i['Business_Term'] = index_business_term_val
                    #self.my_logger.info(i['SFDC'])
            self.my_logger.info(type(data))
            data_list = data["Parameters"]
            #self.my_logger.debug("here")
            for item in file_list_data:
                data_list.append(item)
            data["Parameters"] = data_list
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', filename.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', filename.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def read_simulation_ui_value(self, uuid, parent_uuid):
        self.parent_uuid = parent_uuid
        self.uuid = uuid
        self.my_logger.info(" --- Inside read simulation ui value file ---")
        file_loc = "{}\\{}\\QuotaSettings\\{}\\Simulation\\{}\\{}_ui_value.txt"
        file_loc = file_loc.format(self.main_dir, self.tenant, self.parent_uuid, self.uuid, self.uuid)
        self.my_logger.info(f"--- file location --- {file_loc}")
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def input_file_info(self, json_data):
        data_array = json.loads(json_data)
        data = data_array["Parameters"]
        list_data = list()
        for item in data:
            if 'Action' in item:
                list_data.append(item)
        return list_data

    def get_metrics(self, comp_id, plan_uuid, adapter, adapter_type):
        """
        This method returns adapter columns from selected plan
        :param comp_id: selected component Id
        :param plan_uuid: selected plan UUID
        :param adapter: adapter
        :param adapter_type: adapter type
        :return: json data
        """
        plan_inst_uuid = plan_uuid
        data_type = adapter
        data_adapter = adapter_type
        component_id = comp_id
        self.my_logger.debug("---- inside get metrics method -----")
        data_dict = dict()
        try:
            data_dict = {}
            pjobj = ParseJSON()
            saveuifile_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            self.my_logger.info(f"---- SaveUI file Path ---- {saveuifile_path}")
            path = saveuifile_path + "\\Static_Config\\FilePath.txt"

            try:
                resource_file_data = pjobj.LoadJSONFromFile(path)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', path.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', path.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            typeModule = pjobj.GetValueFromKeyPath(resource_file_data, "FilePath.Plan")
            template_config_data = self.config_obj.returnJSON(typeModule, "JSON")
            self.my_logger.debug(f"---- template config data ---- ")
            self.my_logger.debug(f"template_config_data")
            instance_key = pjobj.GetAllPaths(template_config_data, ["Instances_Folder", "Plan"])[0].split(":->")[0]
            instance_key_data = pjobj.GetValueFromKeyPath(template_config_data, instance_key)
            self.my_logger.debug(f" instance_key_data -- {instance_key_data}")
            instance_file_path = instance_key_data + "\\" + plan_inst_uuid + "\\" + plan_inst_uuid + "_with_values.txt"
            self.my_logger.debug(f"--- instance_file_path ---- {instance_file_path}")
            plan_file = ''
            try:
                plan_file = pjobj.LoadJSONFromFile(instance_file_path)
            except FileNotFoundError as e:
                raise Exception('FileNotFoundError', instance_file_path.rsplit('\\', 1)[1])
            except JSONDecodeError as e:
                raise Exception('JSONDecodeError', instance_file_path.rsplit('\\', 1)[1])
            except Exception as e:
                raise

            self.my_logger.debug(f"--- plan file type ----")
            self.my_logger.debug(f" --- component Id ---- {component_id}")
            main_data = pjobj.GetAllPaths(plan_file, [plan_uuid, "Plan_Components", component_id, "Resultants", "J_1", "Tables", "Info_Entity","Value" ])
            self.my_logger.debug(f"------------- main data -----------")
            self.my_logger.debug(main_data)
            main_key = ''
            self.my_logger.debug(f"--- data_adapter ---- {data_adapter}")
            self.my_logger.debug(f"--- data type ---- {data_type}")
            for row in main_data:
                items = row.split(":->")
                if data_adapter:
                    self.my_logger.debug(f"--- if condition true---")
                    self.my_logger.debug(f"-- item value -- {items[1]}")
                    if items[1] == data_adapter:
                        main_key = items[0]
                        break
                else:
                    self.my_logger.debug(f"-- item value -- {items[1]}")
                    if items[1] == data_type:
                        main_key = items[0]
                        break

            self.my_logger.debug(f"------- main key --------")
            self.my_logger.debug(f"{main_key}")
            column_key = main_key.replace("Info_Entity", "Info_Table_Columns_without_DP")
            type_key = main_key.replace("Info_Entity", "Info_Type_Columns")
            column_value = pjobj.GetValueFromKeyPath(plan_file, column_key)
            type_value = pjobj.GetValueFromKeyPath(plan_file, type_key)
            column_list = column_value.split(",")
            type_list = type_value.split(",")
            if len(column_list) != len(type_list):
                self.my_logger.debug(f"-- column list length {len(column_list)}")
                self.my_logger.debug(f"-- type list length {len(type_list)}")
                diff = len(type_list) - len(column_list)
                new_type_list = type_list[:len(type_list)-diff]
                type_value = ','.join(new_type_list)

            self.my_logger.debug(f"---------- column value ------")
            self.my_logger.debug(f"{column_value}")
            self.my_logger.debug(f"---------- type value ------")
            self.my_logger.debug(f"{type_value}")
            data_dict["columns"] = column_value
            data_dict["types"] = type_value
        except KeyError as e:
            raise Exception('KeyError', e.args)
        except Exception as e:
            raise

        return json.dumps(data_dict)

    def update_listing(self, data):
        self.my_logger.debug("---- inside update listing methods ----")
        status = 'failed'
        try:
            parent_dir = self.main_dir
            file_location = parent_dir+"\\"+self.tenant+"\\plan_listing.txt"
            self.my_logger.debug(f"--- file location ----- {file_location}")
            file_exist = os.path.exists(file_location)
            file_data = {}
            self.my_logger.debug(f"--- file exist ----- {file_exist}")
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug("  file created ")
            else:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)
            curr_uuid = ''
            curr_value = ''
            for key, value in data.items():
                curr_uuid = key
                curr_value = value

            self.my_logger.info("**************** 0 ******************")
            self.my_logger.debug(f"---- curr uuid ---- {curr_uuid}")
            self.my_logger.debug(f"---- curr value -------- {curr_value}")
            self.my_logger.debug(f"--- type of value ---- ")
            self.my_logger.debug(type(curr_value))
            type_scenario = curr_value["Type"]
            output_header_key = ''
            hierarchy_key = ''

            if type_scenario.lower() == 'simulation':
                output_header_key = "output_simulation"
                hierarchy_key = "hierarchy_simulation"
            else:
                output_header_key = "output_qs"
                hierarchy_key = "hierarchy_qs"
            self.my_logger.debug(f"--- output header key --- {output_header_key}")
            self.my_logger.debug(f" ---- hierarchy key ---- {hierarchy_key}")

            output_header = self.config_obj.get_value_from_key('Quota_Setting', output_header_key)
            hierarchy_header = self.config_obj.get_value_from_key('Quota_Setting', hierarchy_key)
            rollup_header = self.config_obj.get_value_from_key('Quota_Setting', 'rollup_header')
            curr_value["output"] = "*"
            curr_value["hierarchy"] = hierarchy_header
            curr_value["rollup_header"] = "*"
            self.my_logger.info("**************** 1 ******************")
            self.my_logger.debug(f"---- curr_value updated ----")
            self.my_logger.debug(curr_value)

            find_key = False
            for key, value in file_data.items():
                if key == curr_uuid:
                    find_key = True
                    break
            self.my_logger.info("**************** 2 ******************")
            self.my_logger.debug(f"------- find_key ------- {find_key}")
            if not find_key:
                file_data[curr_uuid] = curr_value

            with open(file_location, 'w+') as file_obj:
                self.my_logger.debug(f"--- inside writing file method ----")
                json.dump(file_data, file_obj, indent=4)
            status = 'Success'
        except Exception as e:
            self.my_logger.debug("------- error in listing methods -------")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: update_listing method', self.tenant)
            status = 'failed'

        return status

    def getplanuuid(self, suuid):
        curr_planid = ''
        self.my_logger.info(f"--- inside get plan uuid method ----- ")
        try:
            scenario_id = suuid
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)
            sequence_need = '1'
            self.my_logger.debug(f"--- scenario_id --------- {scenario_id}")
            for key, value in file_data.items():
                if key == scenario_id:
                    self.my_logger.debug(f"--- found key value ---- {key}")
                    planlist = file_data[key]["plan_execution"]
                    self.my_logger.debug(f"----- planlist ----- {planlist}")
                    for item in planlist:
                        self.my_logger.debug(f"------- item ----- {item}")
                        for itemkey, itemvalue in item.items():
                            if itemkey == "sequence":
                                self.my_logger.debug(f"---- itemvalue ----- {itemvalue}")
                                self.my_logger.debug(type(itemvalue))
                                self.my_logger.debug(f"---- sequence_need ---- {sequence_need} ")
                                self.my_logger.debug(type(sequence_need))
                                if itemvalue == sequence_need:
                                    curr_planid = item["plan_uuid"]
                                    self.my_logger.debug(f"---- curr_planid ---- {curr_planid}")
                                    self.my_logger.debug(f"---- plan uuid  ----")
                                    self.my_logger.debug(item["plan_uuid"])
                                    return curr_planid
        except Exception as e:
            self.my_logger.debug("--- error in getPlanUUID method ---")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting:getPlanUUID method', suuid)
            return curr_planid

    def getparentuuid(self, suuid):
        parent_id = ''
        self.my_logger.info(f"--- inside get parent uuid method ----- ")
        try:
            scenario_id = suuid
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            for key, value in file_data.items():
                if key == scenario_id:
                    self.my_logger.debug(f"---- parent id --------")
                    self.my_logger.debug(file_data[key]["Goal_Def_Id"])
                    parent_id = file_data[key]["Goal_Def_Id"]
                    return parent_id
        except Exception as e:
            self.my_logger.debug("--- error in getParentUUID method ----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getParentUUID method', suuid)
            return parent_id

    def getparentplan(self, plan_uuid, suuid):
        curr_parent = ''
        self.my_logger.info(f"--- inside get plan parent uuid method ----- ")
        try:
            scenario_id = suuid
            child_uuid = plan_uuid
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            for key, value in file_data.items():
                if key == scenario_id:
                    planlist = file_data[key]["plan_execution"]
                    for item in planlist:
                        for itemkey, itemvalue in item.items():
                            if itemkey == "plan_uuid":
                                if itemvalue == child_uuid:
                                    curr_parent = item["parent_uuid"]
                                    return curr_parent
        except Exception as e:
            self.my_logger.debug("--- error getParentPlan method ---")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getParentPlan method', suuid)
            return curr_parent

    def hasnextplan(self, plan_uuid):
        next_plan_id = ''
        curr_plan_id = plan_uuid
        self.my_logger.debug("----- has  next plan ---- ")
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            curr_sequence = 0
            next_sequence = 0
            curr_instance_found = False
            next_sequence_found = False
            main_key = ''
            for key, value in file_data.items():
                planlist = file_data[key]["plan_execution"]
                for item in planlist:
                    for itemkey, itemvalue in item.items():
                        if itemkey == "plan_uuid":
                            if itemvalue == curr_plan_id:
                                curr_sequence = item["sequence"]
                                curr_instance_found = True
                                main_key = key

            self.my_logger.debug(f"----- current sequence ---- {curr_sequence}")
            self.my_logger.debug(f"---- current inst found --- {curr_instance_found}")
            self.my_logger.debug(f"----- main key --------- {main_key}")
            if curr_instance_found:
                next_sequence = int(curr_sequence) + 1
                self.my_logger.debug(f"--- next sequence ---- {next_sequence} ")
                for key, value in file_data.items():
                    if key == main_key:
                        planlist = file_data[key]["plan_execution"]
                        for item in planlist:
                            for itemkey, itemvalue in item.items():
                                if itemkey == "sequence":
                                    if int(itemvalue) == int(next_sequence):
                                        self.my_logger.debug(f"------ next seq. found -----")
                                        next_sequence_found = True
                                        return next_sequence_found
        except Exception as e:
            self.my_logger.debug(f"---- unable to get next plan -----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: hasNextPlan method', plan_uuid)
            return False

        return False

    def getnextplandata(self, plan_uuid):
        plan_data = dict()
        plan_data["period"] = ''
        plan_data["plan_uuid"] = ''
        plan_data["uuid_param"] = ''
        plan_data["parent_uuid"] = ''
        plan_data["current_plan_parent"] = ''
        plan_data["rollup_flag"] = "false"
        next_plan_id = ''
        curr_plan_id = plan_uuid
        self.my_logger.debug("----- get next plan ---- ")
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            curr_sequence = 0
            next_sequence = 0
            curr_instance_found = False
            next_sequence_found = False
            main_key = ''
            for key, value in file_data.items():
                planlist = file_data[key]["plan_execution"]
                for item in planlist:
                    for itemkey, itemvalue in item.items():
                        if itemkey == "plan_uuid":
                            if itemvalue == curr_plan_id:
                                curr_sequence = item["sequence"]
                                curr_instance_found = True
                                main_key = key
                                plan_data["uuid_param"] = main_key

            self.my_logger.debug(f"---  key ------- {main_key}")
            self.my_logger.debug(f"----- current sequence ---- {curr_sequence}")
            self.my_logger.debug(f"---- current inst found --- {curr_instance_found}")

            if curr_instance_found:
                next_sequence = int(curr_sequence) + 1
                self.my_logger.debug(f"--- next sequence ---- {next_sequence} ")
                for key, value in file_data.items():
                    if key == main_key:
                        plan_data["parent_uuid"] = file_data[key]["Goal_Def_Id"]
                        plan_data["period"] = file_data[key]["Execution_Period"]
                        planlist = file_data[key]["plan_execution"]
                        for item in planlist:
                            for itemkey, itemvalue in item.items():
                                if itemkey == "sequence":
                                    if int(itemvalue) == int(next_sequence):
                                        next_plan_id = item["plan_uuid"]
                                        curr_parent_uuid = item["parent_uuid"]
                                        plan_data["plan_uuid"] = next_plan_id
                                        plan_data["current_plan_parent"] = curr_parent_uuid
                                        if 'rollup_flag' in item.keys():
                                            plan_data["rollup_flag"] = item["rollup_flag"]
                                        self.my_logger.debug(f"------ next seq. found -----{next_plan_id}")
                                        return json.dumps(plan_data)
        except Exception as e:
            self.my_logger.debug(f"---- unable to get next plan -----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getNextPlanData method', plan_uuid)
            return json.dumps(plan_data)

        return json.dumps(plan_data)

    def getsfdcid(self, def_id):
        sc_uuid = def_id
        sfdc_id = ''
        self.my_logger.debug(f"---- inside get sfdc id -----")
        self.my_logger.debug(f"--- definition id ----- {sc_uuid}")
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            for key, value in file_data.items():
                if key == sc_uuid:
                    sfdc_id = file_data[key]["Scenario_Id"]
                    return sfdc_id
        except Exception as e:
            self.my_logger.debug("--- unable to get sfdc id -----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: getSfdcId method', def_id)
            return sfdc_id

    def getscenarioid(self, plan_uuid):
        self.my_logger.debug(f"---- inside get scenario method -----")
        curr_planid = plan_uuid
        scenario_id = ''
        try:
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            file_data = {}
            with open(file_location) as json_file:
                file_data = json.load(json_file)

            for key, value in file_data.items():
                plan_list = file_data[key]["plan_execution"]
                for plan_item in plan_list:
                    for itemkey, itemvalue in plan_item.items():
                        if itemkey == "plan_uuid":
                            if itemvalue == curr_planid:
                                scenario_id = key
                                return scenario_id
        except Exception as e:
            self.my_logger.debug("---- error while reading file -----")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting:getScenarioId method', plan_uuid)

        return scenario_id

    def getfloordata(self, data):
        self.my_logger.debug("---- inside get floor data -----")
        data_dict = dict()
        markers_data = data["Markers"]
        floor_value = ""
        checked = False
        for item in markers_data:
            self.my_logger.debug(f"--- component id ---- ")
            self.my_logger.debug(item["Component_Id"])
            if checked == False:
                floor_value = str(item["Threshold_Floor"])
                checked = True
            else:
                floor_value = floor_value + "," + str(item["Threshold_Floor"])

        self.my_logger.debug(f"---- final floor value ----- {floor_value}")
        return floor_value

    def getcappingdata(self, data ):
        self.my_logger.debug("---- inside get floor data -----")
        data_dict = dict()
        markers_data = data["Markers"]
        cap_value = ""
        checked = False
        for item in markers_data:
            self.my_logger.debug(f"--- component id ---- ")
            self.my_logger.debug(item["Component_Id"])
            if checked == False:
                cap_value = str(item["Threshold_Cap"])
                checked = True
            else:
                cap_value = cap_value + "," +str(item["Threshold_Cap"])

        self.my_logger.debug(f"---- final cap value ----- {cap_value}")
        return cap_value

    def getscenariotype(self, suuid):
        self.my_logger.debug(f"---- insdie get scenario type method ----")
        scenario_id = suuid
        parent_dir = self.main_dir
        file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
        self.my_logger.debug(f"--- file location ----- {file_location}")
        file_data = {}
        with open(file_location) as json_file:
            file_data = json.load(json_file)
        type_value = ""
        for key, value in file_data.items():
            if key == scenario_id:
                type_value = file_data[key]["Type"]
                return type_value

        return type_value

    def get_graph_json(self):
        """
        This method returns graph json data
        :return: json data
        """
        self.my_logger.info("*** Inside getGraphJson ****")
        filename = self.main_dir + "\\" + self.tenant + '\\Graph_Info.txt'
        self.my_logger.info(f"----- filename ------ " + filename)
        data = dict()
        try:
            with open(filename) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', filename.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', filename.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)
    
    def get_graph_data(self, scenario_uuid, view_at, parent_uuid):
        """
        This method returns graph sql data for quota instance
        :param scenario_uuid: quota instance uuid
        :param view_at: geo level
        :param parent_uuid: quota object uuid
        :return: json data
        """
        final_data = {}
        file_location = ''
        try:
            self.my_logger.debug(f" --- inside get graph data method --- ")
            self.engine = src.helper.sql_connection.createEngineToAdminDB(self.tenant, 'processing')
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            with open(file_location) as json_file:
                file_data = json.load(json_file)
            uuid_data = file_data[scenario_uuid]
            hierarchy_column = ""
            rollup_column = ""
            for key, value in uuid_data.items():
                if key.lower() == "hierarchy":
                    hierarchy_column = value
                if key.lower() == "rollup_header":
                    rollup_column = value

            self.my_logger.debug(f"---- rollup header ------ {rollup_column}")
            self.my_logger.debug(f"---- hierarchy_column ----- {hierarchy_column}")
            self.my_logger.debug(f"--- View at value ---- {view_at}")
            rollup_flag = False
            if view_at:
                rollup_value = self.getSetGoalValue(parent_uuid, scenario_uuid)
                if rollup_value is not None and view_at.lower() != rollup_value.lower():
                    scenario_uuid = scenario_uuid + "_rollup"
                    rollup_flag = True
            self.my_logger.debug(f"--- rollup flag ---- {rollup_flag}")
            table_name = 'IC_Output.[t_' + scenario_uuid + ']'
            query = None
            if rollup_flag:
                query = "Select * FROM "+table_name+" WHERE Geo_Type='"+view_at+"' "
            else:
                query = 'exec sp_quota_graph_view \'' + table_name + '\',\'' +hierarchy_column + '\''

            self.my_logger.debug(f"--- query ---- {query}")
            df = pd.read_sql_query(text(query), self.engine)
            data = json.loads(df.to_json(orient='records'))
            final_data["data"] = data
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
        except Exception as e:
            raise
        finally:
            self.engine.dispose()

        return json.dumps(final_data)

    def read_graph_file(self, parent_uuid, sc_uuid):
        """
        This method returns saved graph json file for quota instance
        :param parent_uuid: quota object uuid
        :param sc_uuid: quota instance uuid
        :return: json data
        """
        self.my_logger.debug(f"--- inside read graph file ---- ")
        self.parent_uuid = parent_uuid
        self.uuid = sc_uuid
        parent_dir = self.main_dir
        file_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + self.parent_uuid + "\\Simulation\\" + self.uuid + "\\Graph_Info.txt"
        data = dict()
        try:
            with open(file_loc) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_loc.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_loc.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def prepare_final_table(self, scenario_uuid, plan_uuid):
        status = 'failed'
        admin_db = src.helper.sql_connection.connect_to_sql_db(self.tenant, 'processing')
        cursor_admin = admin_db.cursor()
        try:
            self.my_logger.info(f"--- inside prepare_final_table method ------ ")
            sc_uuid = scenario_uuid
            planid = plan_uuid
            self.my_logger.debug(f"---- scenario uuid ---- {sc_uuid}")
            self.my_logger.debug(f"---- planid uuid ---- {planid}")
            parent_dir = self.main_dir
            file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
            with open(file_location) as json_file:
                file_data = json.load(json_file)
            uuid_data = file_data[scenario_uuid]
            hierarchy_column = None
            for key, value in uuid_data.items():
                if key.lower() == "hierarchy":
                    hierarchy_column = value
            rollup_flag = self.checkForRollup(scenario_uuid, plan_uuid)

            input_table = "IC_Output.[t_" + planid + "]"
            self.my_logger.debug(f"----- input table id ----- {input_table}")
            output_table = None
            self.my_logger.debug(f"--- rollup flag ---- {rollup_flag}")
            if rollup_flag == "false":
                output_table = "IC_Output.[t_" + sc_uuid + "]"
            else:
                output_table = "IC_Output.[t_" + sc_uuid + "_rollup]"

            hierarchy_column_list = hierarchy_column.split(",")
            geo_type_column = hierarchy_column_list[-1]
            order_by = " ORDER BY " + geo_type_column
            self.my_logger.debug(f"--- order by ---- " + order_by)
            where_clause = " Where "+geo_type_column+" IS NOT NULL"

            query = "sp_quota_final '"+input_table+"','"+output_table+"','"+order_by+"','"+where_clause+"'"
            self.my_logger.debug(f"--- final table proc ---> {query}")
            try:
                result_set = cursor_admin.execute(query)
                status = 'Success'
            except Exception as e:
                self.my_logger.debug("--- error while executing prepare_final_table proc ----- ")
                self.my_logger.error(e.args)
                self.my_logger.exception(traceback.format_exc())
                SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
                SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: prepare_final_table method', scenario_uuid)
                status = 'failed'
        except Exception as e:
            status = 'failed'
            self.my_logger.debug(f" --- unable to prepare final table --- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: prepare_final_table method', scenario_uuid)
        finally:
            cursor_admin.close()
            admin_db.commit()
        return status
    
    def read_default_rules(self):
        """
        This method returns default rules json file for quota instance
        :return: json data
        """
        self.my_logger.info(f" ---- inside read_default_rules methods ------ ")
        filename = self.main_dir + "\\" + self.tenant + '\\default_rules_info.txt'
        self.my_logger.info(f"----- filename ------ " + filename)
        data = dict()
        try:
            with open(filename) as json_file:
                data = json.load(json_file)
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', filename.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', filename.rsplit('\\', 1)[1])
        except Exception as e:
            raise

        return json.dumps(data)

    def checkForRollup(self, scenario_uuid, plan_uuid):
        parent_dir = self.main_dir
        file_location = parent_dir + "\\" + self.tenant + "\\plan_listing.txt"
        file_data = {}
        with open(file_location) as json_file:
            file_data = json.load(json_file)

        scenario_data = file_data[scenario_uuid]
        plan_execution_data = scenario_data["plan_execution"]
        rollup_flag = "false"
        for item in plan_execution_data:
            self.my_logger.debug(f"---- item ---- {item}")
            for key, value in item.items():
                if key == "plan_uuid":
                    if value == plan_uuid:
                        self.my_logger.info(f"--- plan id match ----")
                        if "rollup_flag" in item.keys():
                            self.my_logger.info(f"--- getting rollup flag ----")
                            rollup_flag = item["rollup_flag"]
                        else:
                            rollup_flag = "false"

        return rollup_flag

    def checkRollupEnable(self, scenario_parent_uuid, scenario_uuid):
        parent_dir = self.main_dir
        folder_loc = parent_dir + "\\" + self.tenant + "\\QuotaSettings\\" + scenario_parent_uuid + "\\Simulation\\" + scenario_uuid
        file_name = scenario_uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        rollup_enable = "false"
        try:
            data = dict()
            with open(file_loc) as json_file:
                data = json.load(json_file)

            file_data = data["Parameters"]

            for item in file_data:
                for key, value in item.items():
                    if key.lower() == "sfdc":
                        if value == "":
                            rollup_enable = item["Value"]
        except Exception as e:
            self.my_logger.info(f"--- Unable to read UI file ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: checkRollupEnable method', scenario_uuid)

        return rollup_enable.lower()

    def getSetGoalValue(self, parent_uuid, scenario_uuid):
        folder_loc = self.main_dir + "\\" + self.tenant + "\\QuotaSettings\\" + parent_uuid + "\\Simulation\\" + scenario_uuid
        file_name = scenario_uuid + '_ui_value.txt'
        file_loc = folder_loc + '\\' + file_name
        data = dict()
        with open(file_loc) as json_file:
            data = json.load(json_file)
        file_data = data["Parameters"]
        rollup_value = None
        for item in file_data:
            for key, value in item.items():
                if key.lower() == "sfdc" and value.lower() == "calculate_goal_level__c":
                    rollup_value = item["Value"]

        return rollup_value

    def get_component_name(self, plan_uuid, uuid_param, parent_uuid, sequence):
        component_dict = dict()
        try:
            self.my_logger.info(f"*************** Get component name Method *************")
            pjobj = ParseJSON()
            saveuifile_path = self.config_obj.get_value_from_key("folder_location", "SaveUIFile")
            self.my_logger.info(f"---- SaveUI file Path ---- {saveuifile_path}")
            path = saveuifile_path + "\\Static_Config\\FilePath.txt"
            resource_file_data = pjobj.LoadJSONFromFile(path)
            type_module = pjobj.GetValueFromKeyPath(resource_file_data, "FilePath.Plan")
            template_config_data = self.config_obj.returnJSON(type_module, "JSON")
            self.my_logger.debug(f"---- template config data ---- ")
            self.my_logger.debug(f"template_config_data")
            instance_key = pjobj.GetAllPaths(template_config_data, ["Instances_Folder", "Plan"])[0].split(":->")[0]
            instance_key_data = pjobj.GetValueFromKeyPath(template_config_data, instance_key)
            self.my_logger.debug(f" instance_key_data -- {instance_key_data}")
            instance_file_path = instance_key_data + "\\" + plan_uuid + "\\" + plan_uuid + "_with_values.txt"
            self.my_logger.debug(f"--- instance_file_path ---- {instance_file_path}")
            plan_file = pjobj.LoadJSONFromFile(instance_file_path)
            plan_data = plan_file[plan_uuid]
            component_name = None
            name_id_map = {}
            for key, value in plan_data.items():
                if key.lower() == 'plan_components':
                    component_list = plan_data[key]
                    for component in component_list:
                        for comp_key, comp_value_dict in component.items():
                            self.my_logger.debug(f"----- component ID ------ ")
                            self.my_logger.debug(f"{comp_key}")
                            self.my_logger.debug(f"----- Current component Name ------- ")
                            self.my_logger.debug(comp_value_dict["Param_Component_Name"])
                            if component_name is None:
                                component_name = comp_value_dict["Param_Component_Name"]
                            else:
                                component_name = component_name + "," + comp_value_dict["Param_Component_Name"]
                            name_id_map[comp_value_dict["Param_Component_Name"]] = comp_key
            # ---- IQ-61 ----
            id_wt_map = {}
            if sequence == 1:
                ui_file_json = json.loads(self.read_simulation_ui_value(uuid_param, parent_uuid))
                markers_data = ui_file_json["Markers"]
                for marker in markers_data:
                    comp_key = marker["Component_Id"]
                    marker_wt = marker["Marker_Weight"]
                    id_wt_map[comp_key] = marker_wt

            self.my_logger.debug(f"----- component name ------")
            self.my_logger.debug(component_name)

            if component_name is not None:
                component_name_list = component_name.split(",")
                for name in component_name_list:
                    if sequence == 1:
                        comp_key = name_id_map[name]
                        if comp_key in id_wt_map:
                            mark_wt = id_wt_map[comp_key]
                            if mark_wt == "0" or mark_wt == "":
                                component_dict[name] = "False"
                            else:
                                component_dict[name] = "True"
                        else:
                            component_dict[name] = "True"
                    else:
                        component_dict[name] = "True"
            # --- END ---
            self.my_logger.debug(f"----- component dict -------")
            self.my_logger.debug(component_dict)
        except Exception as e:
            component_dict = dict()
            self.my_logger.debug(f"---- Error while getting component Name ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_component_name method', plan_uuid)

        return str(component_dict)

    def get_instance_list(self, scenario_uuid):
        """
        This method returns plan info for quota instance
        :param scenario_uuid: quota instance uuid
        :return: json data
        """
        result = dict()
        file_location = ''
        try:
            file_location = self.main_dir + "\\" + self.tenant + "\\plan_listing.txt"
            self.my_logger.debug(f"--- file location ----- {file_location}")
            file_exist = os.path.exists(file_location)
            file_data = {}
            self.my_logger.debug(f"--- file exist ----- {file_exist}")
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug("  file created ")
            else:
                with open(file_location) as json_file:
                    file_data = json.load(json_file)

            scenario_data = file_data[scenario_uuid]["plan_execution"]
            parent_uuid = file_data[scenario_uuid]["Goal_Def_Id"]
            rollup_flag = self.checkRollupEnable(parent_uuid, scenario_uuid)
            instance_list = []
            for item in scenario_data:
                for key, value in item.items():
                    if key.lower() == "plan_uuid":
                        if "rollup_flag" in item.keys():
                            curr_flag = item["rollup_flag"]
                            if rollup_flag == "false" and curr_flag.lower() == "true":
                                self.my_logger.debug(f"--- No need to add ---- {item[key]}")
                            else:
                                self.my_logger.debug(f"--- Adding Plan uuid ---- {item[key]}")
                                instance_list.append(item["plan_uuid"])
                        else:
                            self.my_logger.debug(f"--- Adding Plan uuid ---- {item[key]}")
                            instance_list.append(item["plan_uuid"])
                    break

            self.my_logger.debug(f"---- Final List ----- ")
            self.my_logger.debug(f"{instance_list}")
            result = dict()
            result["data"] = instance_list
        except FileNotFoundError as e:
            raise Exception('FileNotFoundError', file_location.rsplit('\\', 1)[1])
        except JSONDecodeError as e:
            raise Exception('JSONDecodeError', file_location.rsplit('\\', 1)[1])
        except KeyError as e:
            raise Exception('KeyError', e.args)
        except Exception as e:
            raise

        return json.dumps(result)

    # IQ-557
    def get_paygrid_info(self, plan_uuid, parent_plan_uuid, gs_uuid, gs_parent_uuid):
        paygrid_data = json.dumps(dict())
        status = 'failed'
        try:
            ui_file_json = json.loads(self.read_simulation_ui_value(gs_uuid, gs_parent_uuid))
            parent_plan_id = self.get_parent_plan_sfdcid(parent_plan_uuid, gs_uuid)
            self.my_logger.debug(f"---- parent_plan_id ---- {parent_plan_id}")
            if parent_plan_id is None:
                return status, paygrid_data
            else:
                found_flag = False
                parameters = ui_file_json["Parameters"]
                for item in parameters:
                    for key, value in item.items():
                        if key == 'Type' and value == 'Action_Plan':
                            object_id = item['Object_Id']
                            self.my_logger.debug(f"-- Processing plan ID --- {object_id}")
                            if object_id == parent_plan_id:
                                self.my_logger.debug(f"---- Plan Id found ----")
                                found_flag = True
                                self.my_logger.debug(f"--- current dictionary ---")
                                self.my_logger.debug(f"{item}")
                                if 'payGrid' in item:
                                    self.my_logger.debug(f"--- key Present ---")
                                    paygrid_data = urllib.parse.unquote(item['payGrid'])
                                else:
                                    self.my_logger.debug(f"--- no key Present ---")
                                    paygrid_data = json.dumps(dict())
                if not found_flag:
                    status = 'failed'
                    paygrid_data = json.dumps(dict())
                else:
                    status = 'Success'
        except Exception as e:
            paygrid_data = json.dumps(dict())
            status = 'failed'
            self.my_logger.debug(f"---- Error while getting paygrid information ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_paygrid_info method', gs_uuid)

        return status, paygrid_data

    def insert_time(self, sc_uuid, key, value):
        try:
            file_location = self.main_dir + "\\" + self.tenant + "\\time_stamp.txt"
            self.my_logger.debug(f"--- file location ----- {file_location}")
            file_exist = os.path.exists(file_location)
            file_data = {}
            self.my_logger.debug(f"--- file exists ----- {file_exist}")
            if not file_exist:
                with open(file_location, 'w+') as fileobject:
                    json.dump(file_data, fileobject, indent=4)
                    self.my_logger.debug(" *** Time Stamp file created *** ")

            with open(file_location) as json_file:
                file_data = json.load(json_file)

            if sc_uuid in file_data:
                file_data[sc_uuid][key] = value
            else:
                file_data[sc_uuid] = {}
                file_data[sc_uuid][key] = value

            with open(file_location, 'w+') as file_obj:
                self.my_logger.debug(f" *** writing in time stamp file ----")
                json.dump(file_data, file_obj, indent=4)
        except Exception as e:
            self.my_logger.info("*** Error occurred while writing in stamp file ***")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())

    def get_time(self, sc_uuid, key):
        value = datetime.now()
        try:
            file_location = self.main_dir + "\\" + self.tenant + "\\time_stamp.txt"
            with open(file_location) as json_file:
                file_data = json.load(json_file)
            value = file_data[sc_uuid][key]
        except Exception as e:
            self.my_logger.info(" *** Error occurred while reading time stamp value *** ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())

        return value

    # IQ-557
    def get_parent_plan_sfdcid(self, plan_uuid, gs_uuid):
        record_id = None
        try:
            SalesForce.connect_to_sfdc(self.tenant, self.my_logger)
            query = "Select Id from t_Plan__c where plan_uuid__c='"+plan_uuid+"' and Tenant__r.Name='"+self.tenant+"' and Type__c = 'Plan' LIMIT 1"
            record_list = SalesForce.query(query)
            record_list.columns = ['Id']
            for _, row in record_list.iterrows():
                record_id = row['Id']
        except Exception as e:
            self.my_logger.debug(f"---- Error while getting component Name ---- ")
            self.my_logger.error(e.args)
            self.my_logger.exception(traceback.format_exc())
            SalesForce.insert_log(e, traceback.format_exc(), 'Quota Setting: get_parent_plan_sfdcid method', gs_uuid)

        return record_id

